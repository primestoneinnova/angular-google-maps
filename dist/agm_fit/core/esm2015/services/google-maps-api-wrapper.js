import * as tslib_1 from "tslib";
import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { MapsAPILoader } from './maps-api-loader/maps-api-loader';
/**
 * Wrapper class that handles the communication with the Google Maps Javascript
 * API v3
 */
let GoogleMapsAPIWrapper = class GoogleMapsAPIWrapper {
    constructor(_loader, _zone) {
        this._loader = _loader;
        this._zone = _zone;
        this._map =
            new Promise((resolve) => { this._mapResolver = resolve; });
    }
    createMap(el, mapOptions) {
        return this._zone.runOutsideAngular(() => {
            return this._loader.load().then(() => {
                const map = new google.maps.Map(el, mapOptions);
                this._mapResolver(map);
                return;
            });
        });
    }
    setMapOptions(options) {
        this._map.then((m) => { m.setOptions(options); });
    }
    /**
     * Creates a google map marker with the map context
     */
    createMarker(options = {}, addToMap = true) {
        return this._map.then((map) => {
            if (addToMap) {
                options.map = map;
            }
            return new google.maps.Marker(options);
        });
    }
    createInfoWindow(options) {
        return this._map.then(() => { return new google.maps.InfoWindow(options); });
    }
    /**
     * Creates a google.map.Circle for the current map.
     */
    createCircle(options) {
        return this._map.then((map) => {
            if (typeof options.strokePosition === 'string') {
                options.strokePosition = google.maps.StrokePosition[options.strokePosition];
            }
            options.map = map;
            return new google.maps.Circle(options);
        });
    }
    /**
     * Creates a google.map.Rectangle for the current map.
     */
    createRectangle(options) {
        return this._map.then((map) => {
            options.map = map;
            return new google.maps.Rectangle(options);
        });
    }
    createPolyline(options) {
        return this.getNativeMap().then((map) => {
            let line = new google.maps.Polyline(options);
            line.setMap(map);
            return line;
        });
    }
    createPolygon(options) {
        return this.getNativeMap().then((map) => {
            let polygon = new google.maps.Polygon(options);
            polygon.setMap(map);
            return polygon;
        });
    }
    /**
     * Creates a new google.map.Data layer for the current map
     */
    createDataLayer(options) {
        return this._map.then(m => {
            let data = new google.maps.Data(options);
            data.setMap(m);
            return data;
        });
    }
    /**
     * Creates a TransitLayer instance for a map
     * @param {TransitLayerOptions} options - used for setting layer options
     * @returns {Promise<TransitLayer>} a new transit layer object
     */
    createTransitLayer(options) {
        return this._map.then((map) => {
            let newLayer = new google.maps.TransitLayer();
            newLayer.setMap(options.visible ? map : null);
            return newLayer;
        });
    }
    /**
     * Creates a BicyclingLayer instance for a map
     * @param {BicyclingLayerOptions} options - used for setting layer options
     * @returns {Promise<BicyclingLayer>} a new bicycling layer object
     */
    createBicyclingLayer(options) {
        return this._map.then((map) => {
            let newLayer = new google.maps.BicyclingLayer();
            newLayer.setMap(options.visible ? map : null);
            return newLayer;
        });
    }
    /**
     * Determines if given coordinates are insite a Polygon path.
     */
    containsLocation(latLng, polygon) {
        return google.maps.geometry.poly.containsLocation(latLng, polygon);
    }
    subscribeToMapEvent(eventName) {
        return new Observable((observer) => {
            this._map.then((m) => {
                m.addListener(eventName, (arg) => { this._zone.run(() => observer.next(arg)); });
            });
        });
    }
    clearInstanceListeners() {
        this._map.then((map) => {
            google.maps.event.clearInstanceListeners(map);
        });
    }
    setCenter(latLng) {
        return this._map.then((map) => map.setCenter(latLng));
    }
    getZoom() { return this._map.then((map) => map.getZoom()); }
    getBounds() {
        return this._map.then((map) => map.getBounds());
    }
    getMapTypeId() {
        return this._map.then((map) => map.getMapTypeId());
    }
    setZoom(zoom) {
        return this._map.then((map) => map.setZoom(zoom));
    }
    getCenter() {
        return this._map.then((map) => map.getCenter());
    }
    panTo(latLng) {
        return this._map.then((map) => map.panTo(latLng));
    }
    panBy(x, y) {
        return this._map.then((map) => map.panBy(x, y));
    }
    fitBounds(latLng, padding) {
        return this._map.then((map) => map.fitBounds(latLng, padding));
    }
    panToBounds(latLng, padding) {
        return this._map.then((map) => map.panToBounds(latLng, padding));
    }
    /**
     * Returns the native Google Maps Map instance. Be careful when using this instance directly.
     */
    getNativeMap() { return this._map; }
    /**
     * Triggers the given event name on the map instance.
     */
    triggerMapEvent(eventName) {
        return this._map.then((m) => google.maps.event.trigger(m, eventName));
    }
};
GoogleMapsAPIWrapper.ctorParameters = () => [
    { type: MapsAPILoader },
    { type: NgZone }
];
GoogleMapsAPIWrapper = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [MapsAPILoader, NgZone])
], GoogleMapsAPIWrapper);
export { GoogleMapsAPIWrapper };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLW1hcHMtYXBpLXdyYXBwZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hZ21fZml0L2NvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9nb29nbGUtbWFwcy1hcGktd3JhcHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLFVBQVUsRUFBWSxNQUFNLE1BQU0sQ0FBQztBQUk1QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFLbEU7OztHQUdHO0FBRUgsSUFBYSxvQkFBb0IsR0FBakMsTUFBYSxvQkFBb0I7SUFJL0IsWUFBb0IsT0FBc0IsRUFBVSxLQUFhO1FBQTdDLFlBQU8sR0FBUCxPQUFPLENBQWU7UUFBVSxVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQy9ELElBQUksQ0FBQyxJQUFJO1lBQ0wsSUFBSSxPQUFPLENBQXFCLENBQUMsT0FBbUIsRUFBRSxFQUFFLEdBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBRUQsU0FBUyxDQUFDLEVBQWUsRUFBRSxVQUErQjtRQUN4RCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUUsR0FBRyxFQUFFO1lBQ3hDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFO2dCQUNuQyxNQUFNLEdBQUcsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDaEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUF5QixDQUFDLENBQUM7Z0JBQzdDLE9BQU87WUFDVCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGFBQWEsQ0FBQyxPQUE0QjtRQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQXFCLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQ7O09BRUc7SUFDSCxZQUFZLENBQUMsVUFBa0MsRUFBNEIsRUFBRSxXQUFvQixJQUFJO1FBRW5HLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUF1QixFQUFFLEVBQUU7WUFDaEQsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7YUFDbkI7WUFDRCxPQUFPLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsT0FBb0M7UUFDbkQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxPQUFPLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvRSxDQUFDO0lBRUQ7O09BRUc7SUFDSCxZQUFZLENBQUMsT0FBK0I7UUFDMUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQXVCLEVBQUUsRUFBRTtZQUNoRCxJQUFJLE9BQU8sT0FBTyxDQUFDLGNBQWMsS0FBSyxRQUFRLEVBQUU7Z0JBQzlDLE9BQU8sQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2FBQzdFO1lBQ0QsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7WUFDbEIsT0FBTyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZUFBZSxDQUFDLE9BQWtDO1FBQ2hELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUF1QixFQUFFLEVBQUU7WUFDaEQsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7WUFDbEIsT0FBTyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzVDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGNBQWMsQ0FBQyxPQUF3QjtRQUNyQyxPQUFPLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUF1QixFQUFFLEVBQUU7WUFDMUQsSUFBSSxJQUFJLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM3QyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2pCLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsYUFBYSxDQUFDLE9BQWdDO1FBQzVDLE9BQU8sSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQXVCLEVBQUUsRUFBRTtZQUMxRCxJQUFJLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQy9DLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDcEIsT0FBTyxPQUFPLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxlQUFlLENBQUMsT0FBOEI7UUFDNUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN4QixJQUFJLElBQUksR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDZixPQUFPLElBQUksQ0FBQztRQUNkLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxrQkFBa0IsQ0FBQyxPQUFxQztRQUN0RCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBdUIsRUFBRSxFQUFFO1lBQ2hELElBQUksUUFBUSxHQUEwQixJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDckUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlDLE9BQU8sUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxvQkFBb0IsQ0FBQyxPQUF1QztRQUMxRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBdUIsRUFBRSxFQUFFO1lBQ2hELElBQUksUUFBUSxHQUE0QixJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDekUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlDLE9BQU8sUUFBUSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZ0JBQWdCLENBQUMsTUFBOEIsRUFBRSxPQUF5QjtRQUN4RSxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDckUsQ0FBQztJQUVELG1CQUFtQixDQUFJLFNBQWlCO1FBQ3RDLE9BQU8sSUFBSSxVQUFVLENBQUMsQ0FBQyxRQUFxQixFQUFFLEVBQUU7WUFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFxQixFQUFFLEVBQUU7Z0JBQ3ZDLENBQUMsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUMsR0FBTSxFQUFFLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0RixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNCQUFzQjtRQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQXVCLEVBQUUsRUFBRTtZQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxTQUFTLENBQUMsTUFBOEI7UUFDdEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQXVCLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRUQsT0FBTyxLQUFzQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBdUIsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRWpHLFNBQVM7UUFDUCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBdUIsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVELFlBQVk7UUFDVixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBdUIsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVELE9BQU8sQ0FBQyxJQUFZO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUF1QixFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUVELFNBQVM7UUFDUCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBdUIsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVELEtBQUssQ0FBQyxNQUFnRDtRQUNwRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELEtBQUssQ0FBQyxDQUFTLEVBQUUsQ0FBUztRQUN4QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCxTQUFTLENBQUMsTUFBNEQsRUFBRSxPQUFtQztRQUN6RyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFFRCxXQUFXLENBQUMsTUFBNEQsRUFBRSxPQUFtQztRQUMzRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ25FLENBQUM7SUFFRDs7T0FFRztJQUNILFlBQVksS0FBa0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUVqRTs7T0FFRztJQUNILGVBQWUsQ0FBQyxTQUFpQjtRQUMvQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDeEUsQ0FBQztDQUNGLENBQUE7O1lBdEw4QixhQUFhO1lBQWlCLE1BQU07O0FBSnRELG9CQUFvQjtJQURoQyxVQUFVLEVBQUU7NkNBS2tCLGFBQWEsRUFBaUIsTUFBTTtHQUp0RCxvQkFBb0IsQ0EwTGhDO1NBMUxZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBPYnNlcnZlciB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0ICogYXMgbWFwVHlwZXMgZnJvbSAnLi9nb29nbGUtbWFwcy10eXBlcyc7XHJcbmltcG9ydCB7IFBvbHlsaW5lLCBQb2x5bGluZU9wdGlvbnMgfSBmcm9tICcuL2dvb2dsZS1tYXBzLXR5cGVzJztcclxuaW1wb3J0IHsgTWFwc0FQSUxvYWRlciB9IGZyb20gJy4vbWFwcy1hcGktbG9hZGVyL21hcHMtYXBpLWxvYWRlcic7XHJcblxyXG4vLyB0b2RvOiBhZGQgdHlwZXMgZm9yIHRoaXNcclxuZGVjbGFyZSB2YXIgZ29vZ2xlOiBhbnk7XHJcblxyXG4vKipcclxuICogV3JhcHBlciBjbGFzcyB0aGF0IGhhbmRsZXMgdGhlIGNvbW11bmljYXRpb24gd2l0aCB0aGUgR29vZ2xlIE1hcHMgSmF2YXNjcmlwdFxyXG4gKiBBUEkgdjNcclxuICovXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEdvb2dsZU1hcHNBUElXcmFwcGVyIHtcclxuICBwcml2YXRlIF9tYXA6IFByb21pc2U8bWFwVHlwZXMuR29vZ2xlTWFwPjtcclxuICBwcml2YXRlIF9tYXBSZXNvbHZlcjogKHZhbHVlPzogbWFwVHlwZXMuR29vZ2xlTWFwKSA9PiB2b2lkO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9sb2FkZXI6IE1hcHNBUElMb2FkZXIsIHByaXZhdGUgX3pvbmU6IE5nWm9uZSkge1xyXG4gICAgdGhpcy5fbWFwID1cclxuICAgICAgICBuZXcgUHJvbWlzZTxtYXBUeXBlcy5Hb29nbGVNYXA+KChyZXNvbHZlOiAoKSA9PiB2b2lkKSA9PiB7IHRoaXMuX21hcFJlc29sdmVyID0gcmVzb2x2ZTsgfSk7XHJcbiAgfVxyXG5cclxuICBjcmVhdGVNYXAoZWw6IEhUTUxFbGVtZW50LCBtYXBPcHRpb25zOiBtYXBUeXBlcy5NYXBPcHRpb25zKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICByZXR1cm4gdGhpcy5fem9uZS5ydW5PdXRzaWRlQW5ndWxhciggKCkgPT4ge1xyXG4gICAgICByZXR1cm4gdGhpcy5fbG9hZGVyLmxvYWQoKS50aGVuKCgpID0+IHtcclxuICAgICAgICBjb25zdCBtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGVsLCBtYXBPcHRpb25zKTtcclxuICAgICAgICB0aGlzLl9tYXBSZXNvbHZlcihtYXAgYXMgbWFwVHlwZXMuR29vZ2xlTWFwKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH0pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBzZXRNYXBPcHRpb25zKG9wdGlvbnM6IG1hcFR5cGVzLk1hcE9wdGlvbnMpIHtcclxuICAgIHRoaXMuX21hcC50aGVuKChtOiBtYXBUeXBlcy5Hb29nbGVNYXApID0+IHsgbS5zZXRPcHRpb25zKG9wdGlvbnMpOyB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENyZWF0ZXMgYSBnb29nbGUgbWFwIG1hcmtlciB3aXRoIHRoZSBtYXAgY29udGV4dFxyXG4gICAqL1xyXG4gIGNyZWF0ZU1hcmtlcihvcHRpb25zOiBtYXBUeXBlcy5NYXJrZXJPcHRpb25zID0ge30gYXMgbWFwVHlwZXMuTWFya2VyT3B0aW9ucywgYWRkVG9NYXA6IGJvb2xlYW4gPSB0cnVlKTpcclxuICAgICAgUHJvbWlzZTxtYXBUeXBlcy5NYXJrZXI+IHtcclxuICAgIHJldHVybiB0aGlzLl9tYXAudGhlbigobWFwOiBtYXBUeXBlcy5Hb29nbGVNYXApID0+IHtcclxuICAgICAgaWYgKGFkZFRvTWFwKSB7XHJcbiAgICAgICAgb3B0aW9ucy5tYXAgPSBtYXA7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIG5ldyBnb29nbGUubWFwcy5NYXJrZXIob3B0aW9ucyk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNyZWF0ZUluZm9XaW5kb3cob3B0aW9ucz86IG1hcFR5cGVzLkluZm9XaW5kb3dPcHRpb25zKTogUHJvbWlzZTxtYXBUeXBlcy5JbmZvV2luZG93PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fbWFwLnRoZW4oKCkgPT4geyByZXR1cm4gbmV3IGdvb2dsZS5tYXBzLkluZm9XaW5kb3cob3B0aW9ucyk7IH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ3JlYXRlcyBhIGdvb2dsZS5tYXAuQ2lyY2xlIGZvciB0aGUgY3VycmVudCBtYXAuXHJcbiAgICovXHJcbiAgY3JlYXRlQ2lyY2xlKG9wdGlvbnM6IG1hcFR5cGVzLkNpcmNsZU9wdGlvbnMpOiBQcm9taXNlPG1hcFR5cGVzLkNpcmNsZT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX21hcC50aGVuKChtYXA6IG1hcFR5cGVzLkdvb2dsZU1hcCkgPT4ge1xyXG4gICAgICBpZiAodHlwZW9mIG9wdGlvbnMuc3Ryb2tlUG9zaXRpb24gPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgb3B0aW9ucy5zdHJva2VQb3NpdGlvbiA9IGdvb2dsZS5tYXBzLlN0cm9rZVBvc2l0aW9uW29wdGlvbnMuc3Ryb2tlUG9zaXRpb25dO1xyXG4gICAgICB9XHJcbiAgICAgIG9wdGlvbnMubWFwID0gbWFwO1xyXG4gICAgICByZXR1cm4gbmV3IGdvb2dsZS5tYXBzLkNpcmNsZShvcHRpb25zKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ3JlYXRlcyBhIGdvb2dsZS5tYXAuUmVjdGFuZ2xlIGZvciB0aGUgY3VycmVudCBtYXAuXHJcbiAgICovXHJcbiAgY3JlYXRlUmVjdGFuZ2xlKG9wdGlvbnM6IG1hcFR5cGVzLlJlY3RhbmdsZU9wdGlvbnMpOiBQcm9taXNlPG1hcFR5cGVzLlJlY3RhbmdsZT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX21hcC50aGVuKChtYXA6IG1hcFR5cGVzLkdvb2dsZU1hcCkgPT4ge1xyXG4gICAgICBvcHRpb25zLm1hcCA9IG1hcDtcclxuICAgICAgcmV0dXJuIG5ldyBnb29nbGUubWFwcy5SZWN0YW5nbGUob3B0aW9ucyk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNyZWF0ZVBvbHlsaW5lKG9wdGlvbnM6IFBvbHlsaW5lT3B0aW9ucyk6IFByb21pc2U8UG9seWxpbmU+IHtcclxuICAgIHJldHVybiB0aGlzLmdldE5hdGl2ZU1hcCgpLnRoZW4oKG1hcDogbWFwVHlwZXMuR29vZ2xlTWFwKSA9PiB7XHJcbiAgICAgIGxldCBsaW5lID0gbmV3IGdvb2dsZS5tYXBzLlBvbHlsaW5lKG9wdGlvbnMpO1xyXG4gICAgICBsaW5lLnNldE1hcChtYXApO1xyXG4gICAgICByZXR1cm4gbGluZTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY3JlYXRlUG9seWdvbihvcHRpb25zOiBtYXBUeXBlcy5Qb2x5Z29uT3B0aW9ucyk6IFByb21pc2U8bWFwVHlwZXMuUG9seWdvbj4ge1xyXG4gICAgcmV0dXJuIHRoaXMuZ2V0TmF0aXZlTWFwKCkudGhlbigobWFwOiBtYXBUeXBlcy5Hb29nbGVNYXApID0+IHtcclxuICAgICAgbGV0IHBvbHlnb24gPSBuZXcgZ29vZ2xlLm1hcHMuUG9seWdvbihvcHRpb25zKTtcclxuICAgICAgcG9seWdvbi5zZXRNYXAobWFwKTtcclxuICAgICAgcmV0dXJuIHBvbHlnb247XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENyZWF0ZXMgYSBuZXcgZ29vZ2xlLm1hcC5EYXRhIGxheWVyIGZvciB0aGUgY3VycmVudCBtYXBcclxuICAgKi9cclxuICBjcmVhdGVEYXRhTGF5ZXIob3B0aW9ucz86IG1hcFR5cGVzLkRhdGFPcHRpb25zKTogUHJvbWlzZTxtYXBUeXBlcy5EYXRhPiB7XHJcbiAgICByZXR1cm4gdGhpcy5fbWFwLnRoZW4obSA9PiB7XHJcbiAgICAgIGxldCBkYXRhID0gbmV3IGdvb2dsZS5tYXBzLkRhdGEob3B0aW9ucyk7XHJcbiAgICAgIGRhdGEuc2V0TWFwKG0pO1xyXG4gICAgICByZXR1cm4gZGF0YTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ3JlYXRlcyBhIFRyYW5zaXRMYXllciBpbnN0YW5jZSBmb3IgYSBtYXBcclxuICAgKiBAcGFyYW0ge1RyYW5zaXRMYXllck9wdGlvbnN9IG9wdGlvbnMgLSB1c2VkIGZvciBzZXR0aW5nIGxheWVyIG9wdGlvbnNcclxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxUcmFuc2l0TGF5ZXI+fSBhIG5ldyB0cmFuc2l0IGxheWVyIG9iamVjdFxyXG4gICAqL1xyXG4gIGNyZWF0ZVRyYW5zaXRMYXllcihvcHRpb25zOiBtYXBUeXBlcy5UcmFuc2l0TGF5ZXJPcHRpb25zKTogUHJvbWlzZTxtYXBUeXBlcy5UcmFuc2l0TGF5ZXI+e1xyXG4gICAgcmV0dXJuIHRoaXMuX21hcC50aGVuKChtYXA6IG1hcFR5cGVzLkdvb2dsZU1hcCkgPT4ge1xyXG4gICAgICBsZXQgbmV3TGF5ZXI6IG1hcFR5cGVzLlRyYW5zaXRMYXllciA9IG5ldyBnb29nbGUubWFwcy5UcmFuc2l0TGF5ZXIoKTtcclxuICAgICAgbmV3TGF5ZXIuc2V0TWFwKG9wdGlvbnMudmlzaWJsZSA/IG1hcCA6IG51bGwpO1xyXG4gICAgICByZXR1cm4gbmV3TGF5ZXI7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENyZWF0ZXMgYSBCaWN5Y2xpbmdMYXllciBpbnN0YW5jZSBmb3IgYSBtYXBcclxuICAgKiBAcGFyYW0ge0JpY3ljbGluZ0xheWVyT3B0aW9uc30gb3B0aW9ucyAtIHVzZWQgZm9yIHNldHRpbmcgbGF5ZXIgb3B0aW9uc1xyXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPEJpY3ljbGluZ0xheWVyPn0gYSBuZXcgYmljeWNsaW5nIGxheWVyIG9iamVjdFxyXG4gICAqL1xyXG4gIGNyZWF0ZUJpY3ljbGluZ0xheWVyKG9wdGlvbnM6IG1hcFR5cGVzLkJpY3ljbGluZ0xheWVyT3B0aW9ucyk6IFByb21pc2U8bWFwVHlwZXMuQmljeWNsaW5nTGF5ZXI+e1xyXG4gICAgcmV0dXJuIHRoaXMuX21hcC50aGVuKChtYXA6IG1hcFR5cGVzLkdvb2dsZU1hcCkgPT4ge1xyXG4gICAgICBsZXQgbmV3TGF5ZXI6IG1hcFR5cGVzLkJpY3ljbGluZ0xheWVyID0gbmV3IGdvb2dsZS5tYXBzLkJpY3ljbGluZ0xheWVyKCk7XHJcbiAgICAgIG5ld0xheWVyLnNldE1hcChvcHRpb25zLnZpc2libGUgPyBtYXAgOiBudWxsKTtcclxuICAgICAgcmV0dXJuIG5ld0xheWVyO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBEZXRlcm1pbmVzIGlmIGdpdmVuIGNvb3JkaW5hdGVzIGFyZSBpbnNpdGUgYSBQb2x5Z29uIHBhdGguXHJcbiAgICovXHJcbiAgY29udGFpbnNMb2NhdGlvbihsYXRMbmc6IG1hcFR5cGVzLkxhdExuZ0xpdGVyYWwsIHBvbHlnb246IG1hcFR5cGVzLlBvbHlnb24pOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgIHJldHVybiBnb29nbGUubWFwcy5nZW9tZXRyeS5wb2x5LmNvbnRhaW5zTG9jYXRpb24obGF0TG5nLCBwb2x5Z29uKTtcclxuICB9XHJcblxyXG4gIHN1YnNjcmliZVRvTWFwRXZlbnQ8RT4oZXZlbnROYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPEU+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXI6IE9ic2VydmVyPEU+KSA9PiB7XHJcbiAgICAgIHRoaXMuX21hcC50aGVuKChtOiBtYXBUeXBlcy5Hb29nbGVNYXApID0+IHtcclxuICAgICAgICBtLmFkZExpc3RlbmVyKGV2ZW50TmFtZSwgKGFyZzogRSkgPT4geyB0aGlzLl96b25lLnJ1bigoKSA9PiBvYnNlcnZlci5uZXh0KGFyZykpOyB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNsZWFySW5zdGFuY2VMaXN0ZW5lcnMoKSB7XHJcbiAgICB0aGlzLl9tYXAudGhlbigobWFwOiBtYXBUeXBlcy5Hb29nbGVNYXApID0+IHtcclxuICAgICAgZ29vZ2xlLm1hcHMuZXZlbnQuY2xlYXJJbnN0YW5jZUxpc3RlbmVycyhtYXApO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBzZXRDZW50ZXIobGF0TG5nOiBtYXBUeXBlcy5MYXRMbmdMaXRlcmFsKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICByZXR1cm4gdGhpcy5fbWFwLnRoZW4oKG1hcDogbWFwVHlwZXMuR29vZ2xlTWFwKSA9PiBtYXAuc2V0Q2VudGVyKGxhdExuZykpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Wm9vbSgpOiBQcm9taXNlPG51bWJlcj4geyByZXR1cm4gdGhpcy5fbWFwLnRoZW4oKG1hcDogbWFwVHlwZXMuR29vZ2xlTWFwKSA9PiBtYXAuZ2V0Wm9vbSgpKTsgfVxyXG5cclxuICBnZXRCb3VuZHMoKTogUHJvbWlzZTxtYXBUeXBlcy5MYXRMbmdCb3VuZHM+IHtcclxuICAgIHJldHVybiB0aGlzLl9tYXAudGhlbigobWFwOiBtYXBUeXBlcy5Hb29nbGVNYXApID0+IG1hcC5nZXRCb3VuZHMoKSk7XHJcbiAgfVxyXG5cclxuICBnZXRNYXBUeXBlSWQoKTogUHJvbWlzZTxtYXBUeXBlcy5NYXBUeXBlSWQ+IHtcclxuICAgIHJldHVybiB0aGlzLl9tYXAudGhlbigobWFwOiBtYXBUeXBlcy5Hb29nbGVNYXApID0+IG1hcC5nZXRNYXBUeXBlSWQoKSk7XHJcbiAgfVxyXG5cclxuICBzZXRab29tKHpvb206IG51bWJlcik6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX21hcC50aGVuKChtYXA6IG1hcFR5cGVzLkdvb2dsZU1hcCkgPT4gbWFwLnNldFpvb20oem9vbSkpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Q2VudGVyKCk6IFByb21pc2U8bWFwVHlwZXMuTGF0TG5nPiB7XHJcbiAgICByZXR1cm4gdGhpcy5fbWFwLnRoZW4oKG1hcDogbWFwVHlwZXMuR29vZ2xlTWFwKSA9PiBtYXAuZ2V0Q2VudGVyKCkpO1xyXG4gIH1cclxuXHJcbiAgcGFuVG8obGF0TG5nOiBtYXBUeXBlcy5MYXRMbmcgfCBtYXBUeXBlcy5MYXRMbmdMaXRlcmFsKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICByZXR1cm4gdGhpcy5fbWFwLnRoZW4oKG1hcCkgPT4gbWFwLnBhblRvKGxhdExuZykpO1xyXG4gIH1cclxuXHJcbiAgcGFuQnkoeDogbnVtYmVyLCB5OiBudW1iZXIpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIHJldHVybiB0aGlzLl9tYXAudGhlbigobWFwKSA9PiBtYXAucGFuQnkoeCwgeSkpO1xyXG4gIH1cclxuXHJcbiAgZml0Qm91bmRzKGxhdExuZzogbWFwVHlwZXMuTGF0TG5nQm91bmRzIHwgbWFwVHlwZXMuTGF0TG5nQm91bmRzTGl0ZXJhbCwgcGFkZGluZz86IG51bWJlciB8IG1hcFR5cGVzLlBhZGRpbmcpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIHJldHVybiB0aGlzLl9tYXAudGhlbigobWFwKSA9PiBtYXAuZml0Qm91bmRzKGxhdExuZywgcGFkZGluZykpO1xyXG4gIH1cclxuXHJcbiAgcGFuVG9Cb3VuZHMobGF0TG5nOiBtYXBUeXBlcy5MYXRMbmdCb3VuZHMgfCBtYXBUeXBlcy5MYXRMbmdCb3VuZHNMaXRlcmFsLCBwYWRkaW5nPzogbnVtYmVyIHwgbWFwVHlwZXMuUGFkZGluZyk6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX21hcC50aGVuKChtYXApID0+IG1hcC5wYW5Ub0JvdW5kcyhsYXRMbmcsIHBhZGRpbmcpKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJldHVybnMgdGhlIG5hdGl2ZSBHb29nbGUgTWFwcyBNYXAgaW5zdGFuY2UuIEJlIGNhcmVmdWwgd2hlbiB1c2luZyB0aGlzIGluc3RhbmNlIGRpcmVjdGx5LlxyXG4gICAqL1xyXG4gIGdldE5hdGl2ZU1hcCgpOiBQcm9taXNlPG1hcFR5cGVzLkdvb2dsZU1hcD4geyByZXR1cm4gdGhpcy5fbWFwOyB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRyaWdnZXJzIHRoZSBnaXZlbiBldmVudCBuYW1lIG9uIHRoZSBtYXAgaW5zdGFuY2UuXHJcbiAgICovXHJcbiAgdHJpZ2dlck1hcEV2ZW50KGV2ZW50TmFtZTogc3RyaW5nKTogUHJvbWlzZTx2b2lkPiB7XHJcbiAgICByZXR1cm4gdGhpcy5fbWFwLnRoZW4oKG0pID0+IGdvb2dsZS5tYXBzLmV2ZW50LnRyaWdnZXIobSwgZXZlbnROYW1lKSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==