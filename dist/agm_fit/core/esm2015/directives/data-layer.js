import * as tslib_1 from "tslib";
var AgmDataLayer_1;
import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { DataLayerManager } from './../services/managers/data-layer-manager';
let layerId = 0;
/**
 * AgmDataLayer enables the user to add data layers to the map.
 *
 * ### Example
 * ```typescript
 * import { Component } from 'angular2/core';
 * import { AgmMap, AgmDataLayer } from
 * 'angular-google-maps/core';
 *
 * @Component({
 *  selector: 'my-map-cmp',
 *  directives: [AgmMap, AgmDataLayer],
 *  styles: [`
 *    .agm-container {
 *      height: 300px;
 *    }
 * `],
 *  template: `
 * <agm-map [latitude]="lat" [longitude]="lng" [zoom]="zoom">
 * 	  <agm-data-layer [geoJson]="geoJsonObject" (layerClick)="clicked($event)" [style]="styleFunc">
 * 	  </agm-data-layer>
 * </agm-map>
 *  `
 * })
 * export class MyMapCmp {
 *   lat: number = -25.274449;
 *   lng: number = 133.775060;
 *   zoom: number = 5;
 *
 * clicked(clickEvent) {
 *    console.log(clickEvent);
 *  }
 *
 *  styleFunc(feature) {
 *    return ({
 *      clickable: false,
 *      fillColor: feature.getProperty('color'),
 *      strokeWeight: 1
 *    });
 *  }
 *
 *  geoJsonObject: Object = {
 *    "type": "FeatureCollection",
 *    "features": [
 *      {
 *        "type": "Feature",
 *        "properties": {
 *          "letter": "G",
 *          "color": "blue",
 *          "rank": "7",
 *          "ascii": "71"
 *        },
 *        "geometry": {
 *          "type": "Polygon",
 *          "coordinates": [
 *            [
 *              [123.61, -22.14], [122.38, -21.73], [121.06, -21.69], [119.66, -22.22], [119.00, -23.40],
 *              [118.65, -24.76], [118.43, -26.07], [118.78, -27.56], [119.22, -28.57], [120.23, -29.49],
 *              [121.77, -29.87], [123.57, -29.64], [124.45, -29.03], [124.71, -27.95], [124.80, -26.70],
 *              [124.80, -25.60], [123.61, -25.64], [122.56, -25.64], [121.72, -25.72], [121.81, -26.62],
 *              [121.86, -26.98], [122.60, -26.90], [123.57, -27.05], [123.57, -27.68], [123.35, -28.18],
 *              [122.51, -28.38], [121.77, -28.26], [121.02, -27.91], [120.49, -27.21], [120.14, -26.50],
 *              [120.10, -25.64], [120.27, -24.52], [120.67, -23.68], [121.72, -23.32], [122.43, -23.48],
 *              [123.04, -24.04], [124.54, -24.28], [124.58, -23.20], [123.61, -22.14]
 *            ]
 *          ]
 *        }
 *      },
 *      {
 *        "type": "Feature",
 *        "properties": {
 *          "letter": "o",
 *          "color": "red",
 *          "rank": "15",
 *          "ascii": "111"
 *        },
 *        "geometry": {
 *          "type": "Polygon",
 *          "coordinates": [
 *            [
 *              [128.84, -25.76], [128.18, -25.60], [127.96, -25.52], [127.88, -25.52], [127.70, -25.60],
 *              [127.26, -25.79], [126.60, -26.11], [126.16, -26.78], [126.12, -27.68], [126.21, -28.42],
 *              [126.69, -29.49], [127.74, -29.80], [128.80, -29.72], [129.41, -29.03], [129.72, -27.95],
 *              [129.68, -27.21], [129.33, -26.23], [128.84, -25.76]
 *            ],
 *            [
 *              [128.45, -27.44], [128.32, -26.94], [127.70, -26.82], [127.35, -27.05], [127.17, -27.80],
 *              [127.57, -28.22], [128.10, -28.42], [128.49, -27.80], [128.45, -27.44]
 *            ]
 *          ]
 *        }
 *      },
 *      {
 *        "type": "Feature",
 *        "properties": {
 *          "letter": "o",
 *          "color": "yellow",
 *          "rank": "15",
 *          "ascii": "111"
 *        },
 *        "geometry": {
 *          "type": "Polygon",
 *          "coordinates": [
 *            [
 *              [131.87, -25.76], [131.35, -26.07], [130.95, -26.78], [130.82, -27.64], [130.86, -28.53],
 *              [131.26, -29.22], [131.92, -29.76], [132.45, -29.87], [133.06, -29.76], [133.72, -29.34],
 *              [134.07, -28.80], [134.20, -27.91], [134.07, -27.21], [133.81, -26.31], [133.37, -25.83],
 *              [132.71, -25.64], [131.87, -25.76]
 *            ],
 *            [
 *              [133.15, -27.17], [132.71, -26.86], [132.09, -26.90], [131.74, -27.56], [131.79, -28.26],
 *              [132.36, -28.45], [132.93, -28.34], [133.15, -27.76], [133.15, -27.17]
 *            ]
 *          ]
 *        }
 *      },
 *      {
 *        "type": "Feature",
 *        "properties": {
 *          "letter": "g",
 *          "color": "blue",
 *          "rank": "7",
 *          "ascii": "103"
 *        },
 *        "geometry": {
 *          "type": "Polygon",
 *          "coordinates": [
 *            [
 *              [138.12, -25.04], [136.84, -25.16], [135.96, -25.36], [135.26, -25.99], [135, -26.90],
 *              [135.04, -27.91], [135.26, -28.88], [136.05, -29.45], [137.02, -29.49], [137.81, -29.49],
 *              [137.94, -29.99], [137.90, -31.20], [137.85, -32.24], [136.88, -32.69], [136.45, -32.36],
 *              [136.27, -31.80], [134.95, -31.84], [135.17, -32.99], [135.52, -33.43], [136.14, -33.76],
 *              [137.06, -33.83], [138.12, -33.65], [138.86, -33.21], [139.30, -32.28], [139.30, -31.24],
 *              [139.30, -30.14], [139.21, -28.96], [139.17, -28.22], [139.08, -27.41], [139.08, -26.47],
 *              [138.99, -25.40], [138.73, -25.00], [138.12, -25.04]
 *            ],
 *            [
 *              [137.50, -26.54], [136.97, -26.47], [136.49, -26.58], [136.31, -27.13], [136.31, -27.72],
 *              [136.58, -27.99], [137.50, -28.03], [137.68, -27.68], [137.59, -26.78], [137.50, -26.54]
 *            ]
 *          ]
 *        }
 *      },
 *      {
 *        "type": "Feature",
 *        "properties": {
 *          "letter": "l",
 *          "color": "green",
 *          "rank": "12",
 *          "ascii": "108"
 *        },
 *        "geometry": {
 *          "type": "Polygon",
 *          "coordinates": [
 *            [
 *              [140.14, -21.04], [140.31, -29.42], [141.67, -29.49], [141.59, -20.92], [140.14, -21.04]
 *            ]
 *          ]
 *        }
 *      },
 *      {
 *        "type": "Feature",
 *        "properties": {
 *          "letter": "e",
 *          "color": "red",
 *          "rank": "5",
 *          "ascii": "101"
 *        },
 *        "geometry": {
 *          "type": "Polygon",
 *          "coordinates": [
 *            [
 *              [144.14, -27.41], [145.67, -27.52], [146.86, -27.09], [146.82, -25.64], [146.25, -25.04],
 *              [145.45, -24.68], [144.66, -24.60], [144.09, -24.76], [143.43, -25.08], [142.99, -25.40],
 *              [142.64, -26.03], [142.64, -27.05], [142.64, -28.26], [143.30, -29.11], [144.18, -29.57],
 *              [145.41, -29.64], [146.46, -29.19], [146.64, -28.72], [146.82, -28.14], [144.84, -28.42],
 *              [144.31, -28.26], [144.14, -27.41]
 *            ],
 *            [
 *              [144.18, -26.39], [144.53, -26.58], [145.19, -26.62], [145.72, -26.35], [145.81, -25.91],
 *              [145.41, -25.68], [144.97, -25.68], [144.49, -25.64], [144, -25.99], [144.18, -26.39]
 *            ]
 *          ]
 *        }
 *      }
 *    ]
 *  };
 * }
 * ```
 */
let AgmDataLayer = AgmDataLayer_1 = class AgmDataLayer {
    constructor(_manager) {
        this._manager = _manager;
        this._addedToManager = false;
        this._id = (layerId++).toString();
        this._subscriptions = [];
        /**
         * This event is fired when a feature in the layer is clicked.
         */
        this.layerClick = new EventEmitter();
        /**
         * The geoJson to be displayed
         */
        this.geoJson = null;
    }
    ngOnInit() {
        if (this._addedToManager) {
            return;
        }
        this._manager.addDataLayer(this);
        this._addedToManager = true;
        this._addEventListeners();
    }
    _addEventListeners() {
        const listeners = [
            { name: 'click', handler: (ev) => this.layerClick.emit(ev) },
        ];
        listeners.forEach((obj) => {
            const os = this._manager.createEventObservable(obj.name, this).subscribe(obj.handler);
            this._subscriptions.push(os);
        });
    }
    /** @internal */
    id() { return this._id; }
    /** @internal */
    toString() { return `AgmDataLayer-${this._id.toString()}`; }
    /** @internal */
    ngOnDestroy() {
        this._manager.deleteDataLayer(this);
        // unsubscribe all registered observable subscriptions
        this._subscriptions.forEach(s => s.unsubscribe());
    }
    /** @internal */
    ngOnChanges(changes) {
        if (!this._addedToManager) {
            return;
        }
        var geoJsonChange = changes['geoJson'];
        if (geoJsonChange) {
            this._manager.updateGeoJson(this, geoJsonChange.currentValue);
        }
        let dataOptions = {};
        AgmDataLayer_1._dataOptionsAttributes.forEach(k => dataOptions[k] = changes.hasOwnProperty(k) ? changes[k].currentValue : this[k]);
        this._manager.setDataOptions(this, dataOptions);
    }
};
AgmDataLayer._dataOptionsAttributes = ['style'];
AgmDataLayer.ctorParameters = () => [
    { type: DataLayerManager }
];
tslib_1.__decorate([
    Output(),
    tslib_1.__metadata("design:type", EventEmitter)
], AgmDataLayer.prototype, "layerClick", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Object)
], AgmDataLayer.prototype, "geoJson", void 0);
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Function)
], AgmDataLayer.prototype, "style", void 0);
AgmDataLayer = AgmDataLayer_1 = tslib_1.__decorate([
    Directive({
        selector: 'agm-data-layer',
    }),
    tslib_1.__metadata("design:paramtypes", [DataLayerManager])
], AgmDataLayer);
export { AgmDataLayer };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1sYXllci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FnbV9maXQvY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZGF0YS1sYXllci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBZ0MsTUFBTSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUlwSCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUU3RSxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUM7QUFFaEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQTZMRztBQUlILElBQWEsWUFBWSxvQkFBekIsTUFBYSxZQUFZO0lBc0J2QixZQUFvQixRQUEwQjtRQUExQixhQUFRLEdBQVIsUUFBUSxDQUFrQjtRQW5CdEMsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsUUFBRyxHQUFXLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNyQyxtQkFBYyxHQUFtQixFQUFFLENBQUM7UUFFNUM7O1dBRUc7UUFDTyxlQUFVLEdBQWlDLElBQUksWUFBWSxFQUFrQixDQUFDO1FBRXhGOztXQUVHO1FBQ00sWUFBTyxHQUEyQixJQUFJLENBQUM7SUFPRSxDQUFDO0lBRW5ELFFBQVE7UUFDTixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDNUIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVPLGtCQUFrQjtRQUN4QixNQUFNLFNBQVMsR0FBRztZQUNoQixFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBa0IsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUU7U0FDN0UsQ0FBQztRQUNGLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUN4QixNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxnQkFBZ0I7SUFDaEIsRUFBRSxLQUFhLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFFakMsZ0JBQWdCO0lBQ2hCLFFBQVEsS0FBYSxPQUFPLGdCQUFnQixJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBRXBFLGdCQUFnQjtJQUNoQixXQUFXO1FBQ1QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsc0RBQXNEO1FBQ3RELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELGdCQUFnQjtJQUNoQixXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDekIsT0FBTztTQUNSO1FBRUQsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksYUFBYSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDL0Q7UUFFRCxJQUFJLFdBQVcsR0FBZ0IsRUFBRSxDQUFDO1FBRWxDLGNBQVksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBRSxXQUFtQixDQUFDLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFFLElBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRW5KLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztJQUNsRCxDQUFDO0NBQ0YsQ0FBQTtBQXhFZ0IsbUNBQXNCLEdBQWtCLENBQUMsT0FBTyxDQUFDLENBQUM7O1lBcUJuQyxnQkFBZ0I7O0FBWnBDO0lBQVQsTUFBTSxFQUFFO3NDQUFhLFlBQVk7Z0RBQXNEO0FBSy9FO0lBQVIsS0FBSyxFQUFFO3NDQUFVLE1BQU07NkNBQXdCO0FBS3ZDO0lBQVIsS0FBSyxFQUFFOzsyQ0FBbUI7QUFwQmhCLFlBQVk7SUFIeEIsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGdCQUFnQjtLQUMzQixDQUFDOzZDQXVCOEIsZ0JBQWdCO0dBdEJuQyxZQUFZLENBeUV4QjtTQXpFWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkNoYW5nZXMsIE9uRGVzdHJveSwgT25Jbml0LCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQgeyBEYXRhTW91c2VFdmVudCwgRGF0YU9wdGlvbnMgfSBmcm9tICcuLy4uL3NlcnZpY2VzL2dvb2dsZS1tYXBzLXR5cGVzJztcclxuaW1wb3J0IHsgRGF0YUxheWVyTWFuYWdlciB9IGZyb20gJy4vLi4vc2VydmljZXMvbWFuYWdlcnMvZGF0YS1sYXllci1tYW5hZ2VyJztcclxuXHJcbmxldCBsYXllcklkID0gMDtcclxuXHJcbi8qKlxyXG4gKiBBZ21EYXRhTGF5ZXIgZW5hYmxlcyB0aGUgdXNlciB0byBhZGQgZGF0YSBsYXllcnMgdG8gdGhlIG1hcC5cclxuICpcclxuICogIyMjIEV4YW1wbGVcclxuICogYGBgdHlwZXNjcmlwdFxyXG4gKiBpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdhbmd1bGFyMi9jb3JlJztcclxuICogaW1wb3J0IHsgQWdtTWFwLCBBZ21EYXRhTGF5ZXIgfSBmcm9tXHJcbiAqICdhbmd1bGFyLWdvb2dsZS1tYXBzL2NvcmUnO1xyXG4gKlxyXG4gKiBAQ29tcG9uZW50KHtcclxuICogIHNlbGVjdG9yOiAnbXktbWFwLWNtcCcsXHJcbiAqICBkaXJlY3RpdmVzOiBbQWdtTWFwLCBBZ21EYXRhTGF5ZXJdLFxyXG4gKiAgc3R5bGVzOiBbYFxyXG4gKiAgICAuYWdtLWNvbnRhaW5lciB7XHJcbiAqICAgICAgaGVpZ2h0OiAzMDBweDtcclxuICogICAgfVxyXG4gKiBgXSxcclxuICogIHRlbXBsYXRlOiBgXHJcbiAqIDxhZ20tbWFwIFtsYXRpdHVkZV09XCJsYXRcIiBbbG9uZ2l0dWRlXT1cImxuZ1wiIFt6b29tXT1cInpvb21cIj5cclxuICogXHQgIDxhZ20tZGF0YS1sYXllciBbZ2VvSnNvbl09XCJnZW9Kc29uT2JqZWN0XCIgKGxheWVyQ2xpY2spPVwiY2xpY2tlZCgkZXZlbnQpXCIgW3N0eWxlXT1cInN0eWxlRnVuY1wiPlxyXG4gKiBcdCAgPC9hZ20tZGF0YS1sYXllcj5cclxuICogPC9hZ20tbWFwPlxyXG4gKiAgYFxyXG4gKiB9KVxyXG4gKiBleHBvcnQgY2xhc3MgTXlNYXBDbXAge1xyXG4gKiAgIGxhdDogbnVtYmVyID0gLTI1LjI3NDQ0OTtcclxuICogICBsbmc6IG51bWJlciA9IDEzMy43NzUwNjA7XHJcbiAqICAgem9vbTogbnVtYmVyID0gNTtcclxuICpcclxuICogY2xpY2tlZChjbGlja0V2ZW50KSB7XHJcbiAqICAgIGNvbnNvbGUubG9nKGNsaWNrRXZlbnQpO1xyXG4gKiAgfVxyXG4gKlxyXG4gKiAgc3R5bGVGdW5jKGZlYXR1cmUpIHtcclxuICogICAgcmV0dXJuICh7XHJcbiAqICAgICAgY2xpY2thYmxlOiBmYWxzZSxcclxuICogICAgICBmaWxsQ29sb3I6IGZlYXR1cmUuZ2V0UHJvcGVydHkoJ2NvbG9yJyksXHJcbiAqICAgICAgc3Ryb2tlV2VpZ2h0OiAxXHJcbiAqICAgIH0pO1xyXG4gKiAgfVxyXG4gKlxyXG4gKiAgZ2VvSnNvbk9iamVjdDogT2JqZWN0ID0ge1xyXG4gKiAgICBcInR5cGVcIjogXCJGZWF0dXJlQ29sbGVjdGlvblwiLFxyXG4gKiAgICBcImZlYXR1cmVzXCI6IFtcclxuICogICAgICB7XHJcbiAqICAgICAgICBcInR5cGVcIjogXCJGZWF0dXJlXCIsXHJcbiAqICAgICAgICBcInByb3BlcnRpZXNcIjoge1xyXG4gKiAgICAgICAgICBcImxldHRlclwiOiBcIkdcIixcclxuICogICAgICAgICAgXCJjb2xvclwiOiBcImJsdWVcIixcclxuICogICAgICAgICAgXCJyYW5rXCI6IFwiN1wiLFxyXG4gKiAgICAgICAgICBcImFzY2lpXCI6IFwiNzFcIlxyXG4gKiAgICAgICAgfSxcclxuICogICAgICAgIFwiZ2VvbWV0cnlcIjoge1xyXG4gKiAgICAgICAgICBcInR5cGVcIjogXCJQb2x5Z29uXCIsXHJcbiAqICAgICAgICAgIFwiY29vcmRpbmF0ZXNcIjogW1xyXG4gKiAgICAgICAgICAgIFtcclxuICogICAgICAgICAgICAgIFsxMjMuNjEsIC0yMi4xNF0sIFsxMjIuMzgsIC0yMS43M10sIFsxMjEuMDYsIC0yMS42OV0sIFsxMTkuNjYsIC0yMi4yMl0sIFsxMTkuMDAsIC0yMy40MF0sXHJcbiAqICAgICAgICAgICAgICBbMTE4LjY1LCAtMjQuNzZdLCBbMTE4LjQzLCAtMjYuMDddLCBbMTE4Ljc4LCAtMjcuNTZdLCBbMTE5LjIyLCAtMjguNTddLCBbMTIwLjIzLCAtMjkuNDldLFxyXG4gKiAgICAgICAgICAgICAgWzEyMS43NywgLTI5Ljg3XSwgWzEyMy41NywgLTI5LjY0XSwgWzEyNC40NSwgLTI5LjAzXSwgWzEyNC43MSwgLTI3Ljk1XSwgWzEyNC44MCwgLTI2LjcwXSxcclxuICogICAgICAgICAgICAgIFsxMjQuODAsIC0yNS42MF0sIFsxMjMuNjEsIC0yNS42NF0sIFsxMjIuNTYsIC0yNS42NF0sIFsxMjEuNzIsIC0yNS43Ml0sIFsxMjEuODEsIC0yNi42Ml0sXHJcbiAqICAgICAgICAgICAgICBbMTIxLjg2LCAtMjYuOThdLCBbMTIyLjYwLCAtMjYuOTBdLCBbMTIzLjU3LCAtMjcuMDVdLCBbMTIzLjU3LCAtMjcuNjhdLCBbMTIzLjM1LCAtMjguMThdLFxyXG4gKiAgICAgICAgICAgICAgWzEyMi41MSwgLTI4LjM4XSwgWzEyMS43NywgLTI4LjI2XSwgWzEyMS4wMiwgLTI3LjkxXSwgWzEyMC40OSwgLTI3LjIxXSwgWzEyMC4xNCwgLTI2LjUwXSxcclxuICogICAgICAgICAgICAgIFsxMjAuMTAsIC0yNS42NF0sIFsxMjAuMjcsIC0yNC41Ml0sIFsxMjAuNjcsIC0yMy42OF0sIFsxMjEuNzIsIC0yMy4zMl0sIFsxMjIuNDMsIC0yMy40OF0sXHJcbiAqICAgICAgICAgICAgICBbMTIzLjA0LCAtMjQuMDRdLCBbMTI0LjU0LCAtMjQuMjhdLCBbMTI0LjU4LCAtMjMuMjBdLCBbMTIzLjYxLCAtMjIuMTRdXHJcbiAqICAgICAgICAgICAgXVxyXG4gKiAgICAgICAgICBdXHJcbiAqICAgICAgICB9XHJcbiAqICAgICAgfSxcclxuICogICAgICB7XHJcbiAqICAgICAgICBcInR5cGVcIjogXCJGZWF0dXJlXCIsXHJcbiAqICAgICAgICBcInByb3BlcnRpZXNcIjoge1xyXG4gKiAgICAgICAgICBcImxldHRlclwiOiBcIm9cIixcclxuICogICAgICAgICAgXCJjb2xvclwiOiBcInJlZFwiLFxyXG4gKiAgICAgICAgICBcInJhbmtcIjogXCIxNVwiLFxyXG4gKiAgICAgICAgICBcImFzY2lpXCI6IFwiMTExXCJcclxuICogICAgICAgIH0sXHJcbiAqICAgICAgICBcImdlb21ldHJ5XCI6IHtcclxuICogICAgICAgICAgXCJ0eXBlXCI6IFwiUG9seWdvblwiLFxyXG4gKiAgICAgICAgICBcImNvb3JkaW5hdGVzXCI6IFtcclxuICogICAgICAgICAgICBbXHJcbiAqICAgICAgICAgICAgICBbMTI4Ljg0LCAtMjUuNzZdLCBbMTI4LjE4LCAtMjUuNjBdLCBbMTI3Ljk2LCAtMjUuNTJdLCBbMTI3Ljg4LCAtMjUuNTJdLCBbMTI3LjcwLCAtMjUuNjBdLFxyXG4gKiAgICAgICAgICAgICAgWzEyNy4yNiwgLTI1Ljc5XSwgWzEyNi42MCwgLTI2LjExXSwgWzEyNi4xNiwgLTI2Ljc4XSwgWzEyNi4xMiwgLTI3LjY4XSwgWzEyNi4yMSwgLTI4LjQyXSxcclxuICogICAgICAgICAgICAgIFsxMjYuNjksIC0yOS40OV0sIFsxMjcuNzQsIC0yOS44MF0sIFsxMjguODAsIC0yOS43Ml0sIFsxMjkuNDEsIC0yOS4wM10sIFsxMjkuNzIsIC0yNy45NV0sXHJcbiAqICAgICAgICAgICAgICBbMTI5LjY4LCAtMjcuMjFdLCBbMTI5LjMzLCAtMjYuMjNdLCBbMTI4Ljg0LCAtMjUuNzZdXHJcbiAqICAgICAgICAgICAgXSxcclxuICogICAgICAgICAgICBbXHJcbiAqICAgICAgICAgICAgICBbMTI4LjQ1LCAtMjcuNDRdLCBbMTI4LjMyLCAtMjYuOTRdLCBbMTI3LjcwLCAtMjYuODJdLCBbMTI3LjM1LCAtMjcuMDVdLCBbMTI3LjE3LCAtMjcuODBdLFxyXG4gKiAgICAgICAgICAgICAgWzEyNy41NywgLTI4LjIyXSwgWzEyOC4xMCwgLTI4LjQyXSwgWzEyOC40OSwgLTI3LjgwXSwgWzEyOC40NSwgLTI3LjQ0XVxyXG4gKiAgICAgICAgICAgIF1cclxuICogICAgICAgICAgXVxyXG4gKiAgICAgICAgfVxyXG4gKiAgICAgIH0sXHJcbiAqICAgICAge1xyXG4gKiAgICAgICAgXCJ0eXBlXCI6IFwiRmVhdHVyZVwiLFxyXG4gKiAgICAgICAgXCJwcm9wZXJ0aWVzXCI6IHtcclxuICogICAgICAgICAgXCJsZXR0ZXJcIjogXCJvXCIsXHJcbiAqICAgICAgICAgIFwiY29sb3JcIjogXCJ5ZWxsb3dcIixcclxuICogICAgICAgICAgXCJyYW5rXCI6IFwiMTVcIixcclxuICogICAgICAgICAgXCJhc2NpaVwiOiBcIjExMVwiXHJcbiAqICAgICAgICB9LFxyXG4gKiAgICAgICAgXCJnZW9tZXRyeVwiOiB7XHJcbiAqICAgICAgICAgIFwidHlwZVwiOiBcIlBvbHlnb25cIixcclxuICogICAgICAgICAgXCJjb29yZGluYXRlc1wiOiBbXHJcbiAqICAgICAgICAgICAgW1xyXG4gKiAgICAgICAgICAgICAgWzEzMS44NywgLTI1Ljc2XSwgWzEzMS4zNSwgLTI2LjA3XSwgWzEzMC45NSwgLTI2Ljc4XSwgWzEzMC44MiwgLTI3LjY0XSwgWzEzMC44NiwgLTI4LjUzXSxcclxuICogICAgICAgICAgICAgIFsxMzEuMjYsIC0yOS4yMl0sIFsxMzEuOTIsIC0yOS43Nl0sIFsxMzIuNDUsIC0yOS44N10sIFsxMzMuMDYsIC0yOS43Nl0sIFsxMzMuNzIsIC0yOS4zNF0sXHJcbiAqICAgICAgICAgICAgICBbMTM0LjA3LCAtMjguODBdLCBbMTM0LjIwLCAtMjcuOTFdLCBbMTM0LjA3LCAtMjcuMjFdLCBbMTMzLjgxLCAtMjYuMzFdLCBbMTMzLjM3LCAtMjUuODNdLFxyXG4gKiAgICAgICAgICAgICAgWzEzMi43MSwgLTI1LjY0XSwgWzEzMS44NywgLTI1Ljc2XVxyXG4gKiAgICAgICAgICAgIF0sXHJcbiAqICAgICAgICAgICAgW1xyXG4gKiAgICAgICAgICAgICAgWzEzMy4xNSwgLTI3LjE3XSwgWzEzMi43MSwgLTI2Ljg2XSwgWzEzMi4wOSwgLTI2LjkwXSwgWzEzMS43NCwgLTI3LjU2XSwgWzEzMS43OSwgLTI4LjI2XSxcclxuICogICAgICAgICAgICAgIFsxMzIuMzYsIC0yOC40NV0sIFsxMzIuOTMsIC0yOC4zNF0sIFsxMzMuMTUsIC0yNy43Nl0sIFsxMzMuMTUsIC0yNy4xN11cclxuICogICAgICAgICAgICBdXHJcbiAqICAgICAgICAgIF1cclxuICogICAgICAgIH1cclxuICogICAgICB9LFxyXG4gKiAgICAgIHtcclxuICogICAgICAgIFwidHlwZVwiOiBcIkZlYXR1cmVcIixcclxuICogICAgICAgIFwicHJvcGVydGllc1wiOiB7XHJcbiAqICAgICAgICAgIFwibGV0dGVyXCI6IFwiZ1wiLFxyXG4gKiAgICAgICAgICBcImNvbG9yXCI6IFwiYmx1ZVwiLFxyXG4gKiAgICAgICAgICBcInJhbmtcIjogXCI3XCIsXHJcbiAqICAgICAgICAgIFwiYXNjaWlcIjogXCIxMDNcIlxyXG4gKiAgICAgICAgfSxcclxuICogICAgICAgIFwiZ2VvbWV0cnlcIjoge1xyXG4gKiAgICAgICAgICBcInR5cGVcIjogXCJQb2x5Z29uXCIsXHJcbiAqICAgICAgICAgIFwiY29vcmRpbmF0ZXNcIjogW1xyXG4gKiAgICAgICAgICAgIFtcclxuICogICAgICAgICAgICAgIFsxMzguMTIsIC0yNS4wNF0sIFsxMzYuODQsIC0yNS4xNl0sIFsxMzUuOTYsIC0yNS4zNl0sIFsxMzUuMjYsIC0yNS45OV0sIFsxMzUsIC0yNi45MF0sXHJcbiAqICAgICAgICAgICAgICBbMTM1LjA0LCAtMjcuOTFdLCBbMTM1LjI2LCAtMjguODhdLCBbMTM2LjA1LCAtMjkuNDVdLCBbMTM3LjAyLCAtMjkuNDldLCBbMTM3LjgxLCAtMjkuNDldLFxyXG4gKiAgICAgICAgICAgICAgWzEzNy45NCwgLTI5Ljk5XSwgWzEzNy45MCwgLTMxLjIwXSwgWzEzNy44NSwgLTMyLjI0XSwgWzEzNi44OCwgLTMyLjY5XSwgWzEzNi40NSwgLTMyLjM2XSxcclxuICogICAgICAgICAgICAgIFsxMzYuMjcsIC0zMS44MF0sIFsxMzQuOTUsIC0zMS44NF0sIFsxMzUuMTcsIC0zMi45OV0sIFsxMzUuNTIsIC0zMy40M10sIFsxMzYuMTQsIC0zMy43Nl0sXHJcbiAqICAgICAgICAgICAgICBbMTM3LjA2LCAtMzMuODNdLCBbMTM4LjEyLCAtMzMuNjVdLCBbMTM4Ljg2LCAtMzMuMjFdLCBbMTM5LjMwLCAtMzIuMjhdLCBbMTM5LjMwLCAtMzEuMjRdLFxyXG4gKiAgICAgICAgICAgICAgWzEzOS4zMCwgLTMwLjE0XSwgWzEzOS4yMSwgLTI4Ljk2XSwgWzEzOS4xNywgLTI4LjIyXSwgWzEzOS4wOCwgLTI3LjQxXSwgWzEzOS4wOCwgLTI2LjQ3XSxcclxuICogICAgICAgICAgICAgIFsxMzguOTksIC0yNS40MF0sIFsxMzguNzMsIC0yNS4wMF0sIFsxMzguMTIsIC0yNS4wNF1cclxuICogICAgICAgICAgICBdLFxyXG4gKiAgICAgICAgICAgIFtcclxuICogICAgICAgICAgICAgIFsxMzcuNTAsIC0yNi41NF0sIFsxMzYuOTcsIC0yNi40N10sIFsxMzYuNDksIC0yNi41OF0sIFsxMzYuMzEsIC0yNy4xM10sIFsxMzYuMzEsIC0yNy43Ml0sXHJcbiAqICAgICAgICAgICAgICBbMTM2LjU4LCAtMjcuOTldLCBbMTM3LjUwLCAtMjguMDNdLCBbMTM3LjY4LCAtMjcuNjhdLCBbMTM3LjU5LCAtMjYuNzhdLCBbMTM3LjUwLCAtMjYuNTRdXHJcbiAqICAgICAgICAgICAgXVxyXG4gKiAgICAgICAgICBdXHJcbiAqICAgICAgICB9XHJcbiAqICAgICAgfSxcclxuICogICAgICB7XHJcbiAqICAgICAgICBcInR5cGVcIjogXCJGZWF0dXJlXCIsXHJcbiAqICAgICAgICBcInByb3BlcnRpZXNcIjoge1xyXG4gKiAgICAgICAgICBcImxldHRlclwiOiBcImxcIixcclxuICogICAgICAgICAgXCJjb2xvclwiOiBcImdyZWVuXCIsXHJcbiAqICAgICAgICAgIFwicmFua1wiOiBcIjEyXCIsXHJcbiAqICAgICAgICAgIFwiYXNjaWlcIjogXCIxMDhcIlxyXG4gKiAgICAgICAgfSxcclxuICogICAgICAgIFwiZ2VvbWV0cnlcIjoge1xyXG4gKiAgICAgICAgICBcInR5cGVcIjogXCJQb2x5Z29uXCIsXHJcbiAqICAgICAgICAgIFwiY29vcmRpbmF0ZXNcIjogW1xyXG4gKiAgICAgICAgICAgIFtcclxuICogICAgICAgICAgICAgIFsxNDAuMTQsIC0yMS4wNF0sIFsxNDAuMzEsIC0yOS40Ml0sIFsxNDEuNjcsIC0yOS40OV0sIFsxNDEuNTksIC0yMC45Ml0sIFsxNDAuMTQsIC0yMS4wNF1cclxuICogICAgICAgICAgICBdXHJcbiAqICAgICAgICAgIF1cclxuICogICAgICAgIH1cclxuICogICAgICB9LFxyXG4gKiAgICAgIHtcclxuICogICAgICAgIFwidHlwZVwiOiBcIkZlYXR1cmVcIixcclxuICogICAgICAgIFwicHJvcGVydGllc1wiOiB7XHJcbiAqICAgICAgICAgIFwibGV0dGVyXCI6IFwiZVwiLFxyXG4gKiAgICAgICAgICBcImNvbG9yXCI6IFwicmVkXCIsXHJcbiAqICAgICAgICAgIFwicmFua1wiOiBcIjVcIixcclxuICogICAgICAgICAgXCJhc2NpaVwiOiBcIjEwMVwiXHJcbiAqICAgICAgICB9LFxyXG4gKiAgICAgICAgXCJnZW9tZXRyeVwiOiB7XHJcbiAqICAgICAgICAgIFwidHlwZVwiOiBcIlBvbHlnb25cIixcclxuICogICAgICAgICAgXCJjb29yZGluYXRlc1wiOiBbXHJcbiAqICAgICAgICAgICAgW1xyXG4gKiAgICAgICAgICAgICAgWzE0NC4xNCwgLTI3LjQxXSwgWzE0NS42NywgLTI3LjUyXSwgWzE0Ni44NiwgLTI3LjA5XSwgWzE0Ni44MiwgLTI1LjY0XSwgWzE0Ni4yNSwgLTI1LjA0XSxcclxuICogICAgICAgICAgICAgIFsxNDUuNDUsIC0yNC42OF0sIFsxNDQuNjYsIC0yNC42MF0sIFsxNDQuMDksIC0yNC43Nl0sIFsxNDMuNDMsIC0yNS4wOF0sIFsxNDIuOTksIC0yNS40MF0sXHJcbiAqICAgICAgICAgICAgICBbMTQyLjY0LCAtMjYuMDNdLCBbMTQyLjY0LCAtMjcuMDVdLCBbMTQyLjY0LCAtMjguMjZdLCBbMTQzLjMwLCAtMjkuMTFdLCBbMTQ0LjE4LCAtMjkuNTddLFxyXG4gKiAgICAgICAgICAgICAgWzE0NS40MSwgLTI5LjY0XSwgWzE0Ni40NiwgLTI5LjE5XSwgWzE0Ni42NCwgLTI4LjcyXSwgWzE0Ni44MiwgLTI4LjE0XSwgWzE0NC44NCwgLTI4LjQyXSxcclxuICogICAgICAgICAgICAgIFsxNDQuMzEsIC0yOC4yNl0sIFsxNDQuMTQsIC0yNy40MV1cclxuICogICAgICAgICAgICBdLFxyXG4gKiAgICAgICAgICAgIFtcclxuICogICAgICAgICAgICAgIFsxNDQuMTgsIC0yNi4zOV0sIFsxNDQuNTMsIC0yNi41OF0sIFsxNDUuMTksIC0yNi42Ml0sIFsxNDUuNzIsIC0yNi4zNV0sIFsxNDUuODEsIC0yNS45MV0sXHJcbiAqICAgICAgICAgICAgICBbMTQ1LjQxLCAtMjUuNjhdLCBbMTQ0Ljk3LCAtMjUuNjhdLCBbMTQ0LjQ5LCAtMjUuNjRdLCBbMTQ0LCAtMjUuOTldLCBbMTQ0LjE4LCAtMjYuMzldXHJcbiAqICAgICAgICAgICAgXVxyXG4gKiAgICAgICAgICBdXHJcbiAqICAgICAgICB9XHJcbiAqICAgICAgfVxyXG4gKiAgICBdXHJcbiAqICB9O1xyXG4gKiB9XHJcbiAqIGBgYFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6ICdhZ20tZGF0YS1sYXllcicsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBZ21EYXRhTGF5ZXIgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzIHtcclxuICBwcml2YXRlIHN0YXRpYyBfZGF0YU9wdGlvbnNBdHRyaWJ1dGVzOiBBcnJheTxzdHJpbmc+ID0gWydzdHlsZSddO1xyXG5cclxuICBwcml2YXRlIF9hZGRlZFRvTWFuYWdlciA9IGZhbHNlO1xyXG4gIHByaXZhdGUgX2lkOiBzdHJpbmcgPSAobGF5ZXJJZCsrKS50b1N0cmluZygpO1xyXG4gIHByaXZhdGUgX3N1YnNjcmlwdGlvbnM6IFN1YnNjcmlwdGlvbltdID0gW107XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoaXMgZXZlbnQgaXMgZmlyZWQgd2hlbiBhIGZlYXR1cmUgaW4gdGhlIGxheWVyIGlzIGNsaWNrZWQuXHJcbiAgICovXHJcbiAgQE91dHB1dCgpIGxheWVyQ2xpY2s6IEV2ZW50RW1pdHRlcjxEYXRhTW91c2VFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyPERhdGFNb3VzZUV2ZW50PigpO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgZ2VvSnNvbiB0byBiZSBkaXNwbGF5ZWRcclxuICAgKi9cclxuICBASW5wdXQoKSBnZW9Kc29uOiBPYmplY3QgfCBzdHJpbmcgfCBudWxsID0gbnVsbDtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIGxheWVyJ3Mgc3R5bGUgZnVuY3Rpb24uXHJcbiAgICovXHJcbiAgQElucHV0KCkgc3R5bGU6ICgpID0+IHZvaWQ7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX21hbmFnZXI6IERhdGFMYXllck1hbmFnZXIpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIGlmICh0aGlzLl9hZGRlZFRvTWFuYWdlcikge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICB0aGlzLl9tYW5hZ2VyLmFkZERhdGFMYXllcih0aGlzKTtcclxuICAgIHRoaXMuX2FkZGVkVG9NYW5hZ2VyID0gdHJ1ZTtcclxuICAgIHRoaXMuX2FkZEV2ZW50TGlzdGVuZXJzKCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9hZGRFdmVudExpc3RlbmVycygpIHtcclxuICAgIGNvbnN0IGxpc3RlbmVycyA9IFtcclxuICAgICAgeyBuYW1lOiAnY2xpY2snLCBoYW5kbGVyOiAoZXY6IERhdGFNb3VzZUV2ZW50KSA9PiB0aGlzLmxheWVyQ2xpY2suZW1pdChldikgfSxcclxuICAgIF07XHJcbiAgICBsaXN0ZW5lcnMuZm9yRWFjaCgob2JqKSA9PiB7XHJcbiAgICAgIGNvbnN0IG9zID0gdGhpcy5fbWFuYWdlci5jcmVhdGVFdmVudE9ic2VydmFibGUob2JqLm5hbWUsIHRoaXMpLnN1YnNjcmliZShvYmouaGFuZGxlcik7XHJcbiAgICAgIHRoaXMuX3N1YnNjcmlwdGlvbnMucHVzaChvcyk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKiBAaW50ZXJuYWwgKi9cclxuICBpZCgpOiBzdHJpbmcgeyByZXR1cm4gdGhpcy5faWQ7IH1cclxuXHJcbiAgLyoqIEBpbnRlcm5hbCAqL1xyXG4gIHRvU3RyaW5nKCk6IHN0cmluZyB7IHJldHVybiBgQWdtRGF0YUxheWVyLSR7dGhpcy5faWQudG9TdHJpbmcoKX1gOyB9XHJcblxyXG4gIC8qKiBAaW50ZXJuYWwgKi9cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMuX21hbmFnZXIuZGVsZXRlRGF0YUxheWVyKHRoaXMpO1xyXG4gICAgLy8gdW5zdWJzY3JpYmUgYWxsIHJlZ2lzdGVyZWQgb2JzZXJ2YWJsZSBzdWJzY3JpcHRpb25zXHJcbiAgICB0aGlzLl9zdWJzY3JpcHRpb25zLmZvckVhY2gocyA9PiBzLnVuc3Vic2NyaWJlKCkpO1xyXG4gIH1cclxuXHJcbiAgLyoqIEBpbnRlcm5hbCAqL1xyXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgIGlmICghdGhpcy5fYWRkZWRUb01hbmFnZXIpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBnZW9Kc29uQ2hhbmdlID0gY2hhbmdlc1snZ2VvSnNvbiddO1xyXG4gICAgaWYgKGdlb0pzb25DaGFuZ2UpIHtcclxuICAgICAgdGhpcy5fbWFuYWdlci51cGRhdGVHZW9Kc29uKHRoaXMsIGdlb0pzb25DaGFuZ2UuY3VycmVudFZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBsZXQgZGF0YU9wdGlvbnM6IERhdGFPcHRpb25zID0ge307XHJcblxyXG4gICAgQWdtRGF0YUxheWVyLl9kYXRhT3B0aW9uc0F0dHJpYnV0ZXMuZm9yRWFjaChrID0+IChkYXRhT3B0aW9ucyBhcyBhbnkpW2tdID0gY2hhbmdlcy5oYXNPd25Qcm9wZXJ0eShrKSA/IGNoYW5nZXNba10uY3VycmVudFZhbHVlIDogKHRoaXMgYXMgYW55KVtrXSk7XHJcblxyXG4gICAgdGhpcy5fbWFuYWdlci5zZXREYXRhT3B0aW9ucyh0aGlzLCBkYXRhT3B0aW9ucyk7XHJcbiAgfVxyXG59XHJcbiJdfQ==