import * as tslib_1 from "tslib";
import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { createMVCEventObservable } from '../../utils/mvcarray-utils';
import { GoogleMapsAPIWrapper } from '../google-maps-api-wrapper';
var PolylineManager = /** @class */ (function () {
    function PolylineManager(_mapsWrapper, _zone) {
        this._mapsWrapper = _mapsWrapper;
        this._zone = _zone;
        this._polylines = new Map();
    }
    PolylineManager_1 = PolylineManager;
    PolylineManager._convertPoints = function (line) {
        var path = line._getPoints().map(function (point) {
            return { lat: point.latitude, lng: point.longitude };
        });
        return path;
    };
    PolylineManager._convertPath = function (path) {
        var symbolPath = google.maps.SymbolPath[path];
        if (typeof symbolPath === 'number') {
            return symbolPath;
        }
        else {
            return path;
        }
    };
    PolylineManager._convertIcons = function (line) {
        var icons = line._getIcons().map(function (agmIcon) { return ({
            fixedRotation: agmIcon.fixedRotation,
            offset: agmIcon.offset,
            repeat: agmIcon.repeat,
            icon: {
                anchor: new google.maps.Point(agmIcon.anchorX, agmIcon.anchorY),
                fillColor: agmIcon.fillColor,
                fillOpacity: agmIcon.fillOpacity,
                path: PolylineManager_1._convertPath(agmIcon.path),
                rotation: agmIcon.rotation,
                scale: agmIcon.scale,
                strokeColor: agmIcon.strokeColor,
                strokeOpacity: agmIcon.strokeOpacity,
                strokeWeight: agmIcon.strokeWeight,
            },
        }); });
        // prune undefineds;
        icons.forEach(function (icon) {
            Object.entries(icon).forEach(function (_a) {
                var _b = tslib_1.__read(_a, 2), key = _b[0], val = _b[1];
                if (typeof val === 'undefined') {
                    delete icon[key];
                }
            });
            if (typeof icon.icon.anchor.x === 'undefined' ||
                typeof icon.icon.anchor.y === 'undefined') {
                delete icon.icon.anchor;
            }
        });
        return icons;
    };
    PolylineManager.prototype.addPolyline = function (line) {
        var _this = this;
        var polylinePromise = this._mapsWrapper.getNativeMap()
            .then(function () { return [PolylineManager_1._convertPoints(line),
            PolylineManager_1._convertIcons(line)]; })
            .then(function (_a) {
            var _b = tslib_1.__read(_a, 2), path = _b[0], icons = _b[1];
            return _this._mapsWrapper.createPolyline({
                clickable: line.clickable,
                draggable: line.draggable,
                editable: line.editable,
                geodesic: line.geodesic,
                strokeColor: line.strokeColor,
                strokeOpacity: line.strokeOpacity,
                strokeWeight: line.strokeWeight,
                visible: line.visible,
                zIndex: line.zIndex,
                path: path,
                icons: icons,
            });
        });
        this._polylines.set(line, polylinePromise);
    };
    PolylineManager.prototype.updatePolylinePoints = function (line) {
        var _this = this;
        var path = PolylineManager_1._convertPoints(line);
        var m = this._polylines.get(line);
        if (m == null) {
            return Promise.resolve();
        }
        return m.then(function (l) { return _this._zone.run(function () { l.setPath(path); }); });
    };
    PolylineManager.prototype.updateIconSequences = function (line) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var icons, m;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._mapsWrapper.getNativeMap()];
                    case 1:
                        _a.sent();
                        icons = PolylineManager_1._convertIcons(line);
                        m = this._polylines.get(line);
                        if (m == null) {
                            return [2 /*return*/];
                        }
                        return [2 /*return*/, m.then(function (l) { return _this._zone.run(function () { return l.setOptions({ icons: icons }); }); })];
                }
            });
        });
    };
    PolylineManager.prototype.setPolylineOptions = function (line, options) {
        return this._polylines.get(line).then(function (l) { l.setOptions(options); });
    };
    PolylineManager.prototype.deletePolyline = function (line) {
        var _this = this;
        var m = this._polylines.get(line);
        if (m == null) {
            return Promise.resolve();
        }
        return m.then(function (l) {
            return _this._zone.run(function () {
                l.setMap(null);
                _this._polylines.delete(line);
            });
        });
    };
    PolylineManager.prototype.getMVCPath = function (agmPolyline) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var polyline;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._polylines.get(agmPolyline)];
                    case 1:
                        polyline = _a.sent();
                        return [2 /*return*/, polyline.getPath()];
                }
            });
        });
    };
    PolylineManager.prototype.getPath = function (agmPolyline) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getMVCPath(agmPolyline)];
                    case 1: return [2 /*return*/, (_a.sent()).getArray()];
                }
            });
        });
    };
    PolylineManager.prototype.createEventObservable = function (eventName, line) {
        var _this = this;
        return new Observable(function (observer) {
            _this._polylines.get(line).then(function (l) {
                l.addListener(eventName, function (e) { return _this._zone.run(function () { return observer.next(e); }); });
            });
        });
    };
    PolylineManager.prototype.createPathEventObservable = function (line) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var mvcPath;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getMVCPath(line)];
                    case 1:
                        mvcPath = _a.sent();
                        return [2 /*return*/, createMVCEventObservable(mvcPath)];
                }
            });
        });
    };
    var PolylineManager_1;
    PolylineManager.ctorParameters = function () { return [
        { type: GoogleMapsAPIWrapper },
        { type: NgZone }
    ]; };
    PolylineManager = PolylineManager_1 = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [GoogleMapsAPIWrapper, NgZone])
    ], PolylineManager);
    return PolylineManager;
}());
export { PolylineManager };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9seWxpbmUtbWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FnbV9maXQvY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL21hbmFnZXJzL3BvbHlsaW5lLW1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQVksTUFBTSxNQUFNLENBQUM7QUFJNUMsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDdEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFNbEU7SUFJRSx5QkFBb0IsWUFBa0MsRUFBVSxLQUFhO1FBQXpELGlCQUFZLEdBQVosWUFBWSxDQUFzQjtRQUFVLFVBQUssR0FBTCxLQUFLLENBQVE7UUFIckUsZUFBVSxHQUNkLElBQUksR0FBRyxFQUFrQyxDQUFDO0lBRWtDLENBQUM7d0JBSnRFLGVBQWU7SUFNWCw4QkFBYyxHQUE3QixVQUE4QixJQUFpQjtRQUM3QyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUMsS0FBdUI7WUFDekQsT0FBTyxFQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsU0FBUyxFQUFrQixDQUFDO1FBQ3RFLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRWMsNEJBQVksR0FBM0IsVUFBNEIsSUFDRztRQUM3QixJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNoRCxJQUFJLE9BQU8sVUFBVSxLQUFLLFFBQVEsRUFBRTtZQUNsQyxPQUFPLFVBQVUsQ0FBQztTQUNuQjthQUFLO1lBQ0osT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFYyw2QkFBYSxHQUE1QixVQUE2QixJQUFpQjtRQUM1QyxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsQ0FBQztZQUM3QyxhQUFhLEVBQUUsT0FBTyxDQUFDLGFBQWE7WUFDcEMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxNQUFNO1lBQ3RCLE1BQU0sRUFBRSxPQUFPLENBQUMsTUFBTTtZQUN0QixJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDO2dCQUMvRCxTQUFTLEVBQUUsT0FBTyxDQUFDLFNBQVM7Z0JBQzVCLFdBQVcsRUFBRSxPQUFPLENBQUMsV0FBVztnQkFDaEMsSUFBSSxFQUFFLGlCQUFlLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hELFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUTtnQkFDMUIsS0FBSyxFQUFFLE9BQU8sQ0FBQyxLQUFLO2dCQUNwQixXQUFXLEVBQUUsT0FBTyxDQUFDLFdBQVc7Z0JBQ2hDLGFBQWEsRUFBRSxPQUFPLENBQUMsYUFBYTtnQkFDcEMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxZQUFZO2FBQ25DO1NBQ2UsQ0FBQSxFQWY0QixDQWU1QixDQUFDLENBQUM7UUFDcEIsb0JBQW9CO1FBQ3BCLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO1lBQ2hCLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsRUFBVTtvQkFBViwwQkFBVSxFQUFULFdBQUcsRUFBRSxXQUFHO2dCQUNyQyxJQUFJLE9BQU8sR0FBRyxLQUFLLFdBQVcsRUFBRTtvQkFDOUIsT0FBUSxJQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzNCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLFdBQVc7Z0JBQzNDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLFdBQVcsRUFBRTtnQkFDekMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzthQUN6QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQscUNBQVcsR0FBWCxVQUFZLElBQWlCO1FBQTdCLGlCQW1CQztRQWxCQyxJQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRTthQUN2RCxJQUFJLENBQUMsY0FBTSxPQUFBLENBQUUsaUJBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ3BDLGlCQUFlLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBRHRDLENBQ3NDLENBQUM7YUFDbEQsSUFBSSxDQUFDLFVBQUMsRUFBZ0Q7Z0JBQWhELDBCQUFnRCxFQUEvQyxZQUFJLEVBQUUsYUFBSztZQUNqQixPQUFBLEtBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDO2dCQUMvQixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0JBQ3pCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztnQkFDekIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO2dCQUN2QixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7Z0JBQ3ZCLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVztnQkFDN0IsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhO2dCQUNqQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVk7Z0JBQy9CLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTztnQkFDckIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2dCQUNuQixJQUFJLEVBQUUsSUFBSTtnQkFDVixLQUFLLEVBQUUsS0FBSzthQUNmLENBQUM7UUFaQSxDQVlBLENBQUMsQ0FBQztRQUNKLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsOENBQW9CLEdBQXBCLFVBQXFCLElBQWlCO1FBQXRDLGlCQU9DO1FBTkMsSUFBTSxJQUFJLEdBQUcsaUJBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbEQsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFO1lBQ2IsT0FBTyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDMUI7UUFDRCxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFXLElBQU8sT0FBTyxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxjQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7SUFFSyw2Q0FBbUIsR0FBekIsVUFBMEIsSUFBaUI7Ozs7Ozs0QkFDekMscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsRUFBQTs7d0JBQXRDLFNBQXNDLENBQUM7d0JBQ2pDLEtBQUssR0FBRyxpQkFBZSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDNUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNwQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUU7NEJBQ2Isc0JBQU87eUJBQ1I7d0JBQ0Qsc0JBQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGNBQU0sT0FBQSxDQUFDLENBQUMsVUFBVSxDQUFDLEVBQUMsS0FBSyxFQUFFLEtBQUssRUFBQyxDQUFDLEVBQTVCLENBQTRCLENBQUUsRUFBbkQsQ0FBbUQsQ0FBRSxFQUFDOzs7O0tBQzFFO0lBRUQsNENBQWtCLEdBQWxCLFVBQW1CLElBQWlCLEVBQUUsT0FBa0M7UUFFdEUsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFXLElBQU8sQ0FBQyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3JGLENBQUM7SUFFRCx3Q0FBYyxHQUFkLFVBQWUsSUFBaUI7UUFBaEMsaUJBV0M7UUFWQyxJQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFDYixPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUMxQjtRQUNELE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQVc7WUFDeEIsT0FBTyxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztnQkFDcEIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZixLQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMvQixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVhLG9DQUFVLEdBQXhCLFVBQXlCLFdBQXdCOzs7Ozs0QkFDOUIscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEVBQUE7O3dCQUFqRCxRQUFRLEdBQUcsU0FBc0M7d0JBQ3ZELHNCQUFPLFFBQVEsQ0FBQyxPQUFPLEVBQUUsRUFBQzs7OztLQUMzQjtJQUVLLGlDQUFPLEdBQWIsVUFBYyxXQUF3Qjs7Ozs0QkFDNUIscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsRUFBQTs0QkFBMUMsc0JBQU8sQ0FBQyxTQUFrQyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUM7Ozs7S0FDeEQ7SUFFRCwrQ0FBcUIsR0FBckIsVUFBeUIsU0FBaUIsRUFBRSxJQUFpQjtRQUE3RCxpQkFNQztRQUxDLE9BQU8sSUFBSSxVQUFVLENBQUMsVUFBQyxRQUFxQjtZQUMxQyxLQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFXO2dCQUN6QyxDQUFDLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxVQUFDLENBQUksSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGNBQU0sT0FBQSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFoQixDQUFnQixDQUFDLEVBQXRDLENBQXNDLENBQUMsQ0FBQztZQUM3RSxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVLLG1EQUF5QixHQUEvQixVQUFnQyxJQUFpQjs7Ozs7NEJBQy9CLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUE7O3dCQUFyQyxPQUFPLEdBQUcsU0FBMkI7d0JBQzNDLHNCQUFPLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxFQUFDOzs7O0tBQzFDOzs7Z0JBaklpQyxvQkFBb0I7Z0JBQWlCLE1BQU07O0lBSmxFLGVBQWU7UUFEM0IsVUFBVSxFQUFFO2lEQUt1QixvQkFBb0IsRUFBaUIsTUFBTTtPQUpsRSxlQUFlLENBc0kzQjtJQUFELHNCQUFDO0NBQUEsQUF0SUQsSUFzSUM7U0F0SVksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBPYnNlcnZlciB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgQWdtUG9seWxpbmUsIFBhdGhFdmVudCB9IGZyb20gJy4uLy4uL2RpcmVjdGl2ZXMvcG9seWxpbmUnO1xyXG5pbXBvcnQgeyBBZ21Qb2x5bGluZVBvaW50IH0gZnJvbSAnLi4vLi4vZGlyZWN0aXZlcy9wb2x5bGluZS1wb2ludCc7XHJcbmltcG9ydCB7IGNyZWF0ZU1WQ0V2ZW50T2JzZXJ2YWJsZSB9IGZyb20gJy4uLy4uL3V0aWxzL212Y2FycmF5LXV0aWxzJztcclxuaW1wb3J0IHsgR29vZ2xlTWFwc0FQSVdyYXBwZXIgfSBmcm9tICcuLi9nb29nbGUtbWFwcy1hcGktd3JhcHBlcic7XHJcbmltcG9ydCB7IEljb25TZXF1ZW5jZSwgTGF0TG5nLCBMYXRMbmdMaXRlcmFsLCBNVkNBcnJheSwgUG9seWxpbmUgfSBmcm9tICcuLi9nb29nbGUtbWFwcy10eXBlcyc7XHJcblxyXG5kZWNsYXJlIHZhciBnb29nbGU6IGFueTtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFBvbHlsaW5lTWFuYWdlciB7XHJcbiAgcHJpdmF0ZSBfcG9seWxpbmVzOiBNYXA8QWdtUG9seWxpbmUsIFByb21pc2U8UG9seWxpbmU+PiA9XHJcbiAgICAgIG5ldyBNYXA8QWdtUG9seWxpbmUsIFByb21pc2U8UG9seWxpbmU+PigpO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9tYXBzV3JhcHBlcjogR29vZ2xlTWFwc0FQSVdyYXBwZXIsIHByaXZhdGUgX3pvbmU6IE5nWm9uZSkge31cclxuXHJcbiAgcHJpdmF0ZSBzdGF0aWMgX2NvbnZlcnRQb2ludHMobGluZTogQWdtUG9seWxpbmUpOiBBcnJheTxMYXRMbmdMaXRlcmFsPiB7XHJcbiAgICBjb25zdCBwYXRoID0gbGluZS5fZ2V0UG9pbnRzKCkubWFwKChwb2ludDogQWdtUG9seWxpbmVQb2ludCkgPT4ge1xyXG4gICAgICByZXR1cm4ge2xhdDogcG9pbnQubGF0aXR1ZGUsIGxuZzogcG9pbnQubG9uZ2l0dWRlfSBhcyBMYXRMbmdMaXRlcmFsO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gcGF0aDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc3RhdGljIF9jb252ZXJ0UGF0aChwYXRoOiAnQ0lSQ0xFJyB8ICdCQUNLV0FSRF9DTE9TRURfQVJST1cnIHwgJ0JBQ0tXQVJEX09QRU5fQVJST1cnIHwgJ0ZPUldBUkRfQ0xPU0VEX0FSUk9XJyB8XHJcbiAgJ0ZPUldBUkRfQ0xPU0VEX0FSUk9XJyB8IHN0cmluZyk6IG51bWJlciB8IHN0cmluZ3tcclxuICAgIGNvbnN0IHN5bWJvbFBhdGggPSBnb29nbGUubWFwcy5TeW1ib2xQYXRoW3BhdGhdO1xyXG4gICAgaWYgKHR5cGVvZiBzeW1ib2xQYXRoID09PSAnbnVtYmVyJykge1xyXG4gICAgICByZXR1cm4gc3ltYm9sUGF0aDtcclxuICAgIH0gZWxzZXtcclxuICAgICAgcmV0dXJuIHBhdGg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHN0YXRpYyBfY29udmVydEljb25zKGxpbmU6IEFnbVBvbHlsaW5lKTogQXJyYXk8SWNvblNlcXVlbmNlPiB7XHJcbiAgICBjb25zdCBpY29ucyA9IGxpbmUuX2dldEljb25zKCkubWFwKGFnbUljb24gPT4gKHtcclxuICAgICAgZml4ZWRSb3RhdGlvbjogYWdtSWNvbi5maXhlZFJvdGF0aW9uLFxyXG4gICAgICBvZmZzZXQ6IGFnbUljb24ub2Zmc2V0LFxyXG4gICAgICByZXBlYXQ6IGFnbUljb24ucmVwZWF0LFxyXG4gICAgICBpY29uOiB7XHJcbiAgICAgICAgYW5jaG9yOiBuZXcgZ29vZ2xlLm1hcHMuUG9pbnQoYWdtSWNvbi5hbmNob3JYLCBhZ21JY29uLmFuY2hvclkpLFxyXG4gICAgICAgIGZpbGxDb2xvcjogYWdtSWNvbi5maWxsQ29sb3IsXHJcbiAgICAgICAgZmlsbE9wYWNpdHk6IGFnbUljb24uZmlsbE9wYWNpdHksXHJcbiAgICAgICAgcGF0aDogUG9seWxpbmVNYW5hZ2VyLl9jb252ZXJ0UGF0aChhZ21JY29uLnBhdGgpLFxyXG4gICAgICAgIHJvdGF0aW9uOiBhZ21JY29uLnJvdGF0aW9uLFxyXG4gICAgICAgIHNjYWxlOiBhZ21JY29uLnNjYWxlLFxyXG4gICAgICAgIHN0cm9rZUNvbG9yOiBhZ21JY29uLnN0cm9rZUNvbG9yLFxyXG4gICAgICAgIHN0cm9rZU9wYWNpdHk6IGFnbUljb24uc3Ryb2tlT3BhY2l0eSxcclxuICAgICAgICBzdHJva2VXZWlnaHQ6IGFnbUljb24uc3Ryb2tlV2VpZ2h0LFxyXG4gICAgICB9LFxyXG4gICAgfSBhcyBJY29uU2VxdWVuY2UpKTtcclxuICAgIC8vIHBydW5lIHVuZGVmaW5lZHM7XHJcbiAgICBpY29ucy5mb3JFYWNoKGljb24gPT4ge1xyXG4gICAgICBPYmplY3QuZW50cmllcyhpY29uKS5mb3JFYWNoKChba2V5LCB2YWxdKSA9PiB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiB2YWwgPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICBkZWxldGUgKGljb24gYXMgYW55KVtrZXldO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGlmICh0eXBlb2YgaWNvbi5pY29uLmFuY2hvci54ID09PSAndW5kZWZpbmVkJyB8fFxyXG4gICAgICAgIHR5cGVvZiBpY29uLmljb24uYW5jaG9yLnkgPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICBkZWxldGUgaWNvbi5pY29uLmFuY2hvcjtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBpY29ucztcclxuICB9XHJcblxyXG4gIGFkZFBvbHlsaW5lKGxpbmU6IEFnbVBvbHlsaW5lKSB7XHJcbiAgICBjb25zdCBwb2x5bGluZVByb21pc2UgPSB0aGlzLl9tYXBzV3JhcHBlci5nZXROYXRpdmVNYXAoKVxyXG4gICAgLnRoZW4oKCkgPT4gWyBQb2x5bGluZU1hbmFnZXIuX2NvbnZlcnRQb2ludHMobGluZSksXHJcbiAgICAgICAgICAgICAgICAgIFBvbHlsaW5lTWFuYWdlci5fY29udmVydEljb25zKGxpbmUpXSlcclxuICAgIC50aGVuKChbcGF0aCwgaWNvbnNdOiBbTGF0TG5nTGl0ZXJhbFtdLCBJY29uU2VxdWVuY2VbXV0pID0+XHJcbiAgICAgIHRoaXMuX21hcHNXcmFwcGVyLmNyZWF0ZVBvbHlsaW5lKHtcclxuICAgICAgICBjbGlja2FibGU6IGxpbmUuY2xpY2thYmxlLFxyXG4gICAgICAgIGRyYWdnYWJsZTogbGluZS5kcmFnZ2FibGUsXHJcbiAgICAgICAgZWRpdGFibGU6IGxpbmUuZWRpdGFibGUsXHJcbiAgICAgICAgZ2VvZGVzaWM6IGxpbmUuZ2VvZGVzaWMsXHJcbiAgICAgICAgc3Ryb2tlQ29sb3I6IGxpbmUuc3Ryb2tlQ29sb3IsXHJcbiAgICAgICAgc3Ryb2tlT3BhY2l0eTogbGluZS5zdHJva2VPcGFjaXR5LFxyXG4gICAgICAgIHN0cm9rZVdlaWdodDogbGluZS5zdHJva2VXZWlnaHQsXHJcbiAgICAgICAgdmlzaWJsZTogbGluZS52aXNpYmxlLFxyXG4gICAgICAgIHpJbmRleDogbGluZS56SW5kZXgsXHJcbiAgICAgICAgcGF0aDogcGF0aCxcclxuICAgICAgICBpY29uczogaWNvbnMsXHJcbiAgICB9KSk7XHJcbiAgICB0aGlzLl9wb2x5bGluZXMuc2V0KGxpbmUsIHBvbHlsaW5lUHJvbWlzZSk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVQb2x5bGluZVBvaW50cyhsaW5lOiBBZ21Qb2x5bGluZSk6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgY29uc3QgcGF0aCA9IFBvbHlsaW5lTWFuYWdlci5fY29udmVydFBvaW50cyhsaW5lKTtcclxuICAgIGNvbnN0IG0gPSB0aGlzLl9wb2x5bGluZXMuZ2V0KGxpbmUpO1xyXG4gICAgaWYgKG0gPT0gbnVsbCkge1xyXG4gICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbS50aGVuKChsOiBQb2x5bGluZSkgPT4geyByZXR1cm4gdGhpcy5fem9uZS5ydW4oKCkgPT4geyBsLnNldFBhdGgocGF0aCk7IH0pOyB9KTtcclxuICB9XHJcblxyXG4gIGFzeW5jIHVwZGF0ZUljb25TZXF1ZW5jZXMobGluZTogQWdtUG9seWxpbmUpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIGF3YWl0IHRoaXMuX21hcHNXcmFwcGVyLmdldE5hdGl2ZU1hcCgpO1xyXG4gICAgY29uc3QgaWNvbnMgPSBQb2x5bGluZU1hbmFnZXIuX2NvbnZlcnRJY29ucyhsaW5lKTtcclxuICAgIGNvbnN0IG0gPSB0aGlzLl9wb2x5bGluZXMuZ2V0KGxpbmUpO1xyXG4gICAgaWYgKG0gPT0gbnVsbCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbS50aGVuKGwgPT4gdGhpcy5fem9uZS5ydW4oKCkgPT4gbC5zZXRPcHRpb25zKHtpY29uczogaWNvbnN9KSApICk7XHJcbiAgfVxyXG5cclxuICBzZXRQb2x5bGluZU9wdGlvbnMobGluZTogQWdtUG9seWxpbmUsIG9wdGlvbnM6IHtbcHJvcE5hbWU6IHN0cmluZ106IGFueX0pOlxyXG4gICAgICBQcm9taXNlPHZvaWQ+IHtcclxuICAgIHJldHVybiB0aGlzLl9wb2x5bGluZXMuZ2V0KGxpbmUpLnRoZW4oKGw6IFBvbHlsaW5lKSA9PiB7IGwuc2V0T3B0aW9ucyhvcHRpb25zKTsgfSk7XHJcbiAgfVxyXG5cclxuICBkZWxldGVQb2x5bGluZShsaW5lOiBBZ21Qb2x5bGluZSk6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgY29uc3QgbSA9IHRoaXMuX3BvbHlsaW5lcy5nZXQobGluZSk7XHJcbiAgICBpZiAobSA9PSBudWxsKSB7XHJcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcclxuICAgIH1cclxuICAgIHJldHVybiBtLnRoZW4oKGw6IFBvbHlsaW5lKSA9PiB7XHJcbiAgICAgIHJldHVybiB0aGlzLl96b25lLnJ1bigoKSA9PiB7XHJcbiAgICAgICAgbC5zZXRNYXAobnVsbCk7XHJcbiAgICAgICAgdGhpcy5fcG9seWxpbmVzLmRlbGV0ZShsaW5lKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgYXN5bmMgZ2V0TVZDUGF0aChhZ21Qb2x5bGluZTogQWdtUG9seWxpbmUpOiBQcm9taXNlPE1WQ0FycmF5PExhdExuZz4+IHtcclxuICAgIGNvbnN0IHBvbHlsaW5lID0gYXdhaXQgdGhpcy5fcG9seWxpbmVzLmdldChhZ21Qb2x5bGluZSk7XHJcbiAgICByZXR1cm4gcG9seWxpbmUuZ2V0UGF0aCgpO1xyXG4gIH1cclxuXHJcbiAgYXN5bmMgZ2V0UGF0aChhZ21Qb2x5bGluZTogQWdtUG9seWxpbmUpOiBQcm9taXNlPEFycmF5PExhdExuZz4+IHtcclxuICAgIHJldHVybiAoYXdhaXQgdGhpcy5nZXRNVkNQYXRoKGFnbVBvbHlsaW5lKSkuZ2V0QXJyYXkoKTtcclxuICB9XHJcblxyXG4gIGNyZWF0ZUV2ZW50T2JzZXJ2YWJsZTxUPihldmVudE5hbWU6IHN0cmluZywgbGluZTogQWdtUG9seWxpbmUpOiBPYnNlcnZhYmxlPFQ+IHtcclxuICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXI6IE9ic2VydmVyPFQ+KSA9PiB7XHJcbiAgICAgIHRoaXMuX3BvbHlsaW5lcy5nZXQobGluZSkudGhlbigobDogUG9seWxpbmUpID0+IHtcclxuICAgICAgICBsLmFkZExpc3RlbmVyKGV2ZW50TmFtZSwgKGU6IFQpID0+IHRoaXMuX3pvbmUucnVuKCgpID0+IG9ic2VydmVyLm5leHQoZSkpKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGFzeW5jIGNyZWF0ZVBhdGhFdmVudE9ic2VydmFibGUobGluZTogQWdtUG9seWxpbmUpOiBQcm9taXNlPE9ic2VydmFibGU8UGF0aEV2ZW50Pj4ge1xyXG4gICAgY29uc3QgbXZjUGF0aCA9IGF3YWl0IHRoaXMuZ2V0TVZDUGF0aChsaW5lKTtcclxuICAgIHJldHVybiBjcmVhdGVNVkNFdmVudE9ic2VydmFibGUobXZjUGF0aCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==