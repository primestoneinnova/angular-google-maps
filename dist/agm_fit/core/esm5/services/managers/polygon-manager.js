import * as tslib_1 from "tslib";
import { Injectable, NgZone } from '@angular/core';
import { merge, Observable } from 'rxjs';
import { map, skip, startWith, switchMap } from 'rxjs/operators';
import { createMVCEventObservable } from '../../utils/mvcarray-utils';
import { GoogleMapsAPIWrapper } from '../google-maps-api-wrapper';
var PolygonManager = /** @class */ (function () {
    function PolygonManager(_mapsWrapper, _zone) {
        this._mapsWrapper = _mapsWrapper;
        this._zone = _zone;
        this._polygons = new Map();
    }
    PolygonManager.prototype.addPolygon = function (path) {
        var polygonPromise = this._mapsWrapper.createPolygon({
            clickable: path.clickable,
            draggable: path.draggable,
            editable: path.editable,
            fillColor: path.fillColor,
            fillOpacity: path.fillOpacity,
            geodesic: path.geodesic,
            paths: path.paths,
            strokeColor: path.strokeColor,
            strokeOpacity: path.strokeOpacity,
            strokeWeight: path.strokeWeight,
            visible: path.visible,
            zIndex: path.zIndex,
        });
        this._polygons.set(path, polygonPromise);
    };
    PolygonManager.prototype.updatePolygon = function (polygon) {
        var _this = this;
        var m = this._polygons.get(polygon);
        if (m == null) {
            return Promise.resolve();
        }
        return m.then(function (l) { return _this._zone.run(function () { l.setPaths(polygon.paths); }); });
    };
    PolygonManager.prototype.setPolygonOptions = function (path, options) {
        return this._polygons.get(path).then(function (l) { l.setOptions(options); });
    };
    PolygonManager.prototype.deletePolygon = function (paths) {
        var _this = this;
        var m = this._polygons.get(paths);
        if (m == null) {
            return Promise.resolve();
        }
        return m.then(function (l) {
            return _this._zone.run(function () {
                l.setMap(null);
                _this._polygons.delete(paths);
            });
        });
    };
    PolygonManager.prototype.getPath = function (polygon) {
        return this._polygons.get(polygon)
            .then(function (polygon) { return polygon.getPath().getArray(); });
    };
    PolygonManager.prototype.getPaths = function (polygon) {
        return this._polygons.get(polygon)
            .then(function (polygon) { return polygon.getPaths().getArray().map(function (p) { return p.getArray(); }); });
    };
    PolygonManager.prototype.createEventObservable = function (eventName, path) {
        var _this = this;
        return new Observable(function (observer) {
            _this._polygons.get(path).then(function (l) {
                l.addListener(eventName, function (e) { return _this._zone.run(function () { return observer.next(e); }); });
            });
        });
    };
    PolygonManager.prototype.createPathEventObservable = function (agmPolygon) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var polygon, paths, pathsChanges$;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._polygons.get(agmPolygon)];
                    case 1:
                        polygon = _a.sent();
                        paths = polygon.getPaths();
                        pathsChanges$ = createMVCEventObservable(paths);
                        return [2 /*return*/, pathsChanges$.pipe(startWith({ newArr: paths.getArray() }), // in order to subscribe to them all
                            switchMap(function (parentMVEvent) { return merge.apply(void 0, tslib_1.__spread(// rest parameter
                            parentMVEvent.newArr.map(function (chMVC, index) {
                                return createMVCEventObservable(chMVC)
                                    .pipe(map(function (chMVCEvent) { return ({ parentMVEvent: parentMVEvent, chMVCEvent: chMVCEvent, pathIndex: index }); }));
                            }))).pipe(startWith({ parentMVEvent: parentMVEvent, chMVCEvent: null, pathIndex: null })); }), // start the merged ob with an event signinifing change to parent
                            skip(1), // skip the manually added event
                            map(function (_a) {
                                var parentMVEvent = _a.parentMVEvent, chMVCEvent = _a.chMVCEvent, pathIndex = _a.pathIndex;
                                var retVal;
                                if (!chMVCEvent) {
                                    retVal = {
                                        newArr: parentMVEvent.newArr.map(function (subArr) { return subArr.getArray().map(function (latLng) { return latLng.toJSON(); }); }),
                                        eventName: parentMVEvent.evName,
                                        index: parentMVEvent.index,
                                    };
                                    if (parentMVEvent.previous) {
                                        retVal.previous = parentMVEvent.previous.getArray();
                                    }
                                }
                                else {
                                    retVal = {
                                        newArr: parentMVEvent.newArr.map(function (subArr) { return subArr.getArray().map(function (latLng) { return latLng.toJSON(); }); }),
                                        pathIndex: pathIndex,
                                        eventName: chMVCEvent.evName,
                                        index: chMVCEvent.index,
                                    };
                                    if (chMVCEvent.previous) {
                                        retVal.previous = chMVCEvent.previous;
                                    }
                                }
                                return retVal;
                            }))];
                }
            });
        });
    };
    PolygonManager.ctorParameters = function () { return [
        { type: GoogleMapsAPIWrapper },
        { type: NgZone }
    ]; };
    PolygonManager = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [GoogleMapsAPIWrapper, NgZone])
    ], PolygonManager);
    return PolygonManager;
}());
export { PolygonManager };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9seWdvbi1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWdtX2ZpdC9jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvbWFuYWdlcnMvcG9seWdvbi1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBWSxNQUFNLE1BQU0sQ0FBQztBQUNuRCxPQUFPLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHakUsT0FBTyxFQUFFLHdCQUF3QixFQUFZLE1BQU0sNEJBQTRCLENBQUM7QUFDaEYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFJbEU7SUFJRSx3QkFBb0IsWUFBa0MsRUFBVSxLQUFhO1FBQXpELGlCQUFZLEdBQVosWUFBWSxDQUFzQjtRQUFVLFVBQUssR0FBTCxLQUFLLENBQVE7UUFIckUsY0FBUyxHQUNmLElBQUksR0FBRyxFQUFnQyxDQUFDO0lBRXVDLENBQUM7SUFFbEYsbUNBQVUsR0FBVixVQUFXLElBQWdCO1FBQ3pCLElBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDO1lBQ3JELFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztZQUN6QixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7WUFDekIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztZQUN6QixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7WUFDN0IsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7WUFDN0IsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhO1lBQ2pDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTtZQUMvQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87WUFDckIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO1NBQ3BCLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsc0NBQWEsR0FBYixVQUFjLE9BQW1CO1FBQWpDLGlCQU1DO1FBTEMsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFO1lBQ2IsT0FBTyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDMUI7UUFDRCxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFVLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxjQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQXBELENBQW9ELENBQUMsQ0FBQztJQUN0RixDQUFDO0lBRUQsMENBQWlCLEdBQWpCLFVBQWtCLElBQWdCLEVBQUUsT0FBb0M7UUFDdEUsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFVLElBQU8sQ0FBQyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ25GLENBQUM7SUFFRCxzQ0FBYSxHQUFiLFVBQWMsS0FBaUI7UUFBL0IsaUJBV0M7UUFWQyxJQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFDYixPQUFPLE9BQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUMxQjtRQUNELE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQVU7WUFDdkIsT0FBTyxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztnQkFDcEIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDZixLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMvQixDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGdDQUFPLEdBQVAsVUFBUSxPQUFtQjtRQUN6QixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUMvQixJQUFJLENBQUMsVUFBQyxPQUFPLElBQUssT0FBQSxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQTVCLENBQTRCLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsaUNBQVEsR0FBUixVQUFTLE9BQW1CO1FBQzFCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO2FBQy9CLElBQUksQ0FBQyxVQUFDLE9BQU8sSUFBSyxPQUFBLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQVosQ0FBWSxDQUFDLEVBQXRELENBQXNELENBQUMsQ0FBQztJQUMvRSxDQUFDO0lBRUQsOENBQXFCLEdBQXJCLFVBQXlCLFNBQWlCLEVBQUUsSUFBZ0I7UUFBNUQsaUJBTUM7UUFMQyxPQUFPLElBQUksVUFBVSxDQUFDLFVBQUMsUUFBcUI7WUFDMUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBVTtnQkFDdkMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsVUFBQyxDQUFJLElBQUssT0FBQSxLQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxjQUFNLE9BQUEsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBaEIsQ0FBZ0IsQ0FBQyxFQUF0QyxDQUFzQyxDQUFDLENBQUM7WUFDN0UsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFSyxrREFBeUIsR0FBL0IsVUFBZ0MsVUFBc0I7Ozs7OzRCQUNwQyxxQkFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBQTs7d0JBQTlDLE9BQU8sR0FBRyxTQUFvQzt3QkFDOUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQzt3QkFDM0IsYUFBYSxHQUFHLHdCQUF3QixDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUN0RCxzQkFBTyxhQUFhLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsUUFBUSxFQUFFLEVBQWlDLENBQUMsRUFBRSxvQ0FBb0M7NEJBQ3JJLFNBQVMsQ0FBQyxVQUFBLGFBQWEsSUFBSSxPQUFBLEtBQUssZ0NBQUksaUJBQWlCOzRCQUNuRCxhQUFhLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFDLEtBQUssRUFBRSxLQUFLO2dDQUNwQyxPQUFBLHdCQUF3QixDQUFDLEtBQUssQ0FBQztxQ0FDOUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFBLFVBQVUsSUFBSSxPQUFBLENBQUMsRUFBRSxhQUFhLGVBQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBakQsQ0FBaUQsQ0FBQyxDQUFDOzRCQUQzRSxDQUMyRSxDQUFDLEdBQzdFLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxhQUFhLGVBQUEsRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLEVBSjdDLENBSTZDLENBQUMsRUFBRSxpRUFBaUU7NEJBQzVJLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxnQ0FBZ0M7NEJBQ3pDLEdBQUcsQ0FBQyxVQUFDLEVBQXdDO29DQUF0QyxnQ0FBYSxFQUFFLDBCQUFVLEVBQUUsd0JBQVM7Z0NBQ3pDLElBQUksTUFBTSxDQUFDO2dDQUNYLElBQUksQ0FBQyxVQUFVLEVBQUU7b0NBQ2YsTUFBTSxHQUFHO3dDQUNQLE1BQU0sRUFBRSxhQUFhLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxHQUFHLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxNQUFNLENBQUMsTUFBTSxFQUFFLEVBQWYsQ0FBZSxDQUFDLEVBQWhELENBQWdELENBQUM7d0NBQzVGLFNBQVMsRUFBRSxhQUFhLENBQUMsTUFBTTt3Q0FDL0IsS0FBSyxFQUFFLGFBQWEsQ0FBQyxLQUFLO3FDQUNhLENBQUM7b0NBQzFDLElBQUksYUFBYSxDQUFDLFFBQVEsRUFBRTt3Q0FDMUIsTUFBTSxDQUFDLFFBQVEsR0FBSSxhQUFhLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO3FDQUN0RDtpQ0FDRjtxQ0FBTTtvQ0FDTCxNQUFNLEdBQUc7d0NBQ1AsTUFBTSxFQUFFLGFBQWEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxNQUFNLEVBQUUsRUFBZixDQUFlLENBQUMsRUFBaEQsQ0FBZ0QsQ0FBQzt3Q0FDNUYsU0FBUyxXQUFBO3dDQUNULFNBQVMsRUFBRSxVQUFVLENBQUMsTUFBTTt3Q0FDNUIsS0FBSyxFQUFFLFVBQVUsQ0FBQyxLQUFLO3FDQUNNLENBQUM7b0NBQ2hDLElBQUksVUFBVSxDQUFDLFFBQVEsRUFBRTt3Q0FDdkIsTUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDO3FDQUN2QztpQ0FDRjtnQ0FDRCxPQUFPLE1BQU0sQ0FBQzs0QkFDaEIsQ0FBQyxDQUFDLENBQUMsRUFBQzs7OztLQUNQOztnQkFsR2lDLG9CQUFvQjtnQkFBaUIsTUFBTTs7SUFKbEUsY0FBYztRQUQxQixVQUFVLEVBQUU7aURBS3VCLG9CQUFvQixFQUFpQixNQUFNO09BSmxFLGNBQWMsQ0F1RzFCO0lBQUQscUJBQUM7Q0FBQSxBQXZHRCxJQXVHQztTQXZHWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgTmdab25lIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IG1lcmdlLCBPYnNlcnZhYmxlLCBPYnNlcnZlciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBtYXAsIHNraXAsIHN0YXJ0V2l0aCwgc3dpdGNoTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHsgQWdtUG9seWdvbiwgUGF0aENoYW5nZVBvbHlnb25QYXRoRXZlbnQsIFBhdGhDb2xsZWN0aW9uQ2hhbmdlUG9seWdvblBhdGhFdmVudCwgUG9seWdvblBhdGhFdmVudCB9IGZyb20gJy4uLy4uL2RpcmVjdGl2ZXMvcG9seWdvbic7XHJcbmltcG9ydCB7IGNyZWF0ZU1WQ0V2ZW50T2JzZXJ2YWJsZSwgTVZDRXZlbnQgfSBmcm9tICcuLi8uLi91dGlscy9tdmNhcnJheS11dGlscyc7XHJcbmltcG9ydCB7IEdvb2dsZU1hcHNBUElXcmFwcGVyIH0gZnJvbSAnLi4vZ29vZ2xlLW1hcHMtYXBpLXdyYXBwZXInO1xyXG5pbXBvcnQgeyBMYXRMbmcsIE1WQ0FycmF5LCBQb2x5Z29uIH0gZnJvbSAnLi4vZ29vZ2xlLW1hcHMtdHlwZXMnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgUG9seWdvbk1hbmFnZXIge1xyXG4gIHByaXZhdGUgX3BvbHlnb25zOiBNYXA8QWdtUG9seWdvbiwgUHJvbWlzZTxQb2x5Z29uPj4gPVxyXG4gICAgbmV3IE1hcDxBZ21Qb2x5Z29uLCBQcm9taXNlPFBvbHlnb24+PigpO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9tYXBzV3JhcHBlcjogR29vZ2xlTWFwc0FQSVdyYXBwZXIsIHByaXZhdGUgX3pvbmU6IE5nWm9uZSkgeyB9XHJcblxyXG4gIGFkZFBvbHlnb24ocGF0aDogQWdtUG9seWdvbikge1xyXG4gICAgY29uc3QgcG9seWdvblByb21pc2UgPSB0aGlzLl9tYXBzV3JhcHBlci5jcmVhdGVQb2x5Z29uKHtcclxuICAgICAgY2xpY2thYmxlOiBwYXRoLmNsaWNrYWJsZSxcclxuICAgICAgZHJhZ2dhYmxlOiBwYXRoLmRyYWdnYWJsZSxcclxuICAgICAgZWRpdGFibGU6IHBhdGguZWRpdGFibGUsXHJcbiAgICAgIGZpbGxDb2xvcjogcGF0aC5maWxsQ29sb3IsXHJcbiAgICAgIGZpbGxPcGFjaXR5OiBwYXRoLmZpbGxPcGFjaXR5LFxyXG4gICAgICBnZW9kZXNpYzogcGF0aC5nZW9kZXNpYyxcclxuICAgICAgcGF0aHM6IHBhdGgucGF0aHMsXHJcbiAgICAgIHN0cm9rZUNvbG9yOiBwYXRoLnN0cm9rZUNvbG9yLFxyXG4gICAgICBzdHJva2VPcGFjaXR5OiBwYXRoLnN0cm9rZU9wYWNpdHksXHJcbiAgICAgIHN0cm9rZVdlaWdodDogcGF0aC5zdHJva2VXZWlnaHQsXHJcbiAgICAgIHZpc2libGU6IHBhdGgudmlzaWJsZSxcclxuICAgICAgekluZGV4OiBwYXRoLnpJbmRleCxcclxuICAgIH0pO1xyXG4gICAgdGhpcy5fcG9seWdvbnMuc2V0KHBhdGgsIHBvbHlnb25Qcm9taXNlKTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZVBvbHlnb24ocG9seWdvbjogQWdtUG9seWdvbik6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgY29uc3QgbSA9IHRoaXMuX3BvbHlnb25zLmdldChwb2x5Z29uKTtcclxuICAgIGlmIChtID09IG51bGwpIHtcclxuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSgpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG0udGhlbigobDogUG9seWdvbikgPT4gdGhpcy5fem9uZS5ydW4oKCkgPT4geyBsLnNldFBhdGhzKHBvbHlnb24ucGF0aHMpOyB9KSk7XHJcbiAgfVxyXG5cclxuICBzZXRQb2x5Z29uT3B0aW9ucyhwYXRoOiBBZ21Qb2x5Z29uLCBvcHRpb25zOiB7IFtwcm9wTmFtZTogc3RyaW5nXTogYW55IH0pOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIHJldHVybiB0aGlzLl9wb2x5Z29ucy5nZXQocGF0aCkudGhlbigobDogUG9seWdvbikgPT4geyBsLnNldE9wdGlvbnMob3B0aW9ucyk7IH0pO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlUG9seWdvbihwYXRoczogQWdtUG9seWdvbik6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgY29uc3QgbSA9IHRoaXMuX3BvbHlnb25zLmdldChwYXRocyk7XHJcbiAgICBpZiAobSA9PSBudWxsKSB7XHJcbiAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoKTtcclxuICAgIH1cclxuICAgIHJldHVybiBtLnRoZW4oKGw6IFBvbHlnb24pID0+IHtcclxuICAgICAgcmV0dXJuIHRoaXMuX3pvbmUucnVuKCgpID0+IHtcclxuICAgICAgICBsLnNldE1hcChudWxsKTtcclxuICAgICAgICB0aGlzLl9wb2x5Z29ucy5kZWxldGUocGF0aHMpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0UGF0aChwb2x5Z29uOiBBZ21Qb2x5Z29uKTogUHJvbWlzZTxBcnJheTxMYXRMbmc+PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fcG9seWdvbnMuZ2V0KHBvbHlnb24pXHJcbiAgICAgIC50aGVuKChwb2x5Z29uKSA9PiBwb2x5Z29uLmdldFBhdGgoKS5nZXRBcnJheSgpKTtcclxuICB9XHJcblxyXG4gIGdldFBhdGhzKHBvbHlnb246IEFnbVBvbHlnb24pOiBQcm9taXNlPEFycmF5PEFycmF5PExhdExuZz4+PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fcG9seWdvbnMuZ2V0KHBvbHlnb24pXHJcbiAgICAgIC50aGVuKChwb2x5Z29uKSA9PiBwb2x5Z29uLmdldFBhdGhzKCkuZ2V0QXJyYXkoKS5tYXAoKHApID0+IHAuZ2V0QXJyYXkoKSkpO1xyXG4gIH1cclxuXHJcbiAgY3JlYXRlRXZlbnRPYnNlcnZhYmxlPFQ+KGV2ZW50TmFtZTogc3RyaW5nLCBwYXRoOiBBZ21Qb2x5Z29uKTogT2JzZXJ2YWJsZTxUPiB7XHJcbiAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoKG9ic2VydmVyOiBPYnNlcnZlcjxUPikgPT4ge1xyXG4gICAgICB0aGlzLl9wb2x5Z29ucy5nZXQocGF0aCkudGhlbigobDogUG9seWdvbikgPT4ge1xyXG4gICAgICAgIGwuYWRkTGlzdGVuZXIoZXZlbnROYW1lLCAoZTogVCkgPT4gdGhpcy5fem9uZS5ydW4oKCkgPT4gb2JzZXJ2ZXIubmV4dChlKSkpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgYXN5bmMgY3JlYXRlUGF0aEV2ZW50T2JzZXJ2YWJsZShhZ21Qb2x5Z29uOiBBZ21Qb2x5Z29uKTogUHJvbWlzZTxPYnNlcnZhYmxlPFBvbHlnb25QYXRoRXZlbnQ8YW55Pj4+IHtcclxuICAgIGNvbnN0IHBvbHlnb24gPSBhd2FpdCB0aGlzLl9wb2x5Z29ucy5nZXQoYWdtUG9seWdvbik7XHJcbiAgICBjb25zdCBwYXRocyA9IHBvbHlnb24uZ2V0UGF0aHMoKTtcclxuICAgIGNvbnN0IHBhdGhzQ2hhbmdlcyQgPSBjcmVhdGVNVkNFdmVudE9ic2VydmFibGUocGF0aHMpO1xyXG4gICAgcmV0dXJuIHBhdGhzQ2hhbmdlcyQucGlwZShzdGFydFdpdGgoKHsgbmV3QXJyOiBwYXRocy5nZXRBcnJheSgpIH0gYXMgTVZDRXZlbnQ8TVZDQXJyYXk8TGF0TG5nPj4pKSwgLy8gaW4gb3JkZXIgdG8gc3Vic2NyaWJlIHRvIHRoZW0gYWxsXHJcbiAgICAgIHN3aXRjaE1hcChwYXJlbnRNVkV2ZW50ID0+IG1lcmdlKC4uLi8vIHJlc3QgcGFyYW1ldGVyXHJcbiAgICAgICAgcGFyZW50TVZFdmVudC5uZXdBcnIubWFwKChjaE1WQywgaW5kZXgpID0+XHJcbiAgICAgICAgICBjcmVhdGVNVkNFdmVudE9ic2VydmFibGUoY2hNVkMpXHJcbiAgICAgICAgICAucGlwZShtYXAoY2hNVkNFdmVudCA9PiAoeyBwYXJlbnRNVkV2ZW50LCBjaE1WQ0V2ZW50LCBwYXRoSW5kZXg6IGluZGV4IH0pKSkpKVxyXG4gICAgICAgIC5waXBlKHN0YXJ0V2l0aCh7IHBhcmVudE1WRXZlbnQsIGNoTVZDRXZlbnQ6IG51bGwsIHBhdGhJbmRleDogbnVsbCB9KSkpLCAvLyBzdGFydCB0aGUgbWVyZ2VkIG9iIHdpdGggYW4gZXZlbnQgc2lnbmluaWZpbmcgY2hhbmdlIHRvIHBhcmVudFxyXG4gICAgICBza2lwKDEpLCAvLyBza2lwIHRoZSBtYW51YWxseSBhZGRlZCBldmVudFxyXG4gICAgICBtYXAoKHsgcGFyZW50TVZFdmVudCwgY2hNVkNFdmVudCwgcGF0aEluZGV4IH0pID0+IHtcclxuICAgICAgICBsZXQgcmV0VmFsO1xyXG4gICAgICAgIGlmICghY2hNVkNFdmVudCkge1xyXG4gICAgICAgICAgcmV0VmFsID0ge1xyXG4gICAgICAgICAgICBuZXdBcnI6IHBhcmVudE1WRXZlbnQubmV3QXJyLm1hcChzdWJBcnIgPT4gc3ViQXJyLmdldEFycmF5KCkubWFwKGxhdExuZyA9PiBsYXRMbmcudG9KU09OKCkpKSxcclxuICAgICAgICAgICAgZXZlbnROYW1lOiBwYXJlbnRNVkV2ZW50LmV2TmFtZSxcclxuICAgICAgICAgICAgaW5kZXg6IHBhcmVudE1WRXZlbnQuaW5kZXgsXHJcbiAgICAgICAgICB9IGFzIFBhdGhDb2xsZWN0aW9uQ2hhbmdlUG9seWdvblBhdGhFdmVudDtcclxuICAgICAgICAgIGlmIChwYXJlbnRNVkV2ZW50LnByZXZpb3VzKSB7XHJcbiAgICAgICAgICAgIHJldFZhbC5wcmV2aW91cyA9ICBwYXJlbnRNVkV2ZW50LnByZXZpb3VzLmdldEFycmF5KCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJldFZhbCA9IHtcclxuICAgICAgICAgICAgbmV3QXJyOiBwYXJlbnRNVkV2ZW50Lm5ld0Fyci5tYXAoc3ViQXJyID0+IHN1YkFyci5nZXRBcnJheSgpLm1hcChsYXRMbmcgPT4gbGF0TG5nLnRvSlNPTigpKSksXHJcbiAgICAgICAgICAgIHBhdGhJbmRleCxcclxuICAgICAgICAgICAgZXZlbnROYW1lOiBjaE1WQ0V2ZW50LmV2TmFtZSxcclxuICAgICAgICAgICAgaW5kZXg6IGNoTVZDRXZlbnQuaW5kZXgsXHJcbiAgICAgICAgICB9IGFzIFBhdGhDaGFuZ2VQb2x5Z29uUGF0aEV2ZW50O1xyXG4gICAgICAgICAgaWYgKGNoTVZDRXZlbnQucHJldmlvdXMpIHtcclxuICAgICAgICAgICAgcmV0VmFsLnByZXZpb3VzID0gY2hNVkNFdmVudC5wcmV2aW91cztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJldFZhbDtcclxuICAgICAgfSkpO1xyXG4gIH1cclxufVxyXG4iXX0=