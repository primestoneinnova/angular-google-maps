import * as tslib_1 from "tslib";
import { AgmMarker, GoogleMapsAPIWrapper, MapsAPILoader, MarkerManager } from 'agm_fit/core';
import { Component, ContentChild, ElementRef, EventEmitter, Host, Input, Optional, Output, SkipSelf, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
var AgmSnazzyInfoWindow = /** @class */ (function () {
    function AgmSnazzyInfoWindow(_marker, _wrapper, _manager, _loader) {
        this._marker = _marker;
        this._wrapper = _wrapper;
        this._manager = _manager;
        this._loader = _loader;
        /**
         * Changes the open status of the snazzy info window.
         */
        this.isOpen = false;
        /**
         * Emits when the open status changes.
         */
        this.isOpenChange = new EventEmitter();
        /**
         * Choose where you want the info window to be displayed, relative to the marker.
         */
        this.placement = 'top';
        /**
         * The max width in pixels of the info window.
         */
        this.maxWidth = 200;
        /**
         * The max height in pixels of the info window.
         */
        this.maxHeight = 200;
        /**
         * Determines if the info window will open when the marker is clicked.
         * An internal listener is added to the Google Maps click event which calls the open() method.
         */
        this.openOnMarkerClick = true;
        /**
         * Determines if the info window will close when the map is clicked. An internal listener is added to the Google Maps click event which calls the close() method.
         * This will not activate on the Google Maps drag event when the user is panning the map.
         */
        this.closeOnMapClick = true;
        /**
         * Determines if the info window will close when any other Snazzy Info Window is opened.
         */
        this.closeWhenOthersOpen = false;
        /**
         * Determines if the info window will show a close button.
         */
        this.showCloseButton = true;
        /**
         * Determines if the info window will be panned into view when opened.
         */
        this.panOnOpen = true;
        /**
         * Emits before the info window opens.
         */
        this.beforeOpen = new EventEmitter();
        /**
         * Emits before the info window closes.
         */
        this.afterClose = new EventEmitter();
        this._snazzyInfoWindowInitialized = null;
    }
    /**
     * @internal
     */
    AgmSnazzyInfoWindow.prototype.ngOnChanges = function (changes) {
        if (this._nativeSnazzyInfoWindow == null) {
            return;
        }
        if ('isOpen' in changes && this.isOpen) {
            this._openInfoWindow();
        }
        else if ('isOpen' in changes && !this.isOpen) {
            this._closeInfoWindow();
        }
        if (('latitude' in changes || 'longitude' in changes) && this._marker == null) {
            this._updatePosition();
        }
    };
    /**
     * @internal
     */
    AgmSnazzyInfoWindow.prototype.ngAfterViewInit = function () {
        var _this = this;
        var m = this._manager != null ? this._manager.getNativeMarker(this._marker) : null;
        this._snazzyInfoWindowInitialized = this._loader.load()
            .then(function () { return require('snazzy-info-window'); })
            .then(function (module) { return Promise.all([module, m, _this._wrapper.getNativeMap()]); })
            .then(function (elems) {
            var options = {
                map: elems[2],
                content: '',
                placement: _this.placement,
                maxWidth: _this.maxWidth,
                maxHeight: _this.maxHeight,
                backgroundColor: _this.backgroundColor,
                padding: _this.padding,
                border: _this.border,
                borderRadius: _this.borderRadius,
                fontColor: _this.fontColor,
                pointer: _this.pointer,
                shadow: _this.shadow,
                closeOnMapClick: _this.closeOnMapClick,
                openOnMarkerClick: _this.openOnMarkerClick,
                closeWhenOthersOpen: _this.closeWhenOthersOpen,
                showCloseButton: _this.showCloseButton,
                panOnOpen: _this.panOnOpen,
                wrapperClass: _this.wrapperClass,
                callbacks: {
                    beforeOpen: function () {
                        _this._createViewContent();
                        _this.beforeOpen.emit();
                    },
                    afterOpen: function () {
                        _this.isOpenChange.emit(_this.openStatus());
                    },
                    afterClose: function () {
                        _this.afterClose.emit();
                        _this.isOpenChange.emit(_this.openStatus());
                    },
                },
            };
            if (elems[1] != null) {
                options.marker = elems[1];
            }
            else {
                options.position = {
                    lat: _this.latitude,
                    lng: _this.longitude,
                };
            }
            _this._nativeSnazzyInfoWindow = new elems[0](options);
        });
        this._snazzyInfoWindowInitialized.then(function () {
            if (_this.isOpen) {
                _this._openInfoWindow();
            }
        });
    };
    AgmSnazzyInfoWindow.prototype._openInfoWindow = function () {
        var _this = this;
        this._snazzyInfoWindowInitialized.then(function () {
            _this._createViewContent();
            _this._nativeSnazzyInfoWindow.open();
        });
    };
    AgmSnazzyInfoWindow.prototype._closeInfoWindow = function () {
        var _this = this;
        this._snazzyInfoWindowInitialized.then(function () {
            _this._nativeSnazzyInfoWindow.close();
        });
    };
    AgmSnazzyInfoWindow.prototype._createViewContent = function () {
        if (this._viewContainerRef.length === 1) {
            return;
        }
        var evr = this._viewContainerRef.createEmbeddedView(this._templateRef);
        this._nativeSnazzyInfoWindow.setContent(this._outerWrapper.nativeElement);
        // we have to run this in a separate cycle.
        setTimeout(function () {
            evr.detectChanges();
        });
    };
    AgmSnazzyInfoWindow.prototype._updatePosition = function () {
        this._nativeSnazzyInfoWindow.setPosition({
            lat: this.latitude,
            lng: this.longitude,
        });
    };
    /**
     * Returns true when the Snazzy Info Window is initialized and open.
     */
    AgmSnazzyInfoWindow.prototype.openStatus = function () {
        return this._nativeSnazzyInfoWindow && this._nativeSnazzyInfoWindow.isOpen();
    };
    /**
     * @internal
     */
    AgmSnazzyInfoWindow.prototype.ngOnDestroy = function () {
        if (this._nativeSnazzyInfoWindow) {
            this._nativeSnazzyInfoWindow.destroy();
        }
    };
    AgmSnazzyInfoWindow.ctorParameters = function () { return [
        { type: AgmMarker, decorators: [{ type: Optional }, { type: Host }, { type: SkipSelf }] },
        { type: GoogleMapsAPIWrapper },
        { type: MarkerManager },
        { type: MapsAPILoader }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], AgmSnazzyInfoWindow.prototype, "latitude", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], AgmSnazzyInfoWindow.prototype, "longitude", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "isOpen", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], AgmSnazzyInfoWindow.prototype, "isOpenChange", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmSnazzyInfoWindow.prototype, "placement", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "maxWidth", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "maxHeight", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmSnazzyInfoWindow.prototype, "backgroundColor", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmSnazzyInfoWindow.prototype, "padding", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "border", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmSnazzyInfoWindow.prototype, "borderRadius", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmSnazzyInfoWindow.prototype, "fontColor", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmSnazzyInfoWindow.prototype, "fontSize", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "pointer", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "shadow", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "openOnMarkerClick", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "closeOnMapClick", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmSnazzyInfoWindow.prototype, "wrapperClass", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "closeWhenOthersOpen", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "showCloseButton", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], AgmSnazzyInfoWindow.prototype, "panOnOpen", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], AgmSnazzyInfoWindow.prototype, "beforeOpen", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], AgmSnazzyInfoWindow.prototype, "afterClose", void 0);
    tslib_1.__decorate([
        ViewChild('outerWrapper', { read: ElementRef, static: false }),
        tslib_1.__metadata("design:type", ElementRef)
    ], AgmSnazzyInfoWindow.prototype, "_outerWrapper", void 0);
    tslib_1.__decorate([
        ViewChild('viewContainer', { read: ViewContainerRef, static: false }),
        tslib_1.__metadata("design:type", ViewContainerRef)
    ], AgmSnazzyInfoWindow.prototype, "_viewContainerRef", void 0);
    tslib_1.__decorate([
        ContentChild(TemplateRef, { static: false }),
        tslib_1.__metadata("design:type", TemplateRef)
    ], AgmSnazzyInfoWindow.prototype, "_templateRef", void 0);
    AgmSnazzyInfoWindow = tslib_1.__decorate([
        Component({
            // tslint:disable-next-line:component-selector
            selector: 'agm-snazzy-info-window',
            template: '<div #outerWrapper><div #viewContainer></div></div><ng-content></ng-content>'
        }),
        tslib_1.__param(0, Optional()), tslib_1.__param(0, Host()), tslib_1.__param(0, SkipSelf()),
        tslib_1.__metadata("design:paramtypes", [AgmMarker,
            GoogleMapsAPIWrapper,
            MarkerManager,
            MapsAPILoader])
    ], AgmSnazzyInfoWindow);
    return AgmSnazzyInfoWindow;
}());
export { AgmSnazzyInfoWindow };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic25henp5LWluZm8td2luZG93LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWdtX2ZpdC9zbmF6enktaW5mby13aW5kb3cvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL3NuYXp6eS1pbmZvLXdpbmRvdy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxhQUFhLEVBQUUsYUFBYSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQzdGLE9BQU8sRUFBaUIsU0FBUyxFQUFFLFlBQVksRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQXdCLFFBQVEsRUFBRSxNQUFNLEVBQWlCLFFBQVEsRUFBRSxXQUFXLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBU3pOO0lBK0lFLDZCQUMwQyxPQUFrQixFQUNsRCxRQUE4QixFQUM5QixRQUF1QixFQUN2QixPQUFzQjtRQUhVLFlBQU8sR0FBUCxPQUFPLENBQVc7UUFDbEQsYUFBUSxHQUFSLFFBQVEsQ0FBc0I7UUFDOUIsYUFBUSxHQUFSLFFBQVEsQ0FBZTtRQUN2QixZQUFPLEdBQVAsT0FBTyxDQUFlO1FBdEloQzs7V0FFRztRQUNNLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFFeEI7O1dBRUc7UUFDTyxpQkFBWSxHQUEwQixJQUFJLFlBQVksRUFBVyxDQUFDO1FBRTVFOztXQUVHO1FBQ00sY0FBUyxHQUF3QyxLQUFLLENBQUM7UUFFaEU7O1dBRUc7UUFDTSxhQUFRLEdBQW9CLEdBQUcsQ0FBQztRQUV6Qzs7V0FFRztRQUNNLGNBQVMsR0FBb0IsR0FBRyxDQUFDO1FBOEMxQzs7O1dBR0c7UUFDTSxzQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFFbEM7OztXQUdHO1FBQ00sb0JBQWUsR0FBRyxJQUFJLENBQUM7UUFRaEM7O1dBRUc7UUFDTSx3QkFBbUIsR0FBRyxLQUFLLENBQUM7UUFFckM7O1dBRUc7UUFDTSxvQkFBZSxHQUFHLElBQUksQ0FBQztRQUVoQzs7V0FFRztRQUNNLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFFMUI7O1dBRUc7UUFDTyxlQUFVLEdBQXVCLElBQUksWUFBWSxFQUFRLENBQUM7UUFFcEU7O1dBRUc7UUFDTyxlQUFVLEdBQXVCLElBQUksWUFBWSxFQUFRLENBQUM7UUFrQjFELGlDQUE0QixHQUF3QixJQUFJLENBQUM7SUFPaEUsQ0FBQztJQUVKOztPQUVHO0lBQ0gseUNBQVcsR0FBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLElBQUksRUFBRTtZQUN4QyxPQUFPO1NBQ1I7UUFDRCxJQUFJLFFBQVEsSUFBSSxPQUFPLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUN0QyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDeEI7YUFBTSxJQUFJLFFBQVEsSUFBSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQzlDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1NBQ3pCO1FBQ0QsSUFBSSxDQUFDLFVBQVUsSUFBSSxPQUFPLElBQUksV0FBVyxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO1lBQzdFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUN4QjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNILDZDQUFlLEdBQWY7UUFBQSxpQkFzREM7UUFyREMsSUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3JGLElBQUksQ0FBQyw0QkFBNEIsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRTthQUNwRCxJQUFJLENBQUMsY0FBTSxPQUFBLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxFQUE3QixDQUE2QixDQUFDO2FBQ3pDLElBQUksQ0FBQyxVQUFDLE1BQVcsSUFBSyxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEtBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxFQUF0RCxDQUFzRCxDQUFDO2FBQzdFLElBQUksQ0FBQyxVQUFDLEtBQUs7WUFDVixJQUFNLE9BQU8sR0FBUTtnQkFDbkIsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsT0FBTyxFQUFFLEVBQUU7Z0JBQ1gsU0FBUyxFQUFFLEtBQUksQ0FBQyxTQUFTO2dCQUN6QixRQUFRLEVBQUUsS0FBSSxDQUFDLFFBQVE7Z0JBQ3ZCLFNBQVMsRUFBRSxLQUFJLENBQUMsU0FBUztnQkFDekIsZUFBZSxFQUFFLEtBQUksQ0FBQyxlQUFlO2dCQUNyQyxPQUFPLEVBQUUsS0FBSSxDQUFDLE9BQU87Z0JBQ3JCLE1BQU0sRUFBRSxLQUFJLENBQUMsTUFBTTtnQkFDbkIsWUFBWSxFQUFFLEtBQUksQ0FBQyxZQUFZO2dCQUMvQixTQUFTLEVBQUUsS0FBSSxDQUFDLFNBQVM7Z0JBQ3pCLE9BQU8sRUFBRSxLQUFJLENBQUMsT0FBTztnQkFDckIsTUFBTSxFQUFFLEtBQUksQ0FBQyxNQUFNO2dCQUNuQixlQUFlLEVBQUUsS0FBSSxDQUFDLGVBQWU7Z0JBQ3JDLGlCQUFpQixFQUFFLEtBQUksQ0FBQyxpQkFBaUI7Z0JBQ3pDLG1CQUFtQixFQUFFLEtBQUksQ0FBQyxtQkFBbUI7Z0JBQzdDLGVBQWUsRUFBRSxLQUFJLENBQUMsZUFBZTtnQkFDckMsU0FBUyxFQUFFLEtBQUksQ0FBQyxTQUFTO2dCQUN6QixZQUFZLEVBQUUsS0FBSSxDQUFDLFlBQVk7Z0JBQy9CLFNBQVMsRUFBRTtvQkFDVCxVQUFVLEVBQUU7d0JBQ1YsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7d0JBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQ3pCLENBQUM7b0JBQ0QsU0FBUyxFQUFFO3dCQUNULEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO29CQUM1QyxDQUFDO29CQUNELFVBQVUsRUFBRTt3QkFDVixLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUN2QixLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztvQkFDNUMsQ0FBQztpQkFDRjthQUNGLENBQUM7WUFDRixJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUU7Z0JBQ3BCLE9BQU8sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzNCO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxRQUFRLEdBQUc7b0JBQ2pCLEdBQUcsRUFBRSxLQUFJLENBQUMsUUFBUTtvQkFDbEIsR0FBRyxFQUFFLEtBQUksQ0FBQyxTQUFTO2lCQUNwQixDQUFDO2FBQ0g7WUFDRCxLQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDO1lBQ3JDLElBQUksS0FBSSxDQUFDLE1BQU0sRUFBRTtnQkFDZixLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7YUFDeEI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFUyw2Q0FBZSxHQUF6QjtRQUFBLGlCQUtDO1FBSkMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQztZQUNyQyxLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUMxQixLQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRVMsOENBQWdCLEdBQTFCO1FBQUEsaUJBSUM7UUFIQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDO1lBQ3JDLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFUyxnREFBa0IsR0FBNUI7UUFDRSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3ZDLE9BQU87U0FDUjtRQUNELElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzFFLDJDQUEyQztRQUMzQyxVQUFVLENBQUM7WUFDVCxHQUFHLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRVMsNkNBQWUsR0FBekI7UUFDRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsV0FBVyxDQUFDO1lBQ3ZDLEdBQUcsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUNsQixHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVM7U0FDcEIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsd0NBQVUsR0FBVjtRQUNFLE9BQU8sSUFBSSxDQUFDLHVCQUF1QixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUMvRSxDQUFDO0lBRUQ7O09BRUc7SUFDSCx5Q0FBVyxHQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ3hDO0lBQ0gsQ0FBQzs7Z0JBaElrRCxTQUFTLHVCQUF6RCxRQUFRLFlBQUksSUFBSSxZQUFJLFFBQVE7Z0JBQ1gsb0JBQW9CO2dCQUNwQixhQUFhO2dCQUNkLGFBQWE7O0lBOUl2QjtRQUFSLEtBQUssRUFBRTs7eURBQWtCO0lBTWpCO1FBQVIsS0FBSyxFQUFFOzswREFBbUI7SUFLbEI7UUFBUixLQUFLLEVBQUU7O3VEQUFnQjtJQUtkO1FBQVQsTUFBTSxFQUFFOzBDQUFlLFlBQVk7NkRBQXdDO0lBS25FO1FBQVIsS0FBSyxFQUFFOzswREFBd0Q7SUFLdkQ7UUFBUixLQUFLLEVBQUU7O3lEQUFpQztJQUtoQztRQUFSLEtBQUssRUFBRTs7MERBQWtDO0lBS2pDO1FBQVIsS0FBSyxFQUFFOztnRUFBeUI7SUFLeEI7UUFBUixLQUFLLEVBQUU7O3dEQUFpQjtJQU1oQjtRQUFSLEtBQUssRUFBRTs7dURBQWtEO0lBS2pEO1FBQVIsS0FBSyxFQUFFOzs2REFBc0I7SUFLckI7UUFBUixLQUFLLEVBQUU7OzBEQUFtQjtJQUtsQjtRQUFSLEtBQUssRUFBRTs7eURBQWtCO0lBT2pCO1FBQVIsS0FBSyxFQUFFOzt3REFBMkI7SUFNMUI7UUFBUixLQUFLLEVBQUU7O3VEQUEwRztJQU16RztRQUFSLEtBQUssRUFBRTs7a0VBQTBCO0lBTXpCO1FBQVIsS0FBSyxFQUFFOztnRUFBd0I7SUFNdkI7UUFBUixLQUFLLEVBQUU7OzZEQUFzQjtJQUtyQjtRQUFSLEtBQUssRUFBRTs7b0VBQTZCO0lBSzVCO1FBQVIsS0FBSyxFQUFFOztnRUFBd0I7SUFLdkI7UUFBUixLQUFLLEVBQUU7OzBEQUFrQjtJQUtoQjtRQUFULE1BQU0sRUFBRTswQ0FBYSxZQUFZOzJEQUFrQztJQUsxRDtRQUFULE1BQU0sRUFBRTswQ0FBYSxZQUFZOzJEQUFrQztJQUtOO1FBQTdELFNBQVMsQ0FBQyxjQUFjLEVBQUUsRUFBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUMsQ0FBQzswQ0FBZ0IsVUFBVTs4REFBQztJQUtuQjtRQUFwRSxTQUFTLENBQUMsZUFBZSxFQUFFLEVBQUMsSUFBSSxFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUMsQ0FBQzswQ0FBb0IsZ0JBQWdCO2tFQUFDO0lBSzdEO1FBQTNDLFlBQVksQ0FBQyxXQUFXLEVBQUUsRUFBQyxNQUFNLEVBQUUsS0FBSyxFQUFDLENBQUM7MENBQWUsV0FBVzs2REFBTTtJQTFJaEUsbUJBQW1CO1FBTC9CLFNBQVMsQ0FBQztZQUNULDhDQUE4QztZQUM5QyxRQUFRLEVBQUUsd0JBQXdCO1lBQ2xDLFFBQVEsRUFBRSw4RUFBOEU7U0FDekYsQ0FBQztRQWlKRyxtQkFBQSxRQUFRLEVBQUUsQ0FBQSxFQUFFLG1CQUFBLElBQUksRUFBRSxDQUFBLEVBQUUsbUJBQUEsUUFBUSxFQUFFLENBQUE7aURBQWtCLFNBQVM7WUFDeEMsb0JBQW9CO1lBQ3BCLGFBQWE7WUFDZCxhQUFhO09BbkpyQixtQkFBbUIsQ0FpUi9CO0lBQUQsMEJBQUM7Q0FBQSxBQWpSRCxJQWlSQztTQWpSWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZ21NYXJrZXIsIEdvb2dsZU1hcHNBUElXcmFwcGVyLCBNYXBzQVBJTG9hZGVyLCBNYXJrZXJNYW5hZ2VyIH0gZnJvbSAnYWdtX2ZpdC9jb3JlJztcclxuaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBDb250ZW50Q2hpbGQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgSG9zdCwgSW5wdXQsIE9uQ2hhbmdlcywgT25EZXN0cm95LCBPcHRpb25hbCwgT3V0cHV0LCBTaW1wbGVDaGFuZ2VzLCBTa2lwU2VsZiwgVGVtcGxhdGVSZWYsIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuZGVjbGFyZSB2YXIgcmVxdWlyZTogYW55O1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOmNvbXBvbmVudC1zZWxlY3RvclxyXG4gIHNlbGVjdG9yOiAnYWdtLXNuYXp6eS1pbmZvLXdpbmRvdycsXHJcbiAgdGVtcGxhdGU6ICc8ZGl2ICNvdXRlcldyYXBwZXI+PGRpdiAjdmlld0NvbnRhaW5lcj48L2Rpdj48L2Rpdj48bmctY29udGVudD48L25nLWNvbnRlbnQ+JyxcclxufSlcclxuZXhwb3J0IGNsYXNzIEFnbVNuYXp6eUluZm9XaW5kb3cgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcyB7XHJcbiAgLyoqXHJcbiAgICogVGhlIGxhdGl0dWRlIGFuZCBsb25naXR1ZGUgd2hlcmUgdGhlIGluZm8gd2luZG93IGlzIGFuY2hvcmVkLlxyXG4gICAqIFRoZSBvZmZzZXQgd2lsbCBkZWZhdWx0IHRvIDBweCB3aGVuIHVzaW5nIHRoaXMgb3B0aW9uLiBPbmx5IHJlcXVpcmVkL3VzZWQgaWYgeW91IGFyZSBub3QgdXNpbmcgYSBhZ20tbWFya2VyLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGxhdGl0dWRlOiBudW1iZXI7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBsb25naXR1ZGUgd2hlcmUgdGhlIGluZm8gd2luZG93IGlzIGFuY2hvcmVkLlxyXG4gICAqIFRoZSBvZmZzZXQgd2lsbCBkZWZhdWx0IHRvIDBweCB3aGVuIHVzaW5nIHRoaXMgb3B0aW9uLiBPbmx5IHJlcXVpcmVkL3VzZWQgaWYgeW91IGFyZSBub3QgdXNpbmcgYSBhZ20tbWFya2VyLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGxvbmdpdHVkZTogbnVtYmVyO1xyXG5cclxuICAvKipcclxuICAgKiBDaGFuZ2VzIHRoZSBvcGVuIHN0YXR1cyBvZiB0aGUgc25henp5IGluZm8gd2luZG93LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGlzT3BlbiA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBFbWl0cyB3aGVuIHRoZSBvcGVuIHN0YXR1cyBjaGFuZ2VzLlxyXG4gICAqL1xyXG4gIEBPdXRwdXQoKSBpc09wZW5DaGFuZ2U6IEV2ZW50RW1pdHRlcjxib29sZWFuPiA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4oKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQ2hvb3NlIHdoZXJlIHlvdSB3YW50IHRoZSBpbmZvIHdpbmRvdyB0byBiZSBkaXNwbGF5ZWQsIHJlbGF0aXZlIHRvIHRoZSBtYXJrZXIuXHJcbiAgICovXHJcbiAgQElucHV0KCkgcGxhY2VtZW50OiAndG9wJyB8ICdib3R0b20nIHwgJ2xlZnQnIHwgJ3JpZ2h0JyA9ICd0b3AnO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgbWF4IHdpZHRoIGluIHBpeGVscyBvZiB0aGUgaW5mbyB3aW5kb3cuXHJcbiAgICovXHJcbiAgQElucHV0KCkgbWF4V2lkdGg6IG51bWJlciB8IHN0cmluZyA9IDIwMDtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIG1heCBoZWlnaHQgaW4gcGl4ZWxzIG9mIHRoZSBpbmZvIHdpbmRvdy5cclxuICAgKi9cclxuICBASW5wdXQoKSBtYXhIZWlnaHQ6IG51bWJlciB8IHN0cmluZyA9IDIwMDtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIGNvbG9yIHRvIHVzZSBmb3IgdGhlIGJhY2tncm91bmQgb2YgdGhlIGluZm8gd2luZG93LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGJhY2tncm91bmRDb2xvcjogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBBIGN1c3RvbSBwYWRkaW5nIHNpemUgYXJvdW5kIHRoZSBjb250ZW50IG9mIHRoZSBpbmZvIHdpbmRvdy5cclxuICAgKi9cclxuICBASW5wdXQoKSBwYWRkaW5nOiBzdHJpbmc7XHJcblxyXG4gIC8qKlxyXG4gICAqIEEgY3VzdG9tIGJvcmRlciBhcm91bmQgdGhlIGluZm8gd2luZG93LiBTZXQgdG8gZmFsc2UgdG8gY29tcGxldGVseSByZW1vdmUgdGhlIGJvcmRlci5cclxuICAgKiBUaGUgdW5pdHMgdXNlZCBmb3IgYm9yZGVyIHNob3VsZCBiZSB0aGUgc2FtZSBhcyBwb2ludGVyLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGJvcmRlcjoge3dpZHRoOiBzdHJpbmc7IGNvbG9yOiBzdHJpbmd9IHwgYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogQSBjdXN0b20gQ1NTIGJvcmRlciByYWRpdXMgcHJvcGVydHkgdG8gc3BlY2lmeSB0aGUgcm91bmRlZCBjb3JuZXJzIG9mIHRoZSBpbmZvIHdpbmRvdy5cclxuICAgKi9cclxuICBASW5wdXQoKSBib3JkZXJSYWRpdXM6IHN0cmluZztcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIGZvbnQgY29sb3IgdG8gdXNlIGZvciB0aGUgY29udGVudCBpbnNpZGUgdGhlIGJvZHkgb2YgdGhlIGluZm8gd2luZG93LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGZvbnRDb2xvcjogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgZm9udCBzaXplIHRvIHVzZSBmb3IgdGhlIGNvbnRlbnQgaW5zaWRlIHRoZSBib2R5IG9mIHRoZSBpbmZvIHdpbmRvdy5cclxuICAgKi9cclxuICBASW5wdXQoKSBmb250U2l6ZTogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgaGVpZ2h0IG9mIHRoZSBwb2ludGVyIGZyb20gdGhlIGluZm8gd2luZG93IHRvIHRoZSBtYXJrZXIuXHJcbiAgICogU2V0IHRvIGZhbHNlIHRvIGNvbXBsZXRlbHkgcmVtb3ZlIHRoZSBwb2ludGVyLlxyXG4gICAqIFRoZSB1bml0cyB1c2VkIGZvciBwb2ludGVyIHNob3VsZCBiZSB0aGUgc2FtZSBhcyBib3JkZXIuXHJcbiAgICovXHJcbiAgQElucHV0KCkgcG9pbnRlcjogc3RyaW5nIHwgYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogVGhlIENTUyBwcm9wZXJ0aWVzIGZvciB0aGUgc2hhZG93IG9mIHRoZSBpbmZvIHdpbmRvdy5cclxuICAgKiBTZXQgdG8gZmFsc2UgdG8gY29tcGxldGVseSByZW1vdmUgdGhlIHNoYWRvdy5cclxuICAgKi9cclxuICBASW5wdXQoKSBzaGFkb3c6IGJvb2xlYW4gfCB7aD86IHN0cmluZywgdj86IHN0cmluZywgYmx1cjogc3RyaW5nLCBzcHJlYWQ6IHN0cmluZywgb3BhY2l0eTogbnVtYmVyLCBjb2xvcjogc3RyaW5nfTtcclxuXHJcbiAgLyoqXHJcbiAgICogRGV0ZXJtaW5lcyBpZiB0aGUgaW5mbyB3aW5kb3cgd2lsbCBvcGVuIHdoZW4gdGhlIG1hcmtlciBpcyBjbGlja2VkLlxyXG4gICAqIEFuIGludGVybmFsIGxpc3RlbmVyIGlzIGFkZGVkIHRvIHRoZSBHb29nbGUgTWFwcyBjbGljayBldmVudCB3aGljaCBjYWxscyB0aGUgb3BlbigpIG1ldGhvZC5cclxuICAgKi9cclxuICBASW5wdXQoKSBvcGVuT25NYXJrZXJDbGljayA9IHRydWU7XHJcblxyXG4gIC8qKlxyXG4gICAqIERldGVybWluZXMgaWYgdGhlIGluZm8gd2luZG93IHdpbGwgY2xvc2Ugd2hlbiB0aGUgbWFwIGlzIGNsaWNrZWQuIEFuIGludGVybmFsIGxpc3RlbmVyIGlzIGFkZGVkIHRvIHRoZSBHb29nbGUgTWFwcyBjbGljayBldmVudCB3aGljaCBjYWxscyB0aGUgY2xvc2UoKSBtZXRob2QuXHJcbiAgICogVGhpcyB3aWxsIG5vdCBhY3RpdmF0ZSBvbiB0aGUgR29vZ2xlIE1hcHMgZHJhZyBldmVudCB3aGVuIHRoZSB1c2VyIGlzIHBhbm5pbmcgdGhlIG1hcC5cclxuICAgKi9cclxuICBASW5wdXQoKSBjbG9zZU9uTWFwQ2xpY2sgPSB0cnVlO1xyXG5cclxuICAvKipcclxuICAgKiBBbiBvcHRpb25hbCBDU1MgY2xhc3MgdG8gYXNzaWduIHRvIHRoZSB3cmFwcGVyIGNvbnRhaW5lciBvZiB0aGUgaW5mbyB3aW5kb3cuXHJcbiAgICogQ2FuIGJlIHVzZWQgZm9yIGFwcGx5aW5nIGN1c3RvbSBDU1MgdG8gdGhlIGluZm8gd2luZG93LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHdyYXBwZXJDbGFzczogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBEZXRlcm1pbmVzIGlmIHRoZSBpbmZvIHdpbmRvdyB3aWxsIGNsb3NlIHdoZW4gYW55IG90aGVyIFNuYXp6eSBJbmZvIFdpbmRvdyBpcyBvcGVuZWQuXHJcbiAgICovXHJcbiAgQElucHV0KCkgY2xvc2VXaGVuT3RoZXJzT3BlbiA9IGZhbHNlO1xyXG5cclxuICAvKipcclxuICAgKiBEZXRlcm1pbmVzIGlmIHRoZSBpbmZvIHdpbmRvdyB3aWxsIHNob3cgYSBjbG9zZSBidXR0b24uXHJcbiAgICovXHJcbiAgQElucHV0KCkgc2hvd0Nsb3NlQnV0dG9uID0gdHJ1ZTtcclxuXHJcbiAgLyoqXHJcbiAgICogRGV0ZXJtaW5lcyBpZiB0aGUgaW5mbyB3aW5kb3cgd2lsbCBiZSBwYW5uZWQgaW50byB2aWV3IHdoZW4gb3BlbmVkLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHBhbk9uT3BlbiA9IHRydWU7XHJcblxyXG4gIC8qKlxyXG4gICAqIEVtaXRzIGJlZm9yZSB0aGUgaW5mbyB3aW5kb3cgb3BlbnMuXHJcbiAgICovXHJcbiAgQE91dHB1dCgpIGJlZm9yZU9wZW46IEV2ZW50RW1pdHRlcjx2b2lkPiA9IG5ldyBFdmVudEVtaXR0ZXI8dm9pZD4oKTtcclxuXHJcbiAgLyoqXHJcbiAgICogRW1pdHMgYmVmb3JlIHRoZSBpbmZvIHdpbmRvdyBjbG9zZXMuXHJcbiAgICovXHJcbiAgQE91dHB1dCgpIGFmdGVyQ2xvc2U6IEV2ZW50RW1pdHRlcjx2b2lkPiA9IG5ldyBFdmVudEVtaXR0ZXI8dm9pZD4oKTtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsXHJcbiAgICovXHJcbiAgQFZpZXdDaGlsZCgnb3V0ZXJXcmFwcGVyJywge3JlYWQ6IEVsZW1lbnRSZWYsIHN0YXRpYzogZmFsc2V9KSBfb3V0ZXJXcmFwcGVyOiBFbGVtZW50UmVmO1xyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWxcclxuICAgKi9cclxuICBAVmlld0NoaWxkKCd2aWV3Q29udGFpbmVyJywge3JlYWQ6IFZpZXdDb250YWluZXJSZWYsIHN0YXRpYzogZmFsc2V9KSBfdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZjtcclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsXHJcbiAgICovXHJcbiAgQENvbnRlbnRDaGlsZChUZW1wbGF0ZVJlZiwge3N0YXRpYzogZmFsc2V9KSBfdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPGFueT47XHJcblxyXG4gIHByb3RlY3RlZCBfbmF0aXZlU25henp5SW5mb1dpbmRvdzogYW55O1xyXG4gIHByb3RlY3RlZCBfc25henp5SW5mb1dpbmRvd0luaXRpYWxpemVkOiBQcm9taXNlPGFueT4gfCBudWxsID0gbnVsbDtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBAT3B0aW9uYWwoKSBASG9zdCgpIEBTa2lwU2VsZigpIHByaXZhdGUgX21hcmtlcjogQWdtTWFya2VyLFxyXG4gICAgcHJpdmF0ZSBfd3JhcHBlcjogR29vZ2xlTWFwc0FQSVdyYXBwZXIsXHJcbiAgICBwcml2YXRlIF9tYW5hZ2VyOiBNYXJrZXJNYW5hZ2VyLFxyXG4gICAgcHJpdmF0ZSBfbG9hZGVyOiBNYXBzQVBJTG9hZGVyLFxyXG4gICkge31cclxuXHJcbiAgLyoqXHJcbiAgICogQGludGVybmFsXHJcbiAgICovXHJcbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgaWYgKHRoaXMuX25hdGl2ZVNuYXp6eUluZm9XaW5kb3cgPT0gbnVsbCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAoJ2lzT3BlbicgaW4gY2hhbmdlcyAmJiB0aGlzLmlzT3Blbikge1xyXG4gICAgICB0aGlzLl9vcGVuSW5mb1dpbmRvdygpO1xyXG4gICAgfSBlbHNlIGlmICgnaXNPcGVuJyBpbiBjaGFuZ2VzICYmICF0aGlzLmlzT3Blbikge1xyXG4gICAgICB0aGlzLl9jbG9zZUluZm9XaW5kb3coKTtcclxuICAgIH1cclxuICAgIGlmICgoJ2xhdGl0dWRlJyBpbiBjaGFuZ2VzIHx8ICdsb25naXR1ZGUnIGluIGNoYW5nZXMpICYmIHRoaXMuX21hcmtlciA9PSBudWxsKSB7XHJcbiAgICAgIHRoaXMuX3VwZGF0ZVBvc2l0aW9uKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWxcclxuICAgKi9cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICBjb25zdCBtID0gdGhpcy5fbWFuYWdlciAhPSBudWxsID8gdGhpcy5fbWFuYWdlci5nZXROYXRpdmVNYXJrZXIodGhpcy5fbWFya2VyKSA6IG51bGw7XHJcbiAgICB0aGlzLl9zbmF6enlJbmZvV2luZG93SW5pdGlhbGl6ZWQgPSB0aGlzLl9sb2FkZXIubG9hZCgpXHJcbiAgICAgIC50aGVuKCgpID0+IHJlcXVpcmUoJ3NuYXp6eS1pbmZvLXdpbmRvdycpKVxyXG4gICAgICAudGhlbigobW9kdWxlOiBhbnkpID0+IFByb21pc2UuYWxsKFttb2R1bGUsIG0sIHRoaXMuX3dyYXBwZXIuZ2V0TmF0aXZlTWFwKCldKSlcclxuICAgICAgLnRoZW4oKGVsZW1zKSA9PiB7XHJcbiAgICAgICAgY29uc3Qgb3B0aW9uczogYW55ID0ge1xyXG4gICAgICAgICAgbWFwOiBlbGVtc1syXSxcclxuICAgICAgICAgIGNvbnRlbnQ6ICcnLFxyXG4gICAgICAgICAgcGxhY2VtZW50OiB0aGlzLnBsYWNlbWVudCxcclxuICAgICAgICAgIG1heFdpZHRoOiB0aGlzLm1heFdpZHRoLFxyXG4gICAgICAgICAgbWF4SGVpZ2h0OiB0aGlzLm1heEhlaWdodCxcclxuICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogdGhpcy5iYWNrZ3JvdW5kQ29sb3IsXHJcbiAgICAgICAgICBwYWRkaW5nOiB0aGlzLnBhZGRpbmcsXHJcbiAgICAgICAgICBib3JkZXI6IHRoaXMuYm9yZGVyLFxyXG4gICAgICAgICAgYm9yZGVyUmFkaXVzOiB0aGlzLmJvcmRlclJhZGl1cyxcclxuICAgICAgICAgIGZvbnRDb2xvcjogdGhpcy5mb250Q29sb3IsXHJcbiAgICAgICAgICBwb2ludGVyOiB0aGlzLnBvaW50ZXIsXHJcbiAgICAgICAgICBzaGFkb3c6IHRoaXMuc2hhZG93LFxyXG4gICAgICAgICAgY2xvc2VPbk1hcENsaWNrOiB0aGlzLmNsb3NlT25NYXBDbGljayxcclxuICAgICAgICAgIG9wZW5Pbk1hcmtlckNsaWNrOiB0aGlzLm9wZW5Pbk1hcmtlckNsaWNrLFxyXG4gICAgICAgICAgY2xvc2VXaGVuT3RoZXJzT3BlbjogdGhpcy5jbG9zZVdoZW5PdGhlcnNPcGVuLFxyXG4gICAgICAgICAgc2hvd0Nsb3NlQnV0dG9uOiB0aGlzLnNob3dDbG9zZUJ1dHRvbixcclxuICAgICAgICAgIHBhbk9uT3BlbjogdGhpcy5wYW5Pbk9wZW4sXHJcbiAgICAgICAgICB3cmFwcGVyQ2xhc3M6IHRoaXMud3JhcHBlckNsYXNzLFxyXG4gICAgICAgICAgY2FsbGJhY2tzOiB7XHJcbiAgICAgICAgICAgIGJlZm9yZU9wZW46ICgpID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLl9jcmVhdGVWaWV3Q29udGVudCgpO1xyXG4gICAgICAgICAgICAgIHRoaXMuYmVmb3JlT3Blbi5lbWl0KCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGFmdGVyT3BlbjogKCkgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuaXNPcGVuQ2hhbmdlLmVtaXQodGhpcy5vcGVuU3RhdHVzKCkpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBhZnRlckNsb3NlOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5hZnRlckNsb3NlLmVtaXQoKTtcclxuICAgICAgICAgICAgICB0aGlzLmlzT3BlbkNoYW5nZS5lbWl0KHRoaXMub3BlblN0YXR1cygpKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAoZWxlbXNbMV0gIT0gbnVsbCkge1xyXG4gICAgICAgICAgb3B0aW9ucy5tYXJrZXIgPSBlbGVtc1sxXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgb3B0aW9ucy5wb3NpdGlvbiA9IHtcclxuICAgICAgICAgICAgbGF0OiB0aGlzLmxhdGl0dWRlLFxyXG4gICAgICAgICAgICBsbmc6IHRoaXMubG9uZ2l0dWRlLFxyXG4gICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fbmF0aXZlU25henp5SW5mb1dpbmRvdyA9IG5ldyBlbGVtc1swXShvcHRpb25zKTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMuX3NuYXp6eUluZm9XaW5kb3dJbml0aWFsaXplZC50aGVuKCgpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5pc09wZW4pIHtcclxuICAgICAgICAgIHRoaXMuX29wZW5JbmZvV2luZG93KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHByb3RlY3RlZCBfb3BlbkluZm9XaW5kb3coKSB7XHJcbiAgICB0aGlzLl9zbmF6enlJbmZvV2luZG93SW5pdGlhbGl6ZWQudGhlbigoKSA9PiB7XHJcbiAgICAgIHRoaXMuX2NyZWF0ZVZpZXdDb250ZW50KCk7XHJcbiAgICAgIHRoaXMuX25hdGl2ZVNuYXp6eUluZm9XaW5kb3cub3BlbigpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgX2Nsb3NlSW5mb1dpbmRvdygpIHtcclxuICAgIHRoaXMuX3NuYXp6eUluZm9XaW5kb3dJbml0aWFsaXplZC50aGVuKCgpID0+IHtcclxuICAgICAgdGhpcy5fbmF0aXZlU25henp5SW5mb1dpbmRvdy5jbG9zZSgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBwcm90ZWN0ZWQgX2NyZWF0ZVZpZXdDb250ZW50KCkge1xyXG4gICAgaWYgKHRoaXMuX3ZpZXdDb250YWluZXJSZWYubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGNvbnN0IGV2ciA9IHRoaXMuX3ZpZXdDb250YWluZXJSZWYuY3JlYXRlRW1iZWRkZWRWaWV3KHRoaXMuX3RlbXBsYXRlUmVmKTtcclxuICAgIHRoaXMuX25hdGl2ZVNuYXp6eUluZm9XaW5kb3cuc2V0Q29udGVudCh0aGlzLl9vdXRlcldyYXBwZXIubmF0aXZlRWxlbWVudCk7XHJcbiAgICAvLyB3ZSBoYXZlIHRvIHJ1biB0aGlzIGluIGEgc2VwYXJhdGUgY3ljbGUuXHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgZXZyLmRldGVjdENoYW5nZXMoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJvdGVjdGVkIF91cGRhdGVQb3NpdGlvbigpIHtcclxuICAgIHRoaXMuX25hdGl2ZVNuYXp6eUluZm9XaW5kb3cuc2V0UG9zaXRpb24oe1xyXG4gICAgICBsYXQ6IHRoaXMubGF0aXR1ZGUsXHJcbiAgICAgIGxuZzogdGhpcy5sb25naXR1ZGUsXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJldHVybnMgdHJ1ZSB3aGVuIHRoZSBTbmF6enkgSW5mbyBXaW5kb3cgaXMgaW5pdGlhbGl6ZWQgYW5kIG9wZW4uXHJcbiAgICovXHJcbiAgb3BlblN0YXR1cygpOiBib29sZWFuIHtcclxuICAgIHJldHVybiB0aGlzLl9uYXRpdmVTbmF6enlJbmZvV2luZG93ICYmIHRoaXMuX25hdGl2ZVNuYXp6eUluZm9XaW5kb3cuaXNPcGVuKCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBAaW50ZXJuYWxcclxuICAgKi9cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLl9uYXRpdmVTbmF6enlJbmZvV2luZG93KSB7XHJcbiAgICAgIHRoaXMuX25hdGl2ZVNuYXp6eUluZm9XaW5kb3cuZGVzdHJveSgpO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=