import * as tslib_1 from "tslib";
import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { InfoWindowManager, MarkerManager } from 'agm_fit/core';
import { ClusterManager } from '../services/managers/cluster-manager';
/**
 * AgmMarkerCluster clusters map marker if they are near together
 *
 * ### Example
 * ```typescript
 * import { Component } from '@angular/core';
 *
 * @Component({
 *  selector: 'my-map-cmp',
 *  styles: [`
 *    agm-map {
 *      height: 300px;
 *    }
 * `],
 *  template: `
 *    <agm-map [latitude]="lat" [longitude]="lng" [zoom]="zoom">
 *      <agm-marker-cluster>
 *        <agm-marker [latitude]="lat" [longitude]="lng" [label]="'M'">
 *        </agm-marker>
 *        <agm-marker [latitude]="lat2" [longitude]="lng2" [label]="'N'">
 *        </agm-marker>
 *      </agm-marker-cluster>
 *    </agm-map>
 *  `
 * })
 * ```
 */
var AgmMarkerCluster = /** @class */ (function () {
    function AgmMarkerCluster(_clusterManager) {
        this._clusterManager = _clusterManager;
        this.clusterClick = new EventEmitter();
        this._observableSubscriptions = [];
    }
    /** @internal */
    AgmMarkerCluster.prototype.ngOnDestroy = function () {
        this._clusterManager.clearMarkers();
        this._observableSubscriptions.forEach(function (s) { return s.unsubscribe(); });
    };
    /** @internal */
    AgmMarkerCluster.prototype.ngOnChanges = function (changes) {
        if (changes['gridSize']) {
            this._clusterManager.setGridSize(this);
        }
        if (changes['maxZoom']) {
            this._clusterManager.setMaxZoom(this);
        }
        if (changes['zoomOnClick']) {
            this._clusterManager.setZoomOnClick(this);
        }
        if (changes['averageCenter']) {
            this._clusterManager.setAverageCenter(this);
        }
        if (changes['minimumClusterSize']) {
            this._clusterManager.setMinimumClusterSize(this);
        }
        if (changes['imagePath']) {
            this._clusterManager.setImagePath(this);
        }
        if (changes['imageExtension']) {
            this._clusterManager.setImageExtension(this);
        }
        if (changes['calculator']) {
            this._clusterManager.setCalculator(this);
        }
        if (changes['styles']) {
            this._clusterManager.setStyles(this);
        }
    };
    AgmMarkerCluster.prototype._addEventListeners = function () {
        var _this = this;
        var handlers = [
            { name: 'clusterclick', handler: function (ev) { return _this.clusterClick.emit(ev); } },
        ];
        console.log(handlers);
        handlers.forEach(function (obj) {
            var os = _this._clusterManager.createClusterEventObservable(obj.name).subscribe(obj.handler);
            _this._observableSubscriptions.push(os);
        });
    };
    /** @internal */
    AgmMarkerCluster.prototype.ngOnInit = function () {
        this._addEventListeners();
        this._clusterManager.init({
            gridSize: this.gridSize,
            maxZoom: this.maxZoom,
            zoomOnClick: this.zoomOnClick,
            averageCenter: this.averageCenter,
            minimumClusterSize: this.minimumClusterSize,
            styles: this.styles,
            imagePath: this.imagePath,
            imageExtension: this.imageExtension,
            calculator: this.calculator,
        });
    };
    AgmMarkerCluster.ctorParameters = function () { return [
        { type: ClusterManager }
    ]; };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], AgmMarkerCluster.prototype, "gridSize", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], AgmMarkerCluster.prototype, "maxZoom", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], AgmMarkerCluster.prototype, "zoomOnClick", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], AgmMarkerCluster.prototype, "averageCenter", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], AgmMarkerCluster.prototype, "minimumClusterSize", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], AgmMarkerCluster.prototype, "styles", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Function)
    ], AgmMarkerCluster.prototype, "calculator", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmMarkerCluster.prototype, "imagePath", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AgmMarkerCluster.prototype, "imageExtension", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", EventEmitter)
    ], AgmMarkerCluster.prototype, "clusterClick", void 0);
    AgmMarkerCluster = tslib_1.__decorate([
        Directive({
            selector: 'agm-marker-cluster',
            providers: [
                ClusterManager,
                { provide: MarkerManager, useExisting: ClusterManager },
                InfoWindowManager,
            ],
        }),
        tslib_1.__metadata("design:paramtypes", [ClusterManager])
    ], AgmMarkerCluster);
    return AgmMarkerCluster;
}());
export { AgmMarkerCluster };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFya2VyLWNsdXN0ZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9hZ21fZml0L2pzLW1hcmtlci1jbHVzdGVyZXIvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL21hcmtlci1jbHVzdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQWdDLE1BQU0sRUFBZ0IsTUFBTSxlQUFlLENBQUM7QUFFbkgsT0FBTyxFQUFFLGlCQUFpQixFQUFFLGFBQWEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUNoRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFNdEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBMEJHO0FBU0g7SUEwQ0UsMEJBQW9CLGVBQStCO1FBQS9CLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQUh6QyxpQkFBWSxHQUE2QixJQUFJLFlBQVksRUFBYyxDQUFDO1FBRTFFLDZCQUF3QixHQUFtQixFQUFFLENBQUM7SUFDQyxDQUFDO0lBRXhELGdCQUFnQjtJQUNoQixzQ0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFmLENBQWUsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRCxnQkFBZ0I7SUFDaEIsc0NBQVcsR0FBWCxVQUFZLE9BQXdDO1FBQ2xELElBQUksT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3hDO1FBQ0QsSUFBSSxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkM7UUFDRCxJQUFJLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUMxQixJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMzQztRQUNELElBQUksT0FBTyxDQUFDLGVBQWUsQ0FBQyxFQUFFO1lBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDN0M7UUFDRCxJQUFJLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFO1lBQ2pDLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEQ7UUFDRCxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6QztRQUNELElBQUksT0FBTyxDQUFDLGdCQUFnQixDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUM5QztRQUNELElBQUksT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzFDO1FBQ0QsSUFBSSxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDckIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDO0lBRU8sNkNBQWtCLEdBQTFCO1FBQUEsaUJBWUM7UUFYQyxJQUFNLFFBQVEsR0FBRztZQUNmLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxPQUFPLEVBQUUsVUFBQyxFQUFPLElBQUssT0FBQSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBMUIsQ0FBMEIsRUFBRTtTQUMzRSxDQUFDO1FBRUYsT0FBTyxDQUFDLEdBQUcsQ0FBRSxRQUFRLENBQUUsQ0FBQztRQUd4QixRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUMsR0FBRztZQUNuQixJQUFNLEVBQUUsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLDRCQUE0QixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlGLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0JBQWdCO0lBQ2hCLG1DQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQztZQUN4QixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPO1lBQ3JCLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVztZQUM3QixhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWE7WUFDakMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLGtCQUFrQjtZQUMzQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDbkIsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO1lBQ3pCLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYztZQUNuQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7U0FDNUIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBbkVvQyxjQUFjOztJQXRDMUM7UUFBUixLQUFLLEVBQUU7O3NEQUFrQjtJQUtqQjtRQUFSLEtBQUssRUFBRTs7cURBQWlCO0lBS2hCO1FBQVIsS0FBSyxFQUFFOzt5REFBc0I7SUFLckI7UUFBUixLQUFLLEVBQUU7OzJEQUF3QjtJQUt2QjtRQUFSLEtBQUssRUFBRTs7Z0VBQTRCO0lBSzNCO1FBQVIsS0FBSyxFQUFFOztvREFBd0I7SUFLdkI7UUFBUixLQUFLLEVBQUU7O3dEQUErQjtJQUU5QjtRQUFSLEtBQUssRUFBRTs7dURBQW1CO0lBQ2xCO1FBQVIsS0FBSyxFQUFFOzs0REFBd0I7SUFFdEI7UUFBVCxNQUFNLEVBQUU7MENBQWUsWUFBWTswREFBOEM7SUF2Q3ZFLGdCQUFnQjtRQVI1QixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFNBQVMsRUFBRTtnQkFDVCxjQUFjO2dCQUNkLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFO2dCQUN2RCxpQkFBaUI7YUFDbEI7U0FDRixDQUFDO2lEQTJDcUMsY0FBYztPQTFDeEMsZ0JBQWdCLENBOEc1QjtJQUFELHVCQUFDO0NBQUEsQUE5R0QsSUE4R0M7U0E5R1ksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkNoYW5nZXMsIE9uRGVzdHJveSwgT25Jbml0LCBPdXRwdXQsIFNpbXBsZUNoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgSW5mb1dpbmRvd01hbmFnZXIsIE1hcmtlck1hbmFnZXIgfSBmcm9tICdhZ21fZml0L2NvcmUnO1xyXG5pbXBvcnQgeyBDbHVzdGVyTWFuYWdlciB9IGZyb20gJy4uL3NlcnZpY2VzL21hbmFnZXJzL2NsdXN0ZXItbWFuYWdlcic7XHJcblxyXG5pbXBvcnQgeyBDYWxjdWxhdGVGdW5jdGlvbiwgQ2x1c3Rlck9wdGlvbnMsIENsdXN0ZXJTdHlsZSB9IGZyb20gJy4uL3NlcnZpY2VzL2dvb2dsZS1jbHVzdGVyZXItdHlwZXMnO1xyXG5cclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG4vKipcclxuICogQWdtTWFya2VyQ2x1c3RlciBjbHVzdGVycyBtYXAgbWFya2VyIGlmIHRoZXkgYXJlIG5lYXIgdG9nZXRoZXJcclxuICpcclxuICogIyMjIEV4YW1wbGVcclxuICogYGBgdHlwZXNjcmlwdFxyXG4gKiBpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuICpcclxuICogQENvbXBvbmVudCh7XHJcbiAqICBzZWxlY3RvcjogJ215LW1hcC1jbXAnLFxyXG4gKiAgc3R5bGVzOiBbYFxyXG4gKiAgICBhZ20tbWFwIHtcclxuICogICAgICBoZWlnaHQ6IDMwMHB4O1xyXG4gKiAgICB9XHJcbiAqIGBdLFxyXG4gKiAgdGVtcGxhdGU6IGBcclxuICogICAgPGFnbS1tYXAgW2xhdGl0dWRlXT1cImxhdFwiIFtsb25naXR1ZGVdPVwibG5nXCIgW3pvb21dPVwiem9vbVwiPlxyXG4gKiAgICAgIDxhZ20tbWFya2VyLWNsdXN0ZXI+XHJcbiAqICAgICAgICA8YWdtLW1hcmtlciBbbGF0aXR1ZGVdPVwibGF0XCIgW2xvbmdpdHVkZV09XCJsbmdcIiBbbGFiZWxdPVwiJ00nXCI+XHJcbiAqICAgICAgICA8L2FnbS1tYXJrZXI+XHJcbiAqICAgICAgICA8YWdtLW1hcmtlciBbbGF0aXR1ZGVdPVwibGF0MlwiIFtsb25naXR1ZGVdPVwibG5nMlwiIFtsYWJlbF09XCInTidcIj5cclxuICogICAgICAgIDwvYWdtLW1hcmtlcj5cclxuICogICAgICA8L2FnbS1tYXJrZXItY2x1c3Rlcj5cclxuICogICAgPC9hZ20tbWFwPlxyXG4gKiAgYFxyXG4gKiB9KVxyXG4gKiBgYGBcclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gIHNlbGVjdG9yOiAnYWdtLW1hcmtlci1jbHVzdGVyJyxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIENsdXN0ZXJNYW5hZ2VyLFxyXG4gICAgeyBwcm92aWRlOiBNYXJrZXJNYW5hZ2VyLCB1c2VFeGlzdGluZzogQ2x1c3Rlck1hbmFnZXIgfSxcclxuICAgIEluZm9XaW5kb3dNYW5hZ2VyLFxyXG4gIF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBZ21NYXJrZXJDbHVzdGVyIGltcGxlbWVudHMgT25EZXN0cm95LCBPbkNoYW5nZXMsIE9uSW5pdCwgQ2x1c3Rlck9wdGlvbnMge1xyXG4gIC8qKlxyXG4gICAqIFRoZSBncmlkIHNpemUgb2YgYSBjbHVzdGVyIGluIHBpeGVsc1xyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGdyaWRTaXplOiBudW1iZXI7XHJcblxyXG4gIC8qKlxyXG4gICAqIFRoZSBtYXhpbXVtIHpvb20gbGV2ZWwgdGhhdCBhIG1hcmtlciBjYW4gYmUgcGFydCBvZiBhIGNsdXN0ZXIuXHJcbiAgICovXHJcbiAgQElucHV0KCkgbWF4Wm9vbTogbnVtYmVyO1xyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBkZWZhdWx0IGJlaGF2aW91ciBvZiBjbGlja2luZyBvbiBhIGNsdXN0ZXIgaXMgdG8gem9vbSBpbnRvIGl0LlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHpvb21PbkNsaWNrOiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBXaGV0aGVyIHRoZSBjZW50ZXIgb2YgZWFjaCBjbHVzdGVyIHNob3VsZCBiZSB0aGUgYXZlcmFnZSBvZiBhbGwgbWFya2VycyBpbiB0aGUgY2x1c3Rlci5cclxuICAgKi9cclxuICBASW5wdXQoKSBhdmVyYWdlQ2VudGVyOiBib29sZWFuO1xyXG5cclxuICAvKipcclxuICAgKiBUaGUgbWluaW11bSBudW1iZXIgb2YgbWFya2VycyB0byBiZSBpbiBhIGNsdXN0ZXIgYmVmb3JlIHRoZSBtYXJrZXJzIGFyZSBoaWRkZW4gYW5kIGEgY291bnQgaXMgc2hvd24uXHJcbiAgICovXHJcbiAgQElucHV0KCkgbWluaW11bUNsdXN0ZXJTaXplOiBudW1iZXI7XHJcblxyXG4gIC8qKlxyXG4gICAqIEFuIG9iamVjdCB0aGF0IGhhcyBzdHlsZSBwcm9wZXJ0aWVzLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIHN0eWxlczogQ2x1c3RlclN0eWxlW107XHJcblxyXG4gIC8qKlxyXG4gICAqIEEgZnVuY3Rpb24gdGhhdCBjYWxjdWxhdGVzIHRoZSBjbHVzdGVyIHN0eWxlIGFuZCB0ZXh0IGJhc2VkIG9uIHRoZSBtYXJrZXJzIGluIHRoZSBjbHVzdGVyLlxyXG4gICAqL1xyXG4gIEBJbnB1dCgpIGNhbGN1bGF0b3I6IENhbGN1bGF0ZUZ1bmN0aW9uO1xyXG5cclxuICBASW5wdXQoKSBpbWFnZVBhdGg6IHN0cmluZztcclxuICBASW5wdXQoKSBpbWFnZUV4dGVuc2lvbjogc3RyaW5nO1xyXG5cclxuICBAT3V0cHV0KCkgY2x1c3RlckNsaWNrOiBFdmVudEVtaXR0ZXI8TW91c2VFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyPE1vdXNlRXZlbnQ+KCk7XHJcblxyXG4gIHByaXZhdGUgX29ic2VydmFibGVTdWJzY3JpcHRpb25zOiBTdWJzY3JpcHRpb25bXSA9IFtdO1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX2NsdXN0ZXJNYW5hZ2VyOiBDbHVzdGVyTWFuYWdlcikgeyB9XHJcblxyXG4gIC8qKiBAaW50ZXJuYWwgKi9cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMuX2NsdXN0ZXJNYW5hZ2VyLmNsZWFyTWFya2VycygpO1xyXG4gICAgdGhpcy5fb2JzZXJ2YWJsZVN1YnNjcmlwdGlvbnMuZm9yRWFjaCgocykgPT4gcy51bnN1YnNjcmliZSgpKTtcclxuICB9XHJcblxyXG4gIC8qKiBAaW50ZXJuYWwgKi9cclxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiB7IFtrZXk6IHN0cmluZ106IFNpbXBsZUNoYW5nZSB9KSB7XHJcbiAgICBpZiAoY2hhbmdlc1snZ3JpZFNpemUnXSkge1xyXG4gICAgICB0aGlzLl9jbHVzdGVyTWFuYWdlci5zZXRHcmlkU2l6ZSh0aGlzKTtcclxuICAgIH1cclxuICAgIGlmIChjaGFuZ2VzWydtYXhab29tJ10pIHtcclxuICAgICAgdGhpcy5fY2x1c3Rlck1hbmFnZXIuc2V0TWF4Wm9vbSh0aGlzKTtcclxuICAgIH1cclxuICAgIGlmIChjaGFuZ2VzWyd6b29tT25DbGljayddKSB7XHJcbiAgICAgIHRoaXMuX2NsdXN0ZXJNYW5hZ2VyLnNldFpvb21PbkNsaWNrKHRoaXMpO1xyXG4gICAgfVxyXG4gICAgaWYgKGNoYW5nZXNbJ2F2ZXJhZ2VDZW50ZXInXSkge1xyXG4gICAgICB0aGlzLl9jbHVzdGVyTWFuYWdlci5zZXRBdmVyYWdlQ2VudGVyKHRoaXMpO1xyXG4gICAgfVxyXG4gICAgaWYgKGNoYW5nZXNbJ21pbmltdW1DbHVzdGVyU2l6ZSddKSB7XHJcbiAgICAgIHRoaXMuX2NsdXN0ZXJNYW5hZ2VyLnNldE1pbmltdW1DbHVzdGVyU2l6ZSh0aGlzKTtcclxuICAgIH1cclxuICAgIGlmIChjaGFuZ2VzWydpbWFnZVBhdGgnXSkge1xyXG4gICAgICB0aGlzLl9jbHVzdGVyTWFuYWdlci5zZXRJbWFnZVBhdGgodGhpcyk7XHJcbiAgICB9XHJcbiAgICBpZiAoY2hhbmdlc1snaW1hZ2VFeHRlbnNpb24nXSkge1xyXG4gICAgICB0aGlzLl9jbHVzdGVyTWFuYWdlci5zZXRJbWFnZUV4dGVuc2lvbih0aGlzKTtcclxuICAgIH1cclxuICAgIGlmIChjaGFuZ2VzWydjYWxjdWxhdG9yJ10pIHtcclxuICAgICAgdGhpcy5fY2x1c3Rlck1hbmFnZXIuc2V0Q2FsY3VsYXRvcih0aGlzKTtcclxuICAgIH1cclxuICAgIGlmIChjaGFuZ2VzWydzdHlsZXMnXSkge1xyXG4gICAgICB0aGlzLl9jbHVzdGVyTWFuYWdlci5zZXRTdHlsZXModGhpcyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIF9hZGRFdmVudExpc3RlbmVycygpIHtcclxuICAgIGNvbnN0IGhhbmRsZXJzID0gW1xyXG4gICAgICB7IG5hbWU6ICdjbHVzdGVyY2xpY2snLCBoYW5kbGVyOiAoZXY6IGFueSkgPT4gdGhpcy5jbHVzdGVyQ2xpY2suZW1pdChldikgfSxcclxuICAgIF07XHJcblxyXG4gICAgY29uc29sZS5sb2coIGhhbmRsZXJzICk7XHJcbiAgICBcclxuXHJcbiAgICBoYW5kbGVycy5mb3JFYWNoKChvYmopID0+IHtcclxuICAgICAgY29uc3Qgb3MgPSB0aGlzLl9jbHVzdGVyTWFuYWdlci5jcmVhdGVDbHVzdGVyRXZlbnRPYnNlcnZhYmxlKG9iai5uYW1lKS5zdWJzY3JpYmUob2JqLmhhbmRsZXIpO1xyXG4gICAgICB0aGlzLl9vYnNlcnZhYmxlU3Vic2NyaXB0aW9ucy5wdXNoKG9zKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqIEBpbnRlcm5hbCAqL1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5fYWRkRXZlbnRMaXN0ZW5lcnMoKTtcclxuICAgIHRoaXMuX2NsdXN0ZXJNYW5hZ2VyLmluaXQoe1xyXG4gICAgICBncmlkU2l6ZTogdGhpcy5ncmlkU2l6ZSxcclxuICAgICAgbWF4Wm9vbTogdGhpcy5tYXhab29tLFxyXG4gICAgICB6b29tT25DbGljazogdGhpcy56b29tT25DbGljayxcclxuICAgICAgYXZlcmFnZUNlbnRlcjogdGhpcy5hdmVyYWdlQ2VudGVyLFxyXG4gICAgICBtaW5pbXVtQ2x1c3RlclNpemU6IHRoaXMubWluaW11bUNsdXN0ZXJTaXplLFxyXG4gICAgICBzdHlsZXM6IHRoaXMuc3R5bGVzLFxyXG4gICAgICBpbWFnZVBhdGg6IHRoaXMuaW1hZ2VQYXRoLFxyXG4gICAgICBpbWFnZUV4dGVuc2lvbjogdGhpcy5pbWFnZUV4dGVuc2lvbixcclxuICAgICAgY2FsY3VsYXRvcjogdGhpcy5jYWxjdWxhdG9yLFxyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==