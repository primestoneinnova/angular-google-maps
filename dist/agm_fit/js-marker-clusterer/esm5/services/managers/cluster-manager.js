import * as tslib_1 from "tslib";
import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import 'js-marker-clusterer';
import { AgmMarker, GoogleMapsAPIWrapper, MarkerManager } from 'agm_fit/core';
var ClusterManager = /** @class */ (function (_super) {
    tslib_1.__extends(ClusterManager, _super);
    function ClusterManager(_mapsWrapper, _zone) {
        var _this = _super.call(this, _mapsWrapper, _zone) || this;
        _this._mapsWrapper = _mapsWrapper;
        _this._zone = _zone;
        _this._clustererInstance = new Promise(function (resolver) {
            _this._resolver = resolver;
        });
        return _this;
    }
    ClusterManager.prototype.init = function (options) {
        var _this = this;
        this._mapsWrapper.getNativeMap().then(function (map) {
            var clusterer = new MarkerClusterer(map, [], options);
            _this._resolver(clusterer);
        });
    };
    ClusterManager.prototype.getClustererInstance = function () {
        return this._clustererInstance;
    };
    ClusterManager.prototype.addMarker = function (marker) {
        var clusterPromise = this.getClustererInstance();
        var markerPromise = this._mapsWrapper
            .createMarker({
            position: {
                lat: marker.latitude,
                lng: marker.longitude,
            },
            label: marker.label,
            draggable: marker.draggable,
            icon: marker.iconUrl,
            opacity: marker.opacity,
            visible: marker.visible,
            zIndex: marker.zIndex,
            title: marker.title,
            clickable: marker.clickable,
        }, false);
        Promise
            .all([clusterPromise, markerPromise])
            .then(function (_a) {
            var _b = tslib_1.__read(_a, 2), cluster = _b[0], marker = _b[1];
            return cluster.addMarker(marker);
        });
        this._markers.set(marker, markerPromise);
    };
    ClusterManager.prototype.deleteMarker = function (marker) {
        var _this = this;
        var m = this._markers.get(marker);
        if (m == null) {
            // marker already deleted
            return Promise.resolve();
        }
        return m.then(function (m) {
            _this._zone.run(function () {
                m.setMap(null);
                _this.getClustererInstance().then(function (cluster) {
                    cluster.removeMarker(m);
                    _this._markers.delete(marker);
                });
            });
        });
    };
    ClusterManager.prototype.clearMarkers = function () {
        return this.getClustererInstance().then(function (cluster) {
            cluster.clearMarkers();
        });
    };
    ClusterManager.prototype.setGridSize = function (c) {
        this.getClustererInstance().then(function (cluster) {
            cluster.setGridSize(c.gridSize);
        });
    };
    ClusterManager.prototype.setMaxZoom = function (c) {
        this.getClustererInstance().then(function (cluster) {
            cluster.setMaxZoom(c.maxZoom);
        });
    };
    ClusterManager.prototype.setStyles = function (c) {
        this.getClustererInstance().then(function (cluster) {
            cluster.setStyles(c.styles);
        });
    };
    ClusterManager.prototype.setZoomOnClick = function (c) {
        this.getClustererInstance().then(function (cluster) {
            if (c.zoomOnClick !== undefined) {
                cluster.zoomOnClick_ = c.zoomOnClick;
            }
        });
    };
    ClusterManager.prototype.setAverageCenter = function (c) {
        this.getClustererInstance().then(function (cluster) {
            if (c.averageCenter !== undefined) {
                cluster.averageCenter_ = c.averageCenter;
            }
        });
    };
    ClusterManager.prototype.setImagePath = function (c) {
        this.getClustererInstance().then(function (cluster) {
            if (c.imagePath !== undefined) {
                cluster.imagePath_ = c.imagePath;
            }
        });
    };
    ClusterManager.prototype.setMinimumClusterSize = function (c) {
        this.getClustererInstance().then(function (cluster) {
            if (c.minimumClusterSize !== undefined) {
                cluster.minimumClusterSize_ = c.minimumClusterSize;
            }
        });
    };
    ClusterManager.prototype.setImageExtension = function (c) {
        this.getClustererInstance().then(function (cluster) {
            if (c.imageExtension !== undefined) {
                cluster.imageExtension_ = c.imageExtension;
            }
        });
    };
    ClusterManager.prototype.createClusterEventObservable = function (eventName) {
        var _this = this;
        return Observable.create(function (observer) {
            _this._zone.runOutsideAngular(function () {
                _this._clustererInstance.then(function (m) {
                    m.addListener(eventName, function (e) { return _this._zone.run(function () { return observer.next(e); }); });
                });
            });
        });
    };
    ClusterManager.prototype.setCalculator = function (c) {
        this.getClustererInstance().then(function (cluster) {
            if (typeof c.calculator === 'function') {
                cluster.setCalculator(c.calculator);
            }
        });
    };
    ClusterManager.ctorParameters = function () { return [
        { type: GoogleMapsAPIWrapper },
        { type: NgZone }
    ]; };
    ClusterManager = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [GoogleMapsAPIWrapper, NgZone])
    ], ClusterManager);
    return ClusterManager;
}(MarkerManager));
export { ClusterManager };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2x1c3Rlci1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWdtX2ZpdC9qcy1tYXJrZXItY2x1c3RlcmVyLyIsInNvdXJjZXMiOlsic2VydmljZXMvbWFuYWdlcnMvY2x1c3Rlci1tYW5hZ2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFZLE1BQU0sTUFBTSxDQUFDO0FBRTVDLE9BQU8scUJBQXFCLENBQUM7QUFFN0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxvQkFBb0IsRUFBRSxhQUFhLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFROUU7SUFBb0MsMENBQWE7SUFJL0Msd0JBQXNCLFlBQWtDLEVBQVksS0FBYTtRQUFqRixZQUNFLGtCQUFNLFlBQVksRUFBRSxLQUFLLENBQUMsU0FJM0I7UUFMcUIsa0JBQVksR0FBWixZQUFZLENBQXNCO1FBQVksV0FBSyxHQUFMLEtBQUssQ0FBUTtRQUUvRSxLQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxPQUFPLENBQTBCLFVBQUMsUUFBUTtZQUN0RSxLQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUM1QixDQUFDLENBQUMsQ0FBQzs7SUFDTCxDQUFDO0lBRUQsNkJBQUksR0FBSixVQUFLLE9BQXVCO1FBQTVCLGlCQUtDO1FBSkMsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO1lBQ3ZDLElBQU0sU0FBUyxHQUFHLElBQUksZUFBZSxDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDeEQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM1QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw2Q0FBb0IsR0FBcEI7UUFDRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztJQUNqQyxDQUFDO0lBRUQsa0NBQVMsR0FBVCxVQUFVLE1BQWlCO1FBQ3pCLElBQU0sY0FBYyxHQUFxQyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUNyRixJQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsWUFBWTthQUNwQyxZQUFZLENBQUM7WUFDWixRQUFRLEVBQUU7Z0JBQ1IsR0FBRyxFQUFFLE1BQU0sQ0FBQyxRQUFRO2dCQUNwQixHQUFHLEVBQUUsTUFBTSxDQUFDLFNBQVM7YUFDdEI7WUFDRCxLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7WUFDbkIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxTQUFTO1lBQzNCLElBQUksRUFBRSxNQUFNLENBQUMsT0FBTztZQUNwQixPQUFPLEVBQUUsTUFBTSxDQUFDLE9BQU87WUFDdkIsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPO1lBQ3ZCLE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTTtZQUNyQixLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUs7WUFDbkIsU0FBUyxFQUFFLE1BQU0sQ0FBQyxTQUFTO1NBQzVCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFWixPQUFPO2FBQ0osR0FBRyxDQUFDLENBQUMsY0FBYyxFQUFFLGFBQWEsQ0FBQyxDQUFDO2FBQ3BDLElBQUksQ0FBQyxVQUFDLEVBQWlCO2dCQUFqQiwwQkFBaUIsRUFBaEIsZUFBTyxFQUFFLGNBQU07WUFDckIsT0FBTyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCxxQ0FBWSxHQUFaLFVBQWEsTUFBaUI7UUFBOUIsaUJBZUM7UUFkQyxJQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUU7WUFDYix5QkFBeUI7WUFDekIsT0FBTyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDMUI7UUFDRCxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFTO1lBQ3RCLEtBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO2dCQUNiLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2YsS0FBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztvQkFDdEMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDeEIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQy9CLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxxQ0FBWSxHQUFaO1FBQ0UsT0FBTyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO1lBQzdDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxvQ0FBVyxHQUFYLFVBQVksQ0FBbUI7UUFDN0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxtQ0FBVSxHQUFWLFVBQVcsQ0FBbUI7UUFDNUIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxrQ0FBUyxHQUFULFVBQVUsQ0FBbUI7UUFDM0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx1Q0FBYyxHQUFkLFVBQWUsQ0FBbUI7UUFDaEMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxJQUFJLENBQUMsQ0FBQyxXQUFXLEtBQUssU0FBUyxFQUFFO2dCQUMvQixPQUFPLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxXQUFXLENBQUM7YUFDdEM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEIsVUFBaUIsQ0FBbUI7UUFDbEMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxJQUFJLENBQUMsQ0FBQyxhQUFhLEtBQUssU0FBUyxFQUFFO2dCQUNqQyxPQUFPLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUM7YUFDMUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxxQ0FBWSxHQUFaLFVBQWEsQ0FBbUI7UUFDOUIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxJQUFJLENBQUMsQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO2dCQUM3QixPQUFPLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUM7YUFDbEM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw4Q0FBcUIsR0FBckIsVUFBc0IsQ0FBbUI7UUFDdkMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxJQUFJLENBQUMsQ0FBQyxrQkFBa0IsS0FBSyxTQUFTLEVBQUU7Z0JBQ3RDLE9BQU8sQ0FBQyxtQkFBbUIsR0FBRyxDQUFDLENBQUMsa0JBQWtCLENBQUM7YUFDcEQ7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCwwQ0FBaUIsR0FBakIsVUFBa0IsQ0FBbUI7UUFDbkMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUN0QyxJQUFJLENBQUMsQ0FBQyxjQUFjLEtBQUssU0FBUyxFQUFFO2dCQUNsQyxPQUFPLENBQUMsZUFBZSxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUM7YUFDNUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxxREFBNEIsR0FBNUIsVUFBZ0MsU0FBaUI7UUFBakQsaUJBUUM7UUFQQyxPQUFPLFVBQVUsQ0FBQyxNQUFNLENBQUMsVUFBQyxRQUFxQjtZQUM3QyxLQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDO2dCQUMzQixLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBMEI7b0JBQ3RELENBQUMsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLFVBQUMsQ0FBSSxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsY0FBTSxPQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQWhCLENBQWdCLENBQUMsRUFBdEMsQ0FBc0MsQ0FBQyxDQUFDO2dCQUM3RSxDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsc0NBQWEsR0FBYixVQUFlLENBQW1CO1FBQ2hDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87WUFDdEMsSUFBSSxPQUFPLENBQUMsQ0FBQyxVQUFVLEtBQUssVUFBVSxFQUFFO2dCQUN0QyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNyQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBN0ltQyxvQkFBb0I7Z0JBQW1CLE1BQU07O0lBSnRFLGNBQWM7UUFEMUIsVUFBVSxFQUFFO2lEQUt5QixvQkFBb0IsRUFBbUIsTUFBTTtPQUp0RSxjQUFjLENBa0oxQjtJQUFELHFCQUFDO0NBQUEsQUFsSkQsQ0FBb0MsYUFBYSxHQWtKaEQ7U0FsSlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBPYnNlcnZlciB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0ICdqcy1tYXJrZXItY2x1c3RlcmVyJztcclxuXHJcbmltcG9ydCB7IEFnbU1hcmtlciwgR29vZ2xlTWFwc0FQSVdyYXBwZXIsIE1hcmtlck1hbmFnZXIgfSBmcm9tICdhZ21fZml0L2NvcmUnO1xyXG5pbXBvcnQgeyBNYXJrZXIgfSBmcm9tICdhZ21fZml0L2NvcmUvc2VydmljZXMvZ29vZ2xlLW1hcHMtdHlwZXMnO1xyXG5pbXBvcnQgeyBBZ21NYXJrZXJDbHVzdGVyIH0gZnJvbSAnLi4vLi4vZGlyZWN0aXZlcy9tYXJrZXItY2x1c3Rlcic7XHJcbmltcG9ydCB7IENsdXN0ZXJPcHRpb25zLCBNYXJrZXJDbHVzdGVyZXJJbnN0YW5jZSB9IGZyb20gJy4uL2dvb2dsZS1jbHVzdGVyZXItdHlwZXMnO1xyXG5cclxuZGVjbGFyZSB2YXIgTWFya2VyQ2x1c3RlcmVyOiBhbnk7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBDbHVzdGVyTWFuYWdlciBleHRlbmRzIE1hcmtlck1hbmFnZXIge1xyXG4gIHByaXZhdGUgX2NsdXN0ZXJlckluc3RhbmNlOiBQcm9taXNlPE1hcmtlckNsdXN0ZXJlckluc3RhbmNlPjtcclxuICBwcml2YXRlIF9yZXNvbHZlcjogRnVuY3Rpb247XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBfbWFwc1dyYXBwZXI6IEdvb2dsZU1hcHNBUElXcmFwcGVyLCBwcm90ZWN0ZWQgX3pvbmU6IE5nWm9uZSkge1xyXG4gICAgc3VwZXIoX21hcHNXcmFwcGVyLCBfem9uZSk7XHJcbiAgICB0aGlzLl9jbHVzdGVyZXJJbnN0YW5jZSA9IG5ldyBQcm9taXNlPE1hcmtlckNsdXN0ZXJlckluc3RhbmNlPigocmVzb2x2ZXIpID0+IHtcclxuICAgICAgdGhpcy5fcmVzb2x2ZXIgPSByZXNvbHZlcjtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgaW5pdChvcHRpb25zOiBDbHVzdGVyT3B0aW9ucyk6IHZvaWQge1xyXG4gICAgdGhpcy5fbWFwc1dyYXBwZXIuZ2V0TmF0aXZlTWFwKCkudGhlbihtYXAgPT4ge1xyXG4gICAgICBjb25zdCBjbHVzdGVyZXIgPSBuZXcgTWFya2VyQ2x1c3RlcmVyKG1hcCwgW10sIG9wdGlvbnMpO1xyXG4gICAgICB0aGlzLl9yZXNvbHZlcihjbHVzdGVyZXIpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRDbHVzdGVyZXJJbnN0YW5jZSgpOiBQcm9taXNlPE1hcmtlckNsdXN0ZXJlckluc3RhbmNlPiB7XHJcbiAgICByZXR1cm4gdGhpcy5fY2x1c3RlcmVySW5zdGFuY2U7XHJcbiAgfVxyXG5cclxuICBhZGRNYXJrZXIobWFya2VyOiBBZ21NYXJrZXIpOiB2b2lkIHtcclxuICAgIGNvbnN0IGNsdXN0ZXJQcm9taXNlOiBQcm9taXNlPE1hcmtlckNsdXN0ZXJlckluc3RhbmNlPiA9IHRoaXMuZ2V0Q2x1c3RlcmVySW5zdGFuY2UoKTtcclxuICAgIGNvbnN0IG1hcmtlclByb21pc2UgPSB0aGlzLl9tYXBzV3JhcHBlclxyXG4gICAgICAuY3JlYXRlTWFya2VyKHtcclxuICAgICAgICBwb3NpdGlvbjoge1xyXG4gICAgICAgICAgbGF0OiBtYXJrZXIubGF0aXR1ZGUsXHJcbiAgICAgICAgICBsbmc6IG1hcmtlci5sb25naXR1ZGUsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBsYWJlbDogbWFya2VyLmxhYmVsLFxyXG4gICAgICAgIGRyYWdnYWJsZTogbWFya2VyLmRyYWdnYWJsZSxcclxuICAgICAgICBpY29uOiBtYXJrZXIuaWNvblVybCxcclxuICAgICAgICBvcGFjaXR5OiBtYXJrZXIub3BhY2l0eSxcclxuICAgICAgICB2aXNpYmxlOiBtYXJrZXIudmlzaWJsZSxcclxuICAgICAgICB6SW5kZXg6IG1hcmtlci56SW5kZXgsXHJcbiAgICAgICAgdGl0bGU6IG1hcmtlci50aXRsZSxcclxuICAgICAgICBjbGlja2FibGU6IG1hcmtlci5jbGlja2FibGUsXHJcbiAgICAgIH0sIGZhbHNlKTtcclxuXHJcbiAgICBQcm9taXNlXHJcbiAgICAgIC5hbGwoW2NsdXN0ZXJQcm9taXNlLCBtYXJrZXJQcm9taXNlXSlcclxuICAgICAgLnRoZW4oKFtjbHVzdGVyLCBtYXJrZXJdKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIGNsdXN0ZXIuYWRkTWFya2VyKG1hcmtlcik7XHJcbiAgICAgIH0pO1xyXG4gICAgdGhpcy5fbWFya2Vycy5zZXQobWFya2VyLCBtYXJrZXJQcm9taXNlKTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZU1hcmtlcihtYXJrZXI6IEFnbU1hcmtlcik6IFByb21pc2U8dm9pZD4ge1xyXG4gICAgY29uc3QgbSA9IHRoaXMuX21hcmtlcnMuZ2V0KG1hcmtlcik7XHJcbiAgICBpZiAobSA9PSBudWxsKSB7XHJcbiAgICAgIC8vIG1hcmtlciBhbHJlYWR5IGRlbGV0ZWRcclxuICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSgpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG0udGhlbigobTogTWFya2VyKSA9PiB7XHJcbiAgICAgIHRoaXMuX3pvbmUucnVuKCgpID0+IHtcclxuICAgICAgICBtLnNldE1hcChudWxsKTtcclxuICAgICAgICB0aGlzLmdldENsdXN0ZXJlckluc3RhbmNlKCkudGhlbihjbHVzdGVyID0+IHtcclxuICAgICAgICAgIGNsdXN0ZXIucmVtb3ZlTWFya2VyKG0pO1xyXG4gICAgICAgICAgdGhpcy5fbWFya2Vycy5kZWxldGUobWFya2VyKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNsZWFyTWFya2VycygpOiBQcm9taXNlPHZvaWQ+IHtcclxuICAgIHJldHVybiB0aGlzLmdldENsdXN0ZXJlckluc3RhbmNlKCkudGhlbihjbHVzdGVyID0+IHtcclxuICAgICAgY2x1c3Rlci5jbGVhck1hcmtlcnMoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2V0R3JpZFNpemUoYzogQWdtTWFya2VyQ2x1c3Rlcik6IHZvaWQge1xyXG4gICAgdGhpcy5nZXRDbHVzdGVyZXJJbnN0YW5jZSgpLnRoZW4oY2x1c3RlciA9PiB7XHJcbiAgICAgIGNsdXN0ZXIuc2V0R3JpZFNpemUoYy5ncmlkU2l6ZSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHNldE1heFpvb20oYzogQWdtTWFya2VyQ2x1c3Rlcik6IHZvaWQge1xyXG4gICAgdGhpcy5nZXRDbHVzdGVyZXJJbnN0YW5jZSgpLnRoZW4oY2x1c3RlciA9PiB7XHJcbiAgICAgIGNsdXN0ZXIuc2V0TWF4Wm9vbShjLm1heFpvb20pO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBzZXRTdHlsZXMoYzogQWdtTWFya2VyQ2x1c3Rlcik6IHZvaWQge1xyXG4gICAgdGhpcy5nZXRDbHVzdGVyZXJJbnN0YW5jZSgpLnRoZW4oY2x1c3RlciA9PiB7XHJcbiAgICAgIGNsdXN0ZXIuc2V0U3R5bGVzKGMuc3R5bGVzKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2V0Wm9vbU9uQ2xpY2soYzogQWdtTWFya2VyQ2x1c3Rlcik6IHZvaWQge1xyXG4gICAgdGhpcy5nZXRDbHVzdGVyZXJJbnN0YW5jZSgpLnRoZW4oY2x1c3RlciA9PiB7XHJcbiAgICAgIGlmIChjLnpvb21PbkNsaWNrICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICBjbHVzdGVyLnpvb21PbkNsaWNrXyA9IGMuem9vbU9uQ2xpY2s7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2V0QXZlcmFnZUNlbnRlcihjOiBBZ21NYXJrZXJDbHVzdGVyKTogdm9pZCB7XHJcbiAgICB0aGlzLmdldENsdXN0ZXJlckluc3RhbmNlKCkudGhlbihjbHVzdGVyID0+IHtcclxuICAgICAgaWYgKGMuYXZlcmFnZUNlbnRlciAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgY2x1c3Rlci5hdmVyYWdlQ2VudGVyXyA9IGMuYXZlcmFnZUNlbnRlcjtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBzZXRJbWFnZVBhdGgoYzogQWdtTWFya2VyQ2x1c3Rlcik6IHZvaWQge1xyXG4gICAgdGhpcy5nZXRDbHVzdGVyZXJJbnN0YW5jZSgpLnRoZW4oY2x1c3RlciA9PiB7XHJcbiAgICAgIGlmIChjLmltYWdlUGF0aCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgY2x1c3Rlci5pbWFnZVBhdGhfID0gYy5pbWFnZVBhdGg7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2V0TWluaW11bUNsdXN0ZXJTaXplKGM6IEFnbU1hcmtlckNsdXN0ZXIpOiB2b2lkIHtcclxuICAgIHRoaXMuZ2V0Q2x1c3RlcmVySW5zdGFuY2UoKS50aGVuKGNsdXN0ZXIgPT4ge1xyXG4gICAgICBpZiAoYy5taW5pbXVtQ2x1c3RlclNpemUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgIGNsdXN0ZXIubWluaW11bUNsdXN0ZXJTaXplXyA9IGMubWluaW11bUNsdXN0ZXJTaXplO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHNldEltYWdlRXh0ZW5zaW9uKGM6IEFnbU1hcmtlckNsdXN0ZXIpOiB2b2lkIHtcclxuICAgIHRoaXMuZ2V0Q2x1c3RlcmVySW5zdGFuY2UoKS50aGVuKGNsdXN0ZXIgPT4ge1xyXG4gICAgICBpZiAoYy5pbWFnZUV4dGVuc2lvbiAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgY2x1c3Rlci5pbWFnZUV4dGVuc2lvbl8gPSBjLmltYWdlRXh0ZW5zaW9uO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIGNyZWF0ZUNsdXN0ZXJFdmVudE9ic2VydmFibGU8VD4oZXZlbnROYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFQ+IHtcclxuICAgIHJldHVybiBPYnNlcnZhYmxlLmNyZWF0ZSgob2JzZXJ2ZXI6IE9ic2VydmVyPFQ+KSA9PiB7XHJcbiAgICAgIHRoaXMuX3pvbmUucnVuT3V0c2lkZUFuZ3VsYXIoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuX2NsdXN0ZXJlckluc3RhbmNlLnRoZW4oKG06IE1hcmtlckNsdXN0ZXJlckluc3RhbmNlKSA9PiB7XHJcbiAgICAgICAgICBtLmFkZExpc3RlbmVyKGV2ZW50TmFtZSwgKGU6IFQpID0+IHRoaXMuX3pvbmUucnVuKCgpID0+IG9ic2VydmVyLm5leHQoZSkpKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHNldENhbGN1bGF0b3IgKGM6IEFnbU1hcmtlckNsdXN0ZXIpOiB2b2lkIHtcclxuICAgIHRoaXMuZ2V0Q2x1c3RlcmVySW5zdGFuY2UoKS50aGVuKGNsdXN0ZXIgPT4ge1xyXG4gICAgICBpZiAodHlwZW9mIGMuY2FsY3VsYXRvciA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIGNsdXN0ZXIuc2V0Q2FsY3VsYXRvcihjLmNhbGN1bGF0b3IpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19